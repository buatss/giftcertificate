package com.epam.esm.dao.model.converter;

import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.model.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EntityDtoConverterTest {

    private final LocalDateTime time = LocalDateTime.now();
    private GiftCertificate certificateEntity;
    private Tag tagEntity;
    private User userEntity;
    private Order orderEntity;
    private GiftCertificateDto certificateDto;
    private TagDto tagDto;
    private UserDto userDto;
    private OrderDto orderDto;
    private Tag tagEntity2;
    private TagDto tagDto2;
    private GiftCertificate certificateEntity2;
    private GiftCertificateDto certificateDto2;
    private Order orderEntity2;
    private OrderDto orderDto2;

    @BeforeEach
    void setUp() {
        certificateEntity = new GiftCertificate(11L,
                "name",
                "description",
                BigDecimal.TEN,
                30,
                time,
                time,
                Collections.emptySet(),
                Collections.emptySet()
        );
        certificateEntity2 = new GiftCertificate(2L,
                "name2",
                "description2",
                BigDecimal.ONE,
                33,
                time,
                time,
                Collections.emptySet(),
                Collections.emptySet()
        );

        certificateDto = new GiftCertificateDto(11L,
                "name",
                "description",
                BigDecimal.TEN,
                30,
                time,
                time,
                new TreeSet<>(),
                new TreeSet<>()
        );
        certificateDto2 = new GiftCertificateDto(2L,
                "name2",
                "description2",
                BigDecimal.ONE,
                33,
                time,
                time,
                new TreeSet<>(),
                new TreeSet<>()
        );

        tagEntity = new Tag(2L, "Macropus robustus", Collections.emptySet());
        tagEntity2 = new Tag(2L, "tagname2", Collections.emptySet());

        tagDto = new TagDto(2L, "Macropus robustus", new TreeSet<>());
        tagDto2 = new TagDto(2L, "tagname2", new TreeSet<>());

        userEntity = new User(4L, "Row Romand", "password", Collections.emptySet());
        userDto = new UserDto(4L, "Row Romand", new TreeSet<>());

        orderEntity = new Order(11L, userEntity, certificateEntity, time, new BigDecimal("428532.97"));
        orderEntity2 = new Order(12L, userEntity, certificateEntity2, time, new BigDecimal("14.97"));

        orderDto = new OrderDto(11L, 4L, 11L, userDto, certificateDto, time, new BigDecimal("428532.97"));
        orderDto2 = new OrderDto(12L, 4L, 2L, userDto, certificateDto2, time, new BigDecimal("14.97"));
    }

    @Test
    void convertCertificateEntityToDto() {
        assertEquals(certificateDto, EntityDtoConverter.convertEntityToDto(certificateEntity, false));
    }

    @Test
    void convertTagEntityToDto() {
        assertEquals(tagDto, EntityDtoConverter.convertEntityToDto(tagEntity, false));
    }

    @Test
    void convertUserEntityToDto() {
        assertEquals(userDto, EntityDtoConverter.convertEntityToDto(userEntity, false));
    }

    @Test
    void convertOrderEntityToDto() {
        assertEquals(orderDto, EntityDtoConverter.convertEntityToDto(orderEntity, true));
    }

    @Test
    void convertCertificateEntitiesToDto() {
        List<GiftCertificate> certificates = new ArrayList<>();
        certificates.add(certificateEntity);
        certificates.add(certificateEntity2);
        List<GiftCertificateDto> expected = new ArrayList<>();
        expected.add(certificateDto);
        expected.add(certificateDto2);

        List<GiftCertificateDto> result = EntityDtoConverter.convertCertificateEntitiesToDto(certificates);

        assertEquals(expected, result);
    }

    @Test
    void convertTagEntitiesToDto() {
        List<Tag> tags = new ArrayList<>();
        tags.add(tagEntity);
        tags.add(tagEntity2);
        List<TagDto> expected = new ArrayList<>();
        expected.add(tagDto);
        expected.add(tagDto2);

        List<TagDto> actual = EntityDtoConverter.convertTagEntitiesToDto(tags);

        assertEquals(expected, actual);
    }

    @Test
    void convertOrderEntitiesToDto() {
        List<Order> orders = new ArrayList<>();
        orders.add(orderEntity);
        orders.add(orderEntity2);
        List<OrderDto> expected = new ArrayList<>();
        expected.add(orderDto);
        expected.add(orderDto2);

        List<OrderDto> actual = EntityDtoConverter.convertOrderEntitiesToDto(orders);

        assertEquals(expected, actual);
    }
}
