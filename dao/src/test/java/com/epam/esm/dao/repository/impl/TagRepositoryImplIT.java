package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.config.AppConfigDev;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.TagRepository;
import com.epam.esm.dao.utils.Utils;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("dev")
@ContextConfiguration(classes = {AppConfigDev.class, TagRepositoryImpl.class},
        loader = AnnotationConfigContextLoader.class)
@SpringBootTest
@Transactional
@Sql(value = "/data.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TagRepositoryImplIT {

    @Autowired
    TagRepository repository;

    @Test
    void createTag_validTag_returnsOptionalDto() {
        Tag tag = new Tag();
        tag.setName("new tag");
        tag.setCertificates(Collections.emptySet());
        TagDto expected = new TagDto(11L, "new tag", new TreeSet<>());

        Optional<TagDto> result = repository.createTag(tag);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void createTag_invalidTag_returnsOptionalEmpty() {
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("new tag");
        tag.setCertificates(Collections.emptySet());

        Optional<TagDto> result = repository.createTag(tag);

        assertFalse(result.isPresent());
    }

    @Test
    void connectCertificateAndTag_pairUnassigned_returnsOptionalDto() {
        GiftCertificate certificate = new GiftCertificate(1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                Collections.emptySet(),
                Collections.emptySet()
        );
        Tag tag = new Tag(2L, "Macropus robustus", Collections.emptySet());
        TreeSet<GiftCertificateDto> certificates = new TreeSet<>();
        certificates.add(new GiftCertificateDto(1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                new TreeSet<>(),
                new TreeSet<>()
        ));
        TagDto expected = new TagDto(2L, "Macropus robustus", certificates);

        Optional<TagDto> result = repository.connectTagAndCertificate(certificate, tag);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void connectCertificateAndTag_alreadyAssigned_returnsEmpty() {
        GiftCertificate certificate = new GiftCertificate();
        Tag tag = new Tag();
        certificate.setId(2L);
        tag.setId(1L);

        Optional<TagDto> result = repository.connectTagAndCertificate(certificate, tag);

        assertFalse(result.isPresent());
    }

    @Test
    void findTagById_found_returnsOptionalDto() {
        Tag tag = new Tag();
        tag.setId(2L);
        TagDto expected = new TagDto(2L, "Macropus robustus", new TreeSet<>());

        Optional<TagDto> result = repository.findTagById(tag);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void findTagById_notFound_returnsOptionalEmpty() {
        Tag tag = new Tag();
        tag.setId(22L);

        Optional<TagDto> result = repository.findTagById(tag);

        assertFalse(result.isPresent());
    }

    @Test
    void findTagsById_found_returnsOptionalListOfDtoSortedById() {
        SoftAssertions softly = new SoftAssertions();
        TreeSet<GiftCertificateDto> subset1 = new TreeSet<>();
        subset1.add(new GiftCertificateDto(7L,
                "Sonair",
                "fermentum donec ut",
                new BigDecimal("959288.91"),
                289,
                Utils.convertStringToLocalDateTime("2002-04-15 16:09:46"),
                Utils.convertStringToLocalDateTime("2013-05-18 23:42:11"),
                new TreeSet<>(),
                new TreeSet<>()
        ));
        TreeSet<GiftCertificateDto> subset2 = new TreeSet<>();
        subset2.add(new GiftCertificateDto(10L,
                "Otcom",
                "vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis",
                new BigDecimal("872504.15"),
                6,
                Utils.convertStringToLocalDateTime("2006-03-23 13:29:43"),
                Utils.convertStringToLocalDateTime("2020-01-13 12:51:39"),
                new TreeSet<>(),
                new TreeSet<>()
        ));
        TagDto expected1 = new TagDto(9L, "Passer domesticus", subset1);
        TagDto expected2 = new TagDto(10L, "Macropus rufogriseus", subset2);
        List<TagDto> expectedSortedAscending = List.of(expected1, expected2);
        List<TagDto> expectedSortedDescending = List.of(expected2, expected1);

        Optional<List<TagDto>> resultSortedAsc = repository.findTagsById(5, 2, true);
        Optional<List<TagDto>> resultSortedDesc = repository.findTagsById(1, 2, false);

        softly.assertThat(resultSortedAsc.isPresent()).isTrue();
        softly.assertThat(resultSortedDesc.isPresent()).isTrue();
        softly.assertThat(resultSortedAsc.get()).isEqualTo(expectedSortedAscending);
        softly.assertThat(resultSortedDesc.get()).isEqualTo(expectedSortedDescending);
        softly.assertAll();
    }

    @Test
    void findTagsById_notFound_returnsOptionalEmptyList() {
        Optional<List<TagDto>> result = repository.findTagsById(6, 2, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void findTagsByNamesAndIds_found_returnsOptionalListOfDto() {
        SoftAssertions softly = new SoftAssertions();
        Tag tag = new Tag();
        Tag tag2 = new Tag();
        tag.setName("Macropus rob");
        tag2.setId(2L);
        Optional<List<TagDto>> expected = Optional.of(List.of(new TagDto(2L, "Macropus robustus", new TreeSet<>())));
        Tag[] tagsNameOnly = {tag};
        Tag[] tagsIdOnly = {tag2};

        Optional<List<TagDto>> resultByName = repository.findTagsByNamesAndIds(tagsNameOnly, 1, 5, true);
        Optional<List<TagDto>> resultById = repository.findTagsByNamesAndIds(tagsIdOnly, 1, 5, true);

        softly.assertThat(resultByName.isPresent()).isTrue();
        softly.assertThat(expected).isEqualTo(resultByName);
        softly.assertThat(resultById.isPresent()).isTrue();
        softly.assertThat(expected).isEqualTo(resultById);
        softly.assertAll();
    }

    @Test
    void findTagsByNamesAndIds_notFound_returnsOptionalEmptyList() {
        Tag tag = new Tag();
        tag.setName("not existing tag");
        Tag[] tags = {tag};

        Optional<List<TagDto>> result = repository.findTagsByNamesAndIds(tags, 1, 5, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void findUnassignedTags_found_returnsOptionalDtoSortedById() {
        SoftAssertions softly = new SoftAssertions();
        TagDto assignedTag1 = new TagDto(2L, "Macropus robustus", new TreeSet<>());
        TagDto assignedTag2 = new TagDto(3L, "Eumetopias jubatus", new TreeSet<>());
        TagDto assignedTag3 = new TagDto(6L, "Tayassu tajacu", new TreeSet<>());
        List<TagDto> expectedSortedAsc = List.of(assignedTag1, assignedTag2, assignedTag3);
        List<TagDto> expectedSortedDesc = List.of(assignedTag3, assignedTag2, assignedTag1);

        Optional<List<TagDto>> resultSortedAsc = repository.findUnassignedTags(1, 5, true);
        Optional<List<TagDto>> resultSortedDesc = repository.findUnassignedTags(1, 5, false);

        softly.assertThat(resultSortedAsc.isPresent()).isTrue();
        softly.assertThat(resultSortedDesc.isPresent()).isTrue();
        softly.assertThat(resultSortedAsc.get()).isEqualTo(expectedSortedAsc);
        softly.assertThat(resultSortedDesc.get()).isEqualTo(expectedSortedDesc);
        softly.assertAll();
    }

    @Test
    @Sql(value = "/deleteCertificate_Tag.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void findUnassignedTags_notFound_returnsOptionalEmptyList() {
        Optional<List<TagDto>> result = repository.findUnassignedTags(1, 5, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void deleteTagById_presentBefore_returnsOptionalDto() {
        Tag tag = new Tag();
        tag.setId(2L);
        TagDto expected = new TagDto(2L, "Macropus robustus", new TreeSet<>());

        Optional<TagDto> result = repository.deleteTagById(tag);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void deleteTagById_absent_returnsOptionalEmpty() {
        Tag tag = new Tag();
        tag.setId(22L);

        Optional<TagDto> result = repository.deleteTagById(tag);

        assertFalse(result.isPresent());
    }

    @Test
    void findMostFrequentTagFromUserWithMostExpensiveOrdersTotal_found_returnsOptionalDto() {
        TagDto expected = new TagDto(1L, "Fregata magnificans", new TreeSet<>());
        GiftCertificateDto certificateDto1 = new GiftCertificateDto(2L,
                "Tempsoft",
                "quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec",
                new BigDecimal("382662.75"),
                209,
                Utils.convertStringToLocalDateTime("2007-06-23 10:42:42"),
                Utils.convertStringToLocalDateTime("2013-10-25 15:40:48"),
                new TreeSet<>(),
                new TreeSet<>()
        );

        GiftCertificateDto certificateDto2 = new GiftCertificateDto(3L,
                "Bitwolf",
                "at diam nam tristique",
                new BigDecimal("439449.00"),
                228,
                Utils.convertStringToLocalDateTime("2010-05-02 09:35:05"),
                Utils.convertStringToLocalDateTime("2018-08-14 08:32:31"),
                new TreeSet<>(),
                new TreeSet<>()
        );

        GiftCertificateDto certificateDto3 = new GiftCertificateDto(4L,
                "Kanlam",
                "description = pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet",
                new BigDecimal("443786.01"),
                226,
                Utils.convertStringToLocalDateTime("2008-03-24 15:40:34"),
                Utils.convertStringToLocalDateTime("2022-01-17 19:56:02"),
                new TreeSet<>(),
                new TreeSet<>()
        );

        TreeSet<GiftCertificateDto> certificateDtos = new TreeSet<>();
        certificateDtos.add(certificateDto1);
        certificateDtos.add(certificateDto2);
        certificateDtos.add(certificateDto3);
        expected.setCertificates(certificateDtos);

        Optional<TagDto> result = repository.findMostFrequentTagFromUserWithMostExpensiveOrdersTotal();

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    @Sql(value = "/deleteCertificate_Tag.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    void findMostFrequentTagFromUserWithMostExpensiveOrdersTotal_notFound_returnsOptionalEmpty() {
        Optional<TagDto> result = repository.findMostFrequentTagFromUserWithMostExpensiveOrdersTotal();

        assertFalse(result.isPresent());
    }
}
