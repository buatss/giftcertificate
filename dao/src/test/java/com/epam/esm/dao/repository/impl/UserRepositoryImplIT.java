package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.config.AppConfigDev;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.repository.UserRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("dev")
@ContextConfiguration(classes = {AppConfigDev.class, UserRepositoryImpl.class},
        loader = AnnotationConfigContextLoader.class)
@SpringBootTest
@Transactional
@Sql(value = "/data.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserRepositoryImplIT {

    @Autowired
    UserRepository repository;

    @Test
    void findUserById_found_returnsOptionalDto() {
        UserDto expected = new UserDto(4L, "Row Romand", new TreeSet<>());

        Optional<UserDto> result = repository.findUserById(4L);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void findUserById_notFound_returnsOptionalEmpty() {
        Optional<UserDto> result = repository.findUserById(202L);

        assertFalse(result.isPresent());
    }

    @Test
    void findUsersById_found_returnsOptionalListOfDtoSorted() {
        SoftAssertions softly = new SoftAssertions();
        UserDto expected1 = new UserDto(9L, "Zena Dainty", new TreeSet<>());
        UserDto expected2 = new UserDto(10L, "Wylma Impy", new TreeSet<>());
        List<UserDto> expectedSortedAsc = List.of(expected1, expected2);
        List<UserDto> expectedSortedDesc = List.of(expected2, expected1);

        Optional<List<UserDto>> resultSortedAsc = repository.findUsersById(5, 2, true);
        Optional<List<UserDto>> resultSortedDesc = repository.findUsersById(1, 2, false);

        softly.assertThat(resultSortedAsc.isPresent()).isTrue();
        softly.assertThat(resultSortedDesc.isPresent()).isTrue();
        softly.assertThat(resultSortedAsc.get()).isEqualTo(expectedSortedAsc);
        softly.assertThat(resultSortedDesc.get()).isEqualTo(expectedSortedDesc);
        softly.assertAll();
    }

    @Test
    void findUsersById_notFound_returnsOptionalEmptyList() {
        Optional<List<UserDto>> result = repository.findUsersById(6, 2, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }
}
