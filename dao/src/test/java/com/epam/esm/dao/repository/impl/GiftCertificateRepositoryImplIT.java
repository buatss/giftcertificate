package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.config.AppConfigDev;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.GiftCertificateRepository;
import com.epam.esm.dao.utils.Utils;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("dev")
@ContextConfiguration(classes = {AppConfigDev.class, GiftCertificateRepositoryImpl.class},
        loader = AnnotationConfigContextLoader.class)
@SpringBootTest
@Transactional
@Sql(value = "/data.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class GiftCertificateRepositoryImplIT {
    private final int DEFAULT_PAGE_NUMBER = 1;
    private final int DEFAULT_PAGE_SIZE = 5;
    private final Boolean SORT_ASC = true;
    private final Boolean SORT_DESC = false;

    @Autowired
    GiftCertificateRepository repository;

    @Test
    void createGiftCertificate_validCertificate_returnsOptionalOfDto() throws InterruptedException {
        SoftAssertions softly = new SoftAssertions();
        GiftCertificate certificate =
                new GiftCertificate(null, "name", "description", BigDecimal.TEN, 30, null, null, null, null);
        LocalDateTime now = LocalDateTime.now();
        Thread.sleep(100L);
        GiftCertificateDto expected = new GiftCertificateDto(11L,
                "name",
                "description",
                BigDecimal.TEN,
                30,
                null,
                null,
                new TreeSet<>(),
                new TreeSet<>()
        );

        Optional<GiftCertificateDto> result = repository.createGiftCertificate(certificate);

        assertTrue(result.isPresent());
        GiftCertificateDto actual = result.get();

        softly.assertThat(expected.getId()).isEqualTo(actual.getId());
        softly.assertThat(expected.getName()).isEqualTo(actual.getName());
        softly.assertThat(expected.getDescription()).isEqualTo(actual.getDescription());
        softly.assertThat(expected.getPrice()).isEqualTo(actual.getPrice());
        softly.assertThat(expected.getDuration()).isEqualTo(actual.getDuration());
        softly.assertThat(actual.getCreateDate().isAfter(now)).isTrue();
        softly.assertThat(actual.getLastUpdateDate().isAfter(now)).isTrue();
        softly.assertThat(actual.getTags()).isEqualTo(Collections.emptySet());
        softly.assertThat(actual.getOrders()).isEqualTo(Collections.emptySet());
        softly.assertAll();
    }

    @Test
    void createGiftCertificate_invalidCertificate_returnsOptionalEmpty() {
        GiftCertificate certificate =
                new GiftCertificate(1L, "name", "description", BigDecimal.TEN, 30, null, null, null, null);

        Optional<GiftCertificateDto> result = repository.createGiftCertificate(certificate);
        assertFalse(result.isPresent());
    }

    @Test
    void connectCertificateAndTag_pairUnassigned_returnsOptionalDto() {
        GiftCertificate certificate = new GiftCertificate(1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                Collections.emptySet(),
                Collections.emptySet()
        );
        Tag tag = new Tag(2L, "Macropus robustus", Collections.emptySet());
        TreeSet<TagDto> tags = new TreeSet<>();
        TagDto tagDto = new TagDto(2L, "Macropus robustus", new TreeSet<>());
        tags.add(tagDto);
        GiftCertificateDto expected = new GiftCertificateDto(1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                tags,
                new TreeSet<>()
        );

        Optional<GiftCertificateDto> result = repository.connectCertificateAndTag(certificate, tag);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void connectCertificateAndTag_alreadyAssigned_returnsEmpty() {
        GiftCertificate certificate = new GiftCertificate();
        Tag tag = new Tag();
        certificate.setId(2L);
        tag.setId(1L);

        Optional<GiftCertificateDto> result = repository.connectCertificateAndTag(certificate, tag);
        assertFalse(result.isPresent());
    }

    @Test
    void findGiftCertificateById_found_returnsOptionalOfDto() {
        final String createDate = "2008-11-25 01:17:21";
        final String lastUpdateDate = "2015-09-23 23:15:35";

        GiftCertificateDto expected = new GiftCertificateDto(1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime(createDate),
                Utils.convertStringToLocalDateTime(lastUpdateDate),
                new TreeSet<>(),
                new TreeSet<>()
        );

        GiftCertificate filter = new GiftCertificate();
        filter.setId(1L);
        Optional<GiftCertificateDto> result = repository.findGiftCertificateById(filter);
        assertTrue(result.isPresent());
        GiftCertificateDto actual = result.get();
        assertEquals(expected, actual);
    }

    @Test
    void findGiftCertificatesById_found_returnsOptionalListOfDtoSorted() {
        SoftAssertions softly = new SoftAssertions();
        TreeSet<TagDto> subset1 = new TreeSet<>();
        subset1.add(new TagDto(5L, "Lybius torquatus", new TreeSet<>()));
        TreeSet<TagDto> subset2 = new TreeSet<>();
        subset2.add(new TagDto(10L, "Macropus rufogriseus", new TreeSet<>()));
        TreeSet<OrderDto> subset3 = new TreeSet<>();
        subset3.add(new OrderDto(10L,
                7L,
                10L,
                new UserDto(),
                new GiftCertificateDto(),
                Utils.convertStringToLocalDateTime("2020-08-16 19:50:29"),
                new BigDecimal("777855.51")
        ));
        GiftCertificateDto expected1 = new GiftCertificateDto(9L,
                "Vagram",
                "pellentesque volutpat dui maecenas tristique est et tempus semper est",
                new BigDecimal("514364.03"),
                194,
                Utils.convertStringToLocalDateTime("2009-04-22 09:24:10"),
                Utils.convertStringToLocalDateTime("2017-07-14 06:00:28"),
                subset1,
                new TreeSet<>()
        );
        GiftCertificateDto expected2 = new GiftCertificateDto(10L,
                "Otcom",
                "vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis",
                new BigDecimal("872504.15"),
                6,
                Utils.convertStringToLocalDateTime("2006-03-23 13:29:43"),
                Utils.convertStringToLocalDateTime("2020-01-13 12:51:39"),
                subset2,
                subset3
        );

        List<GiftCertificateDto> expectedSortedAscending = List.of(expected1, expected2);
        Optional<List<GiftCertificateDto>> resultSortedAscending = repository.findGiftCertificatesById(5, 2, true);
        List<GiftCertificateDto> expectedSortedDescending = List.of(expected2, expected1);
        Optional<List<GiftCertificateDto>> resultSortedDescending = repository.findGiftCertificatesById(1, 2, false);

        softly.assertThat(resultSortedAscending.isPresent()).isTrue();
        softly.assertThat(resultSortedDescending.isPresent()).isTrue();
        softly.assertThat(resultSortedAscending.get()).isEqualTo(expectedSortedAscending);
        softly.assertThat(resultSortedDescending.get()).isEqualTo(expectedSortedDescending);
        softly.assertAll();
    }

    @Test
    void findGiftCertificatesById_found_returnsOptionalListOfDtoSortedDesc() {
        TreeSet<TagDto> subset1 = new TreeSet<>();
        subset1.add(new TagDto(5L, "Lybius torquatus", new TreeSet<>()));
        TreeSet<TagDto> subset2 = new TreeSet<>();
        subset2.add(new TagDto(10L, "Macropus rufogriseus", new TreeSet<>()));
        TreeSet<OrderDto> subset3 = new TreeSet<>();
        subset3.add(new OrderDto(10L,
                7L,
                10L,
                new UserDto(),
                new GiftCertificateDto(),
                Utils.convertStringToLocalDateTime("2020-08-16 19:50:29"),
                new BigDecimal("777855.51")
        ));
        GiftCertificateDto expected1 = new GiftCertificateDto(9L,
                "Vagram",
                "pellentesque volutpat dui maecenas tristique est et " + "tempus semper est",
                new BigDecimal("514364.03"),
                194,
                Utils.convertStringToLocalDateTime("2009-04-22 09:24:10"),
                Utils.convertStringToLocalDateTime("2017-07-14 06:00:28"),
                subset1,
                new TreeSet<>()
        );
        GiftCertificateDto expected2 = new GiftCertificateDto(10L,
                "Otcom",
                "vestibulum aliquet ultrices erat tortor sollicitudin " + "mi sit amet lobortis",
                new BigDecimal("872504.15"),
                6,
                Utils.convertStringToLocalDateTime("2006-03-23 13:29:43"),
                Utils.convertStringToLocalDateTime("2020-01-13 12:51:39"),
                subset2,
                subset3
        );

        List<GiftCertificateDto> expected = List.of(expected2, expected1);
        Optional<List<GiftCertificateDto>> result = repository.findGiftCertificatesById(1, 2, SORT_DESC);

        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    void findGiftCertificatesById_notFound_returnsOptionalEmptyList() {
        Optional<List<GiftCertificateDto>> result = repository.findGiftCertificatesById(6, 2, SORT_ASC);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void findGiftCertificateById_notFound_returnsOptionalEmpty() {
        GiftCertificate filter = new GiftCertificate();
        filter.setId(11L);
        Optional<GiftCertificateDto> result = repository.findGiftCertificateById(filter);

        assertFalse(result.isPresent());
    }

    @Test
    void searchByIdsAndNames_matchingFoundForFilters_returnsOptionalDtoSortedById() {
        SoftAssertions softly = new SoftAssertions();
        GiftCertificate certificateNameOnly1 = new GiftCertificate();
        GiftCertificate certificateNameOnly2 = new GiftCertificate();
        certificateNameOnly1.setName("Ve");
        certificateNameOnly2.setName("Va");
        GiftCertificate certificateIdOnly1 = new GiftCertificate();
        GiftCertificate certificateIdOnly2 = new GiftCertificate();
        certificateIdOnly1.setId(9L);
        certificateIdOnly2.setId(8L);
        GiftCertificate certificateIdAndName = new GiftCertificate();
        certificateIdAndName.setId(8L);
        certificateIdAndName.setName("Va");

        GiftCertificate[] certificatesNamesOnly = {certificateIdOnly1, certificateIdOnly2};
        GiftCertificate[] certificatesIdsOnly = {certificateIdOnly1, certificateIdOnly2};
        GiftCertificate[] certificatesIdAndName = {certificateNameOnly1, certificateIdOnly1};
        GiftCertificate[] singleCertificateIdAndName = {certificateIdAndName};
        GiftCertificateDto expected1 = new GiftCertificateDto(8L,
                "Ventosanzap",
                "cubilia curae nulla dapibus dolor " + "vel est donec odio justo sollicitudin ut",
                new BigDecimal("594793.97"),
                262,
                Utils.convertStringToLocalDateTime("2001-01-12 04:59:45"),
                Utils.convertStringToLocalDateTime("2019" + "-11-09 08:24:10"),
                new TreeSet<>(),
                new TreeSet<>()
        );
        GiftCertificateDto expected2 = new GiftCertificateDto(9L,
                "Vagram",
                "pellentesque volutpat dui maecenas " + "tristique est et tempus semper est",
                new BigDecimal("514364.03"),
                194,
                Utils.convertStringToLocalDateTime("2009-04-22 09:24:10"),
                Utils.convertStringToLocalDateTime("2017" + "-07-14 06:00:28"),
                Utils.initializeTreeSet(new TagDto(5L, "Lybius torquatus", new TreeSet<>())),
                new TreeSet<>()
        );
        List<GiftCertificateDto> expectedSortedAscending = List.of(expected1, expected2);
        List<GiftCertificateDto> expectedSortedDescending = List.of(expected2, expected1);

        Optional<List<GiftCertificateDto>> resultSortedAscendingFoundByNamesOnly =
                repository.searchByIdsAndNames(certificatesNamesOnly, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);
        Optional<List<GiftCertificateDto>> resultSortedDescendingFoundByNamesOnly = repository.searchByIdsAndNames(
                certificatesNamesOnly,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_DESC
        );
        Optional<List<GiftCertificateDto>> resultSortedAscendingFoundByIdsOnly =
                repository.searchByIdsAndNames(certificatesIdsOnly, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);
        Optional<List<GiftCertificateDto>> resultSortedDescendingFoundByIdsOnly =
                repository.searchByIdsAndNames(certificatesIdsOnly, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_DESC);
        Optional<List<GiftCertificateDto>> resultSortedAscendingFoundByTwoCertificatesIdAndName =
                repository.searchByIdsAndNames(certificatesIdAndName, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);
        Optional<List<GiftCertificateDto>> resultSortedDescendingFoundByTwoCertificatesIdAndName =
                repository.searchByIdsAndNames(certificatesIdAndName,
                        DEFAULT_PAGE_NUMBER,
                        DEFAULT_PAGE_SIZE,
                        SORT_DESC
                );
        Optional<List<GiftCertificateDto>> resultSortedAscendingFoundByOneCertificateWithIdAndName =
                repository.searchByIdsAndNames(singleCertificateIdAndName,
                        DEFAULT_PAGE_NUMBER,
                        DEFAULT_PAGE_SIZE,
                        SORT_ASC
                );
        Optional<List<GiftCertificateDto>> resultSortedDescendingFoundByOneCertificateWithIdAndName =
                repository.searchByIdsAndNames(singleCertificateIdAndName,
                        DEFAULT_PAGE_NUMBER,
                        DEFAULT_PAGE_SIZE,
                        SORT_DESC
                );

        softly.assertThat(resultSortedAscendingFoundByNamesOnly.isPresent()).isTrue();
        softly.assertThat(resultSortedAscendingFoundByNamesOnly.get()).isEqualTo(expectedSortedAscending);
        softly.assertThat(resultSortedDescendingFoundByNamesOnly.isPresent()).isTrue();
        softly.assertThat(resultSortedDescendingFoundByNamesOnly.get()).isEqualTo(expectedSortedDescending);
        softly.assertThat(resultSortedAscendingFoundByIdsOnly.isPresent()).isTrue();
        softly.assertThat(resultSortedAscendingFoundByIdsOnly.get()).isEqualTo(expectedSortedAscending);
        softly.assertThat(resultSortedDescendingFoundByIdsOnly.isPresent()).isTrue();
        softly.assertThat(resultSortedDescendingFoundByIdsOnly.get()).isEqualTo(expectedSortedDescending);
        softly.assertThat(resultSortedAscendingFoundByTwoCertificatesIdAndName.isPresent()).isTrue();
        softly.assertThat(resultSortedAscendingFoundByTwoCertificatesIdAndName.get())
              .isEqualTo(expectedSortedAscending);
        softly.assertThat(resultSortedDescendingFoundByTwoCertificatesIdAndName.isPresent()).isTrue();
        softly.assertThat(resultSortedDescendingFoundByTwoCertificatesIdAndName.get())
              .isEqualTo(expectedSortedDescending);
        softly.assertThat(resultSortedAscendingFoundByOneCertificateWithIdAndName.isPresent()).isTrue();
        softly.assertThat(resultSortedAscendingFoundByOneCertificateWithIdAndName.get())
              .isEqualTo(expectedSortedAscending);
        softly.assertThat(resultSortedDescendingFoundByOneCertificateWithIdAndName.isPresent()).isTrue();
        softly.assertThat(resultSortedDescendingFoundByOneCertificateWithIdAndName.get())
              .isEqualTo(expectedSortedDescending);
        softly.assertAll();
    }

    @Test
    void searchByIdsAndNames_matchingNotFound_returnsOptionalEmptyList() {
        SoftAssertions softly = new SoftAssertions();
        GiftCertificate certificate = new GiftCertificate();
        certificate.setName("Not existing matching");
        GiftCertificate[] emptyCertificate = {certificate};
        GiftCertificate[] emptyArray = {};

        Optional<List<GiftCertificateDto>> resultOfSearchingByEmptyCertificate =
                repository.searchByIdsAndNames(emptyCertificate, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);
        Optional<List<GiftCertificateDto>> resultOfSearchingByEmptyArray =
                repository.searchByIdsAndNames(emptyArray, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);

        softly.assertThat(resultOfSearchingByEmptyCertificate).isPresent();
        softly.assertThat(resultOfSearchingByEmptyArray).isPresent();
        softly.assertThat(resultOfSearchingByEmptyCertificate.get()).isEmpty();
        softly.assertThat(resultOfSearchingByEmptyArray.get()).isEmpty();
        softly.assertAll();
    }

    @Test
    void searchByManyTagsIdAndName_found_returnsOptionalListOfDtoSortedById() {
        SoftAssertions softly = new SoftAssertions();
        Tag[] tagsWithIdOnly = {new Tag(10L, null, null), new Tag(9L, null, null)};
        Tag[] tagsWithNameOnly =
                {new Tag(null, "Macropus rufogriseus", null), new Tag(null, "Passer domesticus", null)};
        Tag[] singleTagWithIdAndName = {new Tag(10L, "Passer domesticus", null)};
        Tag[] twoTagsMixedAttributes = {new Tag(10L, null, null), new Tag(null, "Passer domesticus", null)};
        TreeSet<TagDto> tags1 = new TreeSet<>();
        TreeSet<TagDto> tags2 = new TreeSet<>();
        TreeSet<OrderDto> orders = new TreeSet<>();
        tags1.add(new TagDto(10L, "Macropus rufogriseu", new TreeSet<>()));
        tags2.add(new TagDto(9L, "Passer domesticus", new TreeSet<>()));
        orders.add(new OrderDto(10L,
                7L,
                10L,
                new UserDto(),
                new GiftCertificateDto(),
                Utils.convertStringToLocalDateTime("2020-08-16 19:50:29"),
                new BigDecimal("777855.51")
        ));
        GiftCertificateDto expected1 = new GiftCertificateDto(10L,
                "Otcom",
                "vestibulum aliquet ultrices erat tortor sollicitudin " + "mi sit amet lobortis",
                new BigDecimal("872504.15"),
                6,
                Utils.convertStringToLocalDateTime("2006-03-23 13:29:43"),
                Utils.convertStringToLocalDateTime("2020-01-13 12:51:39"),
                tags1,
                orders
        );
        GiftCertificateDto expected2 = new GiftCertificateDto(7L,
                "Sonair",
                "fermentum donec ut",
                new BigDecimal("959288.91"),
                289,
                Utils.convertStringToLocalDateTime("2002-04-15 16:09:46"),
                Utils.convertStringToLocalDateTime("2013-05-18 23:42:11"),
                tags2,
                new TreeSet<>()
        );
        List<GiftCertificateDto> expectedAscending = List.of(expected2, expected1);
        List<GiftCertificateDto> expectedDescending = List.of(expected1, expected2);

        Optional<List<GiftCertificateDto>> resultFoundByTwoTagsWithNameAsc = repository.searchByManyTagsIdAndName(
                tagsWithNameOnly,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_ASC
        );
        Optional<List<GiftCertificateDto>> resultFoundByTwoTagsWithNameDesc = repository.searchByManyTagsIdAndName(
                tagsWithNameOnly,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_DESC
        );
        Optional<List<GiftCertificateDto>> resultFoundByTwoTagsWithIdAsc =
                repository.searchByManyTagsIdAndName(tagsWithIdOnly, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);
        Optional<List<GiftCertificateDto>> resultFoundByTwoTagsWithIdDesc =
                repository.searchByManyTagsIdAndName(tagsWithIdOnly, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_DESC);
        Optional<List<GiftCertificateDto>> resultFoundBySingleTagAsc = repository.searchByManyTagsIdAndName(
                singleTagWithIdAndName,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_ASC
        );
        Optional<List<GiftCertificateDto>> resultFoundBySingleTagDesc = repository.searchByManyTagsIdAndName(
                singleTagWithIdAndName,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_DESC
        );

        Optional<List<GiftCertificateDto>> resultFoundByMixedTagsAsc = repository.searchByManyTagsIdAndName(
                twoTagsMixedAttributes,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_ASC
        );
        Optional<List<GiftCertificateDto>> resultFoundByMixedTagsDesc = repository.searchByManyTagsIdAndName(
                twoTagsMixedAttributes,
                DEFAULT_PAGE_NUMBER,
                DEFAULT_PAGE_SIZE,
                SORT_DESC
        );

        softly.assertThat(resultFoundByTwoTagsWithNameAsc.isPresent()).isTrue();
        softly.assertThat(resultFoundByTwoTagsWithNameAsc.get()).isEqualTo(expectedAscending);
        softly.assertThat(resultFoundByTwoTagsWithNameDesc.isPresent()).isTrue();
        softly.assertThat(resultFoundByTwoTagsWithNameDesc.get()).isEqualTo(expectedDescending);
        softly.assertThat(resultFoundByTwoTagsWithIdAsc.isPresent()).isTrue();
        softly.assertThat(resultFoundByTwoTagsWithIdAsc.get()).isEqualTo(expectedAscending);
        softly.assertThat(resultFoundByTwoTagsWithIdDesc.isPresent()).isTrue();
        softly.assertThat(resultFoundByTwoTagsWithIdDesc.get()).isEqualTo(expectedDescending);
        softly.assertThat(resultFoundBySingleTagAsc.isPresent()).isTrue();
        softly.assertThat(resultFoundBySingleTagAsc.get()).isEqualTo(expectedAscending);
        softly.assertThat(resultFoundBySingleTagDesc.isPresent()).isTrue();
        softly.assertThat(resultFoundBySingleTagDesc.get()).isEqualTo(expectedDescending);
        softly.assertThat(resultFoundByMixedTagsAsc.isPresent()).isTrue();
        softly.assertThat(resultFoundByMixedTagsAsc.get()).isEqualTo(expectedAscending);
        softly.assertThat(resultFoundByMixedTagsDesc.isPresent()).isTrue();
        softly.assertThat(resultFoundByMixedTagsDesc.get()).isEqualTo(expectedDescending);
        softly.assertAll();
    }

    @Test
    void searchByManyTagsIdAndName_notFound_returnsOptionalEmptyList() {
        SoftAssertions softly = new SoftAssertions();
        Tag[] emptyArray = {};
        Tag[] emptyTag = {new Tag()};

        Optional<List<GiftCertificateDto>> resultByEmptyArray =
                repository.searchByManyTagsIdAndName(emptyTag, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_DESC);
        Optional<List<GiftCertificateDto>> resultByEmptyTag =
                repository.searchByManyTagsIdAndName(emptyArray, DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_DESC);

        softly.assertThat(resultByEmptyArray).isPresent();
        softly.assertThat(resultByEmptyTag).isPresent();
        softly.assertThat(resultByEmptyArray.get()).isEmpty();
        softly.assertThat(resultByEmptyTag.get()).isEmpty();
        softly.assertAll();
    }

    @Test
    void findUnassignedCertificates_found_returnsOptionalDtoSorted() {
        SoftAssertions softly = new SoftAssertions();
        GiftCertificateDto expected1 = new GiftCertificateDto(1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                new TreeSet<>(),
                new TreeSet<>()
        );
        GiftCertificateDto expected2 = new GiftCertificateDto(8L,
                "Ventosanzap",
                "cubilia curae nulla dapibus dolor " + "vel est donec odio justo sollicitudin ut",
                new BigDecimal("594793.97"),
                262,
                Utils.convertStringToLocalDateTime("2001-01-12 04:59:45"),
                Utils.convertStringToLocalDateTime("2019" + "-11-09 08:24:10"),
                new TreeSet<>(),
                new TreeSet<>()
        );
        List<GiftCertificateDto> expectedAsc = List.of(expected1, expected2);
        List<GiftCertificateDto> expectedDesc = List.of(expected2, expected1);

        Optional<List<GiftCertificateDto>> resultAsc =
                repository.findUnassignedCertificates(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);
        Optional<List<GiftCertificateDto>> resultDesc =
                repository.findUnassignedCertificates(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_DESC);

        softly.assertThat(resultAsc.isPresent()).isTrue();
        softly.assertThat(resultDesc.isPresent()).isTrue();
        softly.assertThat(expectedAsc).isEqualTo(resultAsc.get());
        softly.assertThat(expectedDesc).isEqualTo(resultDesc.get());
        softly.assertAll();
    }

    @Test
    void findUnassignedCertificates_notFound_returnsOptionalEmptyList() {
        GiftCertificate filter = new GiftCertificate();
        filter.setId(1L);
        repository.deleteGiftCertificateById(filter);
        filter.setId(8L);
        repository.deleteGiftCertificateById(filter);
        filter.setId(9L);
        repository.deleteGiftCertificateById(filter);

        Optional<List<GiftCertificateDto>> result =
                repository.findUnassignedCertificates(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, SORT_ASC);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void updateGiftCertificate_validCertificate_returnsOptionalDto() {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(1L);
        certificate.setDescription("UPDATED DESCRIPTION");
        GiftCertificateDto expected = new GiftCertificateDto(1L,
                "Zontrax",
                "UPDATED DESCRIPTION",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                new TreeSet<>(),
                new TreeSet<>()
        );

        Optional<GiftCertificateDto> result = repository.updateGiftCertificate(certificate);

        assertTrue(result.isPresent());
        assertEquals(expected.getDescription(), result.get().getDescription());
    }

    @Test
    void updateGiftCertificate_invalidCertificate_returnsOptionalEmpty() {
        GiftCertificate certificate = new GiftCertificate();

        Optional<GiftCertificateDto> result = repository.updateGiftCertificate(certificate);

        assertFalse(result.isPresent());
    }

    @Test
    void deleteGiftCertificateById_presentBefore_returnsOptionalDto() {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(1L);

        Optional<GiftCertificateDto> result = repository.deleteGiftCertificateById(certificate);

        assertTrue(result.isPresent());
    }

    @Test
    void deleteGiftCertificateById_absentBefore_returnsOptionalEmpty() {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(11L);

        Optional<GiftCertificateDto> result = repository.deleteGiftCertificateById(certificate);
        Optional<GiftCertificateDto> result2 = repository.deleteGiftCertificateById(new GiftCertificate());

        assertFalse(result.isPresent());
        assertFalse(result2.isPresent());
    }
}
