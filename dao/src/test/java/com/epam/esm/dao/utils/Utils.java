package com.epam.esm.dao.utils;

import com.epam.esm.dao.model.dto.TagDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Utils {
    public static LocalDateTime convertStringToLocalDateTime(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(str, formatter);
    }

    public static TreeSet<TagDto> initializeTreeSet(TagDto... dto) {
        return Arrays.stream(dto)
                     .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(TagDto::getId))));
    }
}
