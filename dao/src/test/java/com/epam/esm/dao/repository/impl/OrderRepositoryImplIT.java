package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.config.AppConfigDev;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.OrderRepository;
import com.epam.esm.dao.utils.Utils;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("dev")
@ContextConfiguration(classes = {AppConfigDev.class, OrderRepositoryImpl.class},
        loader = AnnotationConfigContextLoader.class)
@SpringBootTest
@Transactional
@Sql(value = "/data.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class OrderRepositoryImplIT {

    private final UserDto expectedUserDto = new UserDto(1L, "Donnell Bissell", new TreeSet<>());
    private final GiftCertificateDto expectedCertificateDto = new GiftCertificateDto(
            2L,
            "Tempsoft",
            "quisque erat " + "eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec",
            new BigDecimal("382662.75"),
            209,
            Utils.convertStringToLocalDateTime("2007-06-23 10:42:42"),
            Utils.convertStringToLocalDateTime("2013-10-25 15:40:48"),
            new TreeSet<>(),
            new TreeSet<>()
    );
    @Autowired
    OrderRepository repository;

    @Test
    void createOrder_validOrder_returnsOptionalDto() {
        SoftAssertions softly = new SoftAssertions();
        LocalDateTime timeBefore = LocalDateTime.now();
        GiftCertificate certificate = new GiftCertificate(
                1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                Collections.emptySet(),
                Collections.emptySet()
        );
        User user = new User(1L, "Donnell Bissell", "bXWXSBI1l2", Collections.emptySet());

        Order order = new Order(null, user, certificate, null, new BigDecimal("428532.97"));
        Optional<OrderDto> result = repository.createOrder(certificate.getId(), user.getId());

        assertTrue(result.isPresent());
        OrderDto actual = result.get();
        certificate.setOrders(Collections.singleton(order));
        certificate.setOrders(Collections.singleton(order));

        GiftCertificateDto certificateDto = new GiftCertificateDto(
                1L,
                "Zontrax",
                "volutpat eleifend donec ut",
                new BigDecimal("428532.97"),
                187,
                Utils.convertStringToLocalDateTime("2008-11-25 01:17:21"),
                Utils.convertStringToLocalDateTime("2015-09-23 23:15:35"),
                new TreeSet<>(),
                new TreeSet<>()
        );
        UserDto userDto = new UserDto(1L, "Donnell Bissell", new TreeSet<>());
        OrderDto expected =
                new OrderDto(11L, 1L, 1L, userDto, certificateDto, LocalDateTime.now(), new BigDecimal("428532.97"));

        softly.assertThat(expected.getId()).isEqualTo(actual.getId());
        softly.assertThat(expected.getCertificateId()).isEqualTo(actual.getCertificateId());
        softly.assertThat(expected.getGiftCertificate()).isEqualTo(actual.getGiftCertificate());
        softly.assertThat(expected.getUserId()).isEqualTo(actual.getUserId());
        softly.assertThat(expected.getUser()).isEqualTo(actual.getUser());
        softly.assertThat(expected.getPrice()).isEqualTo(actual.getPrice());
        softly.assertThat(timeBefore).isBeforeOrEqualTo(actual.getPurchase_time());
        softly.assertThat(LocalDateTime.now()).isAfterOrEqualTo(actual.getPurchase_time());
        softly.assertAll();
    }

    @Test
    void createOrder_invalidOrder_returnsOptionalEmpty() {
        Optional<OrderDto> result = repository.createOrder(11L, 11L);
        assertFalse(result.isPresent());
    }

    @Test
    void findOrderById_validOrder_returnsOptionalDto() {
        Order order = new Order();
        order.setId(1L);
        Optional<OrderDto> result = repository.findOrderById(order);
        assertTrue(result.isPresent());
        UserDto user = new UserDto(1L, "Donnell Bissell", new TreeSet<>());

        OrderDto expected = new OrderDto(
                1L,
                1L,
                2L,
                user,
                expectedCertificateDto,
                Utils.convertStringToLocalDateTime("2021-07-31 21:04:03"),
                new BigDecimal("778809.00")
        );
        assertEquals(expected, result.get());
    }

    @Test
    void findOrderById_notFound_returnsOptionalEmpty() {
        Order order = new Order();
        order.setId(11L);
        Optional<OrderDto> result = repository.findOrderById(order);
        assertFalse(result.isPresent());
    }

    @Test
    void findOrdersById_found_returnsOptionalListOfDtoSortedById() {
        SoftAssertions softly = new SoftAssertions();
        OrderDto expected1 =
                new OrderDto(9L, 2L, 6L, new UserDto(2L, "Kirk Melendez", new TreeSet<>()), new GiftCertificateDto(
                        6L,
                        "Bytecard",
                        "lacinia erat vestibulum sed",
                        new BigDecimal("12895" + ".69"),
                        108,
                        Utils.convertStringToLocalDateTime("2005-04-11 17:41:30"),
                        Utils.convertStringToLocalDateTime("2019-12-19 16:23:54"),
                        new TreeSet<>(),
                        new TreeSet<>()
                ), Utils.convertStringToLocalDateTime("2020-11-11 07:52:21"), new BigDecimal("248374.57"));
        OrderDto expected2 =
                new OrderDto(10L, 7L, 10L, new UserDto(7L, "Leesa Rope", new TreeSet<>()), new GiftCertificateDto(
                        10L,
                        "Otcom",
                        "vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis",
                        new BigDecimal("872504.15"),
                        6,
                        Utils.convertStringToLocalDateTime("2006-03-23 13:29:43"),
                        Utils.convertStringToLocalDateTime("2020-01-13 12:51:39"),
                        new TreeSet<>(),
                        new TreeSet<>()
                ), Utils.convertStringToLocalDateTime("2020-08-16 19:50:29"), new BigDecimal("777855.51"));

        List<OrderDto> expectedSortedAsc = List.of(expected1, expected2);
        List<OrderDto> expectedSortedDesc = List.of(expected2, expected1);

        Optional<List<OrderDto>> resultSortedAsc = repository.findOrdersById(5, 2, true);
        Optional<List<OrderDto>> resultSortedDesc = repository.findOrdersById(1, 2, false);

        softly.assertThat(resultSortedAsc.isPresent()).isTrue();
        softly.assertThat(resultSortedDesc.isPresent()).isTrue();
        softly.assertThat(resultSortedAsc.get()).isEqualTo(expectedSortedAsc);
        softly.assertThat(resultSortedDesc.get()).isEqualTo(expectedSortedDesc);
        softly.assertAll();
    }

    @Test
    void findOrdersById_notFound_returnsOptionalEmptyList() {
        Optional<List<OrderDto>> result = repository.findOrdersById(6, 2, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void findOrdersByUserId_found_returnsOptionalListOfDtoSorted() {
        SoftAssertions softly = new SoftAssertions();
        User user = new User();
        user.setId(1L);
        User[] users = {user};
        GiftCertificateDto expectedCertificate1 = expectedCertificateDto;
        OrderDto expected1 = new OrderDto(
                1L,
                1L,
                2L,
                expectedUserDto,
                expectedCertificate1,
                Utils.convertStringToLocalDateTime("2021-07-31 21:04:03"),
                new BigDecimal("778809.00")
        );
        OrderDto expected2 = new OrderDto(
                2L,
                1L,
                2L,
                expectedUserDto,
                expectedCertificate1,
                Utils.convertStringToLocalDateTime("2022-06-28 08:11:01"),
                new BigDecimal("852942.22")
        );

        Optional<List<OrderDto>> resultSortedAsc = repository.findOrdersByUserId(users, 1, 5, true);
        Optional<List<OrderDto>> resultSortedDesc = repository.findOrdersByUserId(users, 1, 2, false);

        List<OrderDto> expectedSortedAsc = List.of(expected1, expected2);
        List<OrderDto> expectedSortedDesc = List.of(expected2, expected1);

        softly.assertThat(resultSortedAsc.isPresent()).isTrue();
        softly.assertThat(resultSortedDesc.isPresent()).isTrue();
        softly.assertThat(resultSortedAsc.get()).isEqualTo(expectedSortedAsc);
        softly.assertThat(resultSortedDesc.get()).isEqualTo(expectedSortedDesc);
        softly.assertAll();
    }

    @Test
    void findOrdersByUserId_notFound_returnsOptionalEmptyList() {
        User user = new User();
        user.setId(11L);
        User[] users = {user};
        Optional<List<OrderDto>> result = repository.findOrdersByUserId(users, 1, 5, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

    @Test
    void findOrdersByCertificateId_found_returnsOptionalListOfDtoSortedById() {
        SoftAssertions softly = new SoftAssertions();
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(2L);
        GiftCertificate[] giftCertificate = {certificate};
        UserDto expectedUser = expectedUserDto;
        GiftCertificateDto expectedCertificate = expectedCertificateDto;
        OrderDto expected1 = new OrderDto(
                1L,
                1L,
                2L,
                expectedUser,
                expectedCertificate,
                Utils.convertStringToLocalDateTime("2021-07-31 21:04:03"),
                new BigDecimal("778809.00")
        );
        OrderDto expected2 = new OrderDto(
                2L,
                1L,
                2L,
                expectedUser,
                expectedCertificate,
                Utils.convertStringToLocalDateTime("2022-06-28 08:11:01"),
                new BigDecimal("852942.22")
        );

        List<OrderDto> expectedSortedAsc = List.of(expected1, expected2);
        List<OrderDto> expectedSortedDesc = List.of(expected2, expected1);

        Optional<List<OrderDto>> resultSortedAsc = repository.findOrdersByCertificateId(giftCertificate, 1, 5, true);
        Optional<List<OrderDto>> resultSortedDesc = repository.findOrdersByCertificateId(giftCertificate, 1, 2, false);

        softly.assertThat(resultSortedAsc.isPresent()).isTrue();
        softly.assertThat(resultSortedDesc.isPresent()).isTrue();
        softly.assertThat(resultSortedAsc.get()).isEqualTo(expectedSortedAsc);
        softly.assertThat(resultSortedDesc.get()).isEqualTo(expectedSortedDesc);
        softly.assertAll();
    }

    @Test
    void findOrdersByCertificateId_notFound_returnsOptionalEmptyList() {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(22L);
        GiftCertificate[] giftCertificate = {certificate};

        Optional<List<OrderDto>> result = repository.findOrdersByCertificateId(giftCertificate, 1, 5, true);

        assertTrue(result.isPresent());
        assertTrue(result.get().isEmpty());
    }

}
