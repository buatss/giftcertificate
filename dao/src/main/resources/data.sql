insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Zontrax', 'volutpat eleifend donec ut', 187, 428532.97, '2008-11-25 01:17:21', '2015-09-23 23:15:35');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Tempsoft', 'quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec', 209,
 382662.75, '2007-06-23 10:42:42', '2013-10-25 15:40:48');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Bitwolf', 'at diam nam tristique', 228, 439449.0, '2010-05-02 09:35:05', '2018-08-14 08:32:31');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Kanlam', 'pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet', 226, 443786.01,
'2008-03-24 15:40:34', '2022-01-17 19:56:02');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Flexidy', 'iaculis justo in hac habitasse platea dictumst etiam faucibus', 192, 20871.14, '2008-06-23 20:53:18',
'2019-09-04 09:41:12');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Bytecard', 'lacinia erat vestibulum sed', 108, 12895.69, '2005-04-11 17:41:30', '2019-12-19 16:23:54');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Sonair', 'fermentum donec ut', 289, 959288.91, '2002-04-15 16:09:46', '2013-05-18 23:42:11');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Ventosanzap', 'cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut', 262, 594793.97,
'2001-01-12 04:59:45', '2019-11-09 08:24:10');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Vagram', 'pellentesque volutpat dui maecenas tristique est et tempus semper est', 194, 514364.03,
'2009-04-22 09:24:10', '2017-07-14 06:00:28');
insert into certificates (name, description, duration, price, createDate, lastUpdateDate) values
('Otcom', 'vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis', 6, 872504.15,
'2006-03-23 13:29:43', '2020-01-13 12:51:39');

insert into tags (name) values ('Fregata magnificans');
insert into tags (name) values ('Macropus robustus');
insert into tags (name) values ('Eumetopias jubatus');
insert into tags (name) values ('Suricata suricatta');
insert into tags (name) values ('Lybius torquatus');
insert into tags (name) values ('Tayassu tajacu');
insert into tags (name) values ('Chloephaga melanoptera');
insert into tags (name) values ('Ictalurus furcatus');
insert into tags (name) values ('Passer domesticus');
insert into tags (name) values ('Macropus rufogriseus');

insert into users (userName, password) values ('Donnell Bissell', 'bXWXSBI1l2');
insert into users (userName, password) values ('Kirk Melendez', 'jWzKkI3Hkl9K');
insert into users (userName, password) values ('Sherilyn Watts', 'l1wQCw5a5j1');
insert into users (userName, password) values ('Row Romand', '6EyYGUC');
insert into users (userName, password) values ('Christye Sabie', 'MfkeGl5');
insert into users (userName, password) values ('Henriette Chivers', 'tQNV8V');
insert into users (userName, password) values ('Leesa Rope', 'QNoSGVHHwYRk');
insert into users (userName, password) values ('Jourdain Ranson', 'kefwmMpI');
insert into users (userName, password) values ('Zena Dainty', 'jxIZ672');
insert into users (userName, password) values ('Wylma Impy', '1yos7JwNGu');

insert into certificate_tag (tag_id, giftcertificate_id) values (1, 2);
insert into certificate_tag (tag_id, giftcertificate_id) values (1, 4);
insert into certificate_tag (tag_id, giftcertificate_id) values (1, 3);
insert into certificate_tag (tag_id, giftcertificate_id) values (4, 2);
insert into certificate_tag (tag_id, giftcertificate_id) values (5, 5);
insert into certificate_tag (tag_id, giftcertificate_id) values (5, 9);
insert into certificate_tag (tag_id, giftcertificate_id) values (4, 4);
insert into certificate_tag (tag_id, giftcertificate_id) values (7, 3);
insert into certificate_tag (tag_id, giftcertificate_id) values (8, 6);
insert into certificate_tag (tag_id, giftcertificate_id) values (9, 7);
insert into certificate_tag (tag_id, giftcertificate_id) values (10, 10);

insert into orders (giftCertificate_id, user_id, price, purchase_time) values (2, 1, 778809.0, '2021-07-31 21:04:03');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (2, 1, 852942.22, '2022-06-28 08:11:01');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (6, 2, 272880.19, '2020-01-16 18:03:47');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (4, 3, 755871.0, '2021-12-02 03:24:52');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (6, 2, 511754.41, '2021-07-25 05:28:10');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (6, 2, 397390.48, '2022-01-16 13:47:32');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (6, 7, 365021.78, '2020-04-08 17:41:05');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (6, 6, 791621.35, '2020-06-23 13:57:57');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (6, 2, 248374.57, '2020-11-11 07:52:21');
insert into orders (giftCertificate_id, user_id, price, purchase_time) values (10, 7, 777855.51, '2020-08-16 19:50:29');
