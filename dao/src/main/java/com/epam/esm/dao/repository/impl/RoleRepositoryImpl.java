package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.model.entity.Role;
import com.epam.esm.dao.repository.RoleRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Locale;
import java.util.Optional;

/**
 * This provides CR operations on Role entity.
 */
@Repository
public class RoleRepositoryImpl implements RoleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<Role> createRole(Role role) {
        role.setName(role.getName().toLowerCase());
        entityManager.persist(role);
        return Optional.of(entityManager.find(Role.class, role.getId()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<Role> findRoleByName(Role role) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Role> cq = cb.createQuery(Role.class);
        Root<Role> root = cq.from(Role.class);
        cq = cq.select(root).where(cb.equal(root.get("name"), role.getName().toLowerCase(Locale.ROOT)));
        TypedQuery<Role> query = entityManager.createQuery(cq);
        try {
            return Optional.of(query.getSingleResult());
        } catch (PersistenceException e) {
            return Optional.empty();
        }
    }
}
