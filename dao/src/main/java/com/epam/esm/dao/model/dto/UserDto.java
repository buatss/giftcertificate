package com.epam.esm.dao.model.dto;

import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Objects;
import java.util.TreeSet;

/**
 * A DTO for the {@link com.epam.esm.dao.model.entity.User} entity
 */
public class UserDto extends RepresentationModel<UserDto> implements Serializable, Comparable<UserDto> {
    private Long id;
    private String userName;
    private TreeSet<OrderDto> orders = new TreeSet<>();

    public UserDto() {
    }

    public UserDto(Long id, String userName, TreeSet<OrderDto> orders) {
        this.id = id;
        this.userName = userName;
        this.orders = orders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TreeSet<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(TreeSet<OrderDto> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDto entity = (UserDto) o;
        return Objects.equals(this.id, entity.id) && Objects.equals(this.userName, entity.userName) &&
                Objects.equals(this.orders, entity.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, orders);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + "id = " + id + ", " + "userName = " + userName + ", " + "orders = " +
                orders + ")";
    }

    @Override
    public int compareTo(UserDto o) {
        return this.getId().compareTo(o.getId());
    }
}
