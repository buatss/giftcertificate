package com.epam.esm.dao.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.epam.esm.dao.model.entity.Order} entity
 */
public class OrderDto extends RepresentationModel<OrderDto> implements Serializable, Comparable<OrderDto> {
    private Long id;
    private Long userId;
    private Long certificateId;
    @JsonIgnoreProperties("orders")
    private UserDto user;
    @JsonIgnoreProperties({"tags", "orders"})
    private GiftCertificateDto giftCertificate;
    private LocalDateTime purchase_time;
    private BigDecimal price;

    public OrderDto() {
    }

    public OrderDto(Long id, Long userId, Long certificateId, UserDto user, GiftCertificateDto giftCertificate,
            LocalDateTime purchase_time, BigDecimal price) {
        this.id = id;
        this.userId = userId;
        this.certificateId = certificateId;
        this.user = user;
        this.giftCertificate = giftCertificate;
        this.purchase_time = purchase_time;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public GiftCertificateDto getGiftCertificate() {
        return giftCertificate;
    }

    public void setGiftCertificate(GiftCertificateDto giftCertificate) {
        this.giftCertificate = giftCertificate;
    }

    public LocalDateTime getPurchase_time() {
        return purchase_time;
    }

    public void setPurchase_time(LocalDateTime purchase_time) {
        this.purchase_time = purchase_time;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderDto entity = (OrderDto) o;
        return Objects.equals(this.id, entity.id) && Objects.equals(this.userId, entity.userId) &&
                Objects.equals(this.certificateId, entity.certificateId) && Objects.equals(this.user, entity.user) &&
                Objects.equals(this.giftCertificate, entity.giftCertificate) &&
                Objects.equals(this.purchase_time, entity.purchase_time) && Objects.equals(this.price, entity.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, certificateId, user, giftCertificate, purchase_time, price);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + "id = " + id + ", " + "userId = " + userId + ", " + "certificateId " +
                "= " + certificateId + ", " + "user = " + user + ", " + "giftCertificate = " + giftCertificate + "," +
                " " + "purchase_time = " + purchase_time + ", " + "price = " + price + ")";
    }

    @Override
    public int compareTo(OrderDto o) {
        return this.getId().compareTo(o.getId());
    }
}
