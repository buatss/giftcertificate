package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.model.converter.EntityDtoConverter;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.GiftCertificateRepository;
import com.epam.esm.dao.repository.impl.utils.CertificateTagUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Objects.isNull;
import static org.apache.logging.log4j.util.Strings.isEmpty;

/**
 * This provides performing CRUD operations on GiftCertificate entity, pagination is supported for read operations
 */
@Repository
public class GiftCertificateRepositoryImpl implements GiftCertificateRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<GiftCertificateDto> createGiftCertificate(GiftCertificate certificate) {
        try {
            entityManager.persist(certificate);
            certificate = entityManager.find(GiftCertificate.class, certificate.getId());
        } catch (PersistenceException e) {
            return Optional.empty();
        }
        return Optional.of(EntityDtoConverter.convertEntityToDto(certificate, true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<GiftCertificateDto> connectCertificateAndTag(GiftCertificate certificate, Tag tag) {
        try {
            certificate = entityManager.find(GiftCertificate.class, certificate.getId());
            tag = entityManager.find(Tag.class, tag.getId());
            if (CertificateTagUtils.isBidirectionalRelation(certificate, tag)) {
                return Optional.empty();
            }
        } catch (NullPointerException e) {
            return Optional.empty();
        }
        certificate.getTags().add(tag);
        entityManager.flush();
        return Optional.of(EntityDtoConverter.convertEntityToDto(certificate, true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<GiftCertificateDto> findGiftCertificateById(GiftCertificate certificate) {
        try {
            return Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.find(GiftCertificate.class,
                    certificate.getId()
            ), true));
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<GiftCertificateDto>> findGiftCertificatesById(int pageNumber, int pageSize,
            boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> cq = cb.createQuery(GiftCertificate.class);
        Root<GiftCertificate> rootEntry = cq.from(GiftCertificate.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(rootEntry.get("id")));
        } else {
            cq.orderBy(cb.desc(rootEntry.get("id")));
        }
        cq.distinct(true);
        TypedQuery<GiftCertificate> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertCertificateEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<GiftCertificateDto>> searchByIdsAndNames(GiftCertificate[] certificates, int pageNumber,
            int pageSize, boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> cq = cb.createQuery(GiftCertificate.class);
        Root<GiftCertificate> root = cq.from(GiftCertificate.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        Stream<Predicate> predicateIds = Arrays.stream(certificates)
                                               .map(GiftCertificate::getId)
                                               .filter(id -> !isNull(id))
                                               .map(id -> cb.equal(root.get("id"), id));
        Stream<Predicate> predicateNames = Arrays.stream(certificates)
                                                 .map(GiftCertificate::getName)
                                                 .filter(name -> !isNull(name) && !isEmpty(name))
                                                 .map(name -> cb.like(root.get("name"), name + "%"));
        Predicate[] predicateAll = Stream.concat(predicateIds, predicateNames).toArray(Predicate[]::new);
        cq.distinct(true);
        cq.where(cb.or(predicateAll));
        TypedQuery<GiftCertificate> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.of(EntityDtoConverter.convertCertificateEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<GiftCertificateDto>> searchByManyTagsIdAndName(Tag[] tags, int pageNumber, int pageSize,
            boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> cq = cb.createQuery(GiftCertificate.class);
        Metamodel m = entityManager.getMetamodel();
        EntityType<GiftCertificate> metaModel = m.entity(GiftCertificate.class);
        Root<GiftCertificate> rootCertificates = cq.from(GiftCertificate.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(rootCertificates.get("id")));
        } else {
            cq.orderBy(cb.desc(rootCertificates.get("id")));
        }
        cq.distinct(true);
        Join<GiftCertificate, Tag> rootTags = rootCertificates.join(metaModel.getSet("tags", Tag.class));
        Stream<Predicate> predicateForIds = Arrays.stream(tags)
                                                  .map(Tag::getId)
                                                  .filter(id -> !isNull(id))
                                                  .map(id -> cb.equal(rootTags.get("id"), id));
        Stream<Predicate> predicateForNames = Arrays.stream(tags)
                                                    .map(Tag::getName)
                                                    .filter(name -> !isNull(name) && !isEmpty(name))
                                                    .map(name -> cb.like(rootTags.get("name"), name + "%"));
        Predicate[] predicateAll = Stream.concat(predicateForIds, predicateForNames).toArray(Predicate[]::new);
        cq.where(cb.or(predicateAll));
        TypedQuery<GiftCertificate> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.of(EntityDtoConverter.convertCertificateEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<GiftCertificateDto>> findUnassignedCertificates(int pageNumber, int pageSize,
            boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<GiftCertificate> cq = cb.createQuery(GiftCertificate.class);
        Root<GiftCertificate> root = cq.from(GiftCertificate.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        Predicate predicate = cb.isEmpty(root.get("tags"));
        cq.where(predicate);
        TypedQuery<GiftCertificate> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.of(EntityDtoConverter.convertCertificateEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<GiftCertificateDto> updateGiftCertificate(GiftCertificate certificate) {
        GiftCertificate oldCertificate;
        try {
            oldCertificate = entityManager.find(GiftCertificate.class, certificate.getId());
        } catch (NullPointerException | IllegalArgumentException e) {
            return Optional.empty();
        }
        return Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.merge(mergeCertificatesToUpdate(oldCertificate,
                certificate
        )), true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<GiftCertificateDto> deleteGiftCertificateById(GiftCertificate certificate) {
        try {
            certificate = entityManager.find(GiftCertificate.class, certificate.getId());
            entityManager.remove(certificate);
        } catch (NullPointerException | IllegalArgumentException e) {
            return Optional.empty();
        }
        return Optional.of(EntityDtoConverter.convertEntityToDto(certificate, true));
    }

    private GiftCertificate mergeCertificatesToUpdate(GiftCertificate oldCertificate, GiftCertificate newCertificate) {
        if (newCertificate.getName() != null) {
            oldCertificate.setName(newCertificate.getName());
        }
        if (newCertificate.getDescription() != null) {
            oldCertificate.setDescription(newCertificate.getDescription());
        }
        if (newCertificate.getDuration() != null) {
            oldCertificate.setDuration(newCertificate.getDuration());
        }
        if (newCertificate.getPrice() != null) {
            oldCertificate.setPrice(newCertificate.getPrice());
        }
        if (newCertificate.getTags() != null) {
            oldCertificate.setTags(newCertificate.getTags());
        }
        return oldCertificate;
    }
}
