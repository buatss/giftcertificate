package com.epam.esm.dao.model.dto;

import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * A DTO for the {@link com.epam.esm.dao.model.entity.Tag} entity
 */
public class TagDto extends RepresentationModel<TagDto> implements Serializable, Comparable<TagDto> {
    private Long id;
    private String name;
    private TreeSet<GiftCertificateDto> certificates = new TreeSet<>();

    public TagDto() {
    }

    public TagDto(Long id, String name, TreeSet<GiftCertificateDto> certificates) {
        this.id = id;
        this.name = name;
        this.certificates = certificates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<GiftCertificateDto> getCertificates() {
        return certificates;
    }

    public void setCertificates(TreeSet<GiftCertificateDto> certificates) {
        this.certificates = certificates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TagDto entity = (TagDto) o;
        return Objects.equals(this.id, entity.id) && Objects.equals(this.name, entity.name) &&
                Objects.equals(this.certificates, entity.certificates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, certificates);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + "id = " + id + ", " + "name = " + name + ", " + "certificates = " +
                certificates + ")";
    }

    @Override
    public int compareTo(TagDto o) {
        return this.getId().compareTo(o.getId());
    }
}
