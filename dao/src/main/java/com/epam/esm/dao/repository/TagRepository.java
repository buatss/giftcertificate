package com.epam.esm.dao.repository;

import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;

import java.util.List;
import java.util.Optional;

/**
 * Implementing this interface allows to do CRD operations on Tag entity.
 * <p>
 * NOTE: Some attributes are mandatory and some redundant, read @param description for details.
 */
public interface TagRepository {

    /**
     * This persists Tag record in database initially without any association then retrieves its dto.
     *
     * @param tag not null with not null, not empty and not duplicated name.
     * @return Optional TagDto in case of success, otherwise empty.
     */
    Optional<TagDto> createTag(Tag tag);

    /**
     * This associates persisted records in database GiftCertificate and Tag by theirs primary keys(id) then retrieves
     * tag's dto.
     *
     * @param certificate not null with not null id.
     * @param tag not null with not null id.
     * @return Optional TagDto in case of success, otherwise empty.
     */
    Optional<TagDto> connectTagAndCertificate(GiftCertificate certificate, Tag tag);

    /**
     * This retrieves Tag record by primary key(id) from database and returns dto.
     *
     * @param tag not null with not null id of persisted tag in db.
     * @return Optional TagDto in case of success, otherwise empty.
     */
    Optional<TagDto> findTagById(Tag tag);

    /**
     * This retrieves collection of TagDto sorted by id which are in range of page parameters(offset and limit).
     * If no record found retrieved collection is empty.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of TagDto.
     */
    Optional<List<TagDto>> findTagsById(int pageNumber, int pageSize, boolean sortAscending);

    /**
     * This retrieves tags searched by theirs id and name, where searching by name is by matching prefix.
     * If no record found retrieved collection is empty.
     *
     * @param tags not null with not null nor empty name or id.
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of TagDto persisted in database otherwise empty.
     */
    Optional<List<TagDto>> findTagsByNamesAndIds(Tag[] tags, int pageNumber, int pageSize, boolean sortAscending);

    /**
     * This retrieves collection of unassociated certificates dtos from database.
     * If no record found retrieved collection is empty.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of TagDto if found, otherwise Optional.empty().
     */
    Optional<List<TagDto>> findUnassignedTags(int pageNumber, int pageSize, boolean sortAscending);

    /**
     * This deletes record from database then retrieves its dto.
     *
     * @param tag not null with not null id.
     * @return Optional DTO of deleted Tag otherwise empty even if tag wasn't persisted in database before.
     */
    Optional<TagDto> deleteTagById(Tag tag);

    /**
     * This finds most frequent tag associated to certificates from orders of user, who spent most money of all.
     *
     * @return Optional TagDto.
     */
    Optional<TagDto> findMostFrequentTagFromUserWithMostExpensiveOrdersTotal();
}
