package com.epam.esm.dao.repository.impl.utils;

import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;

/**
 * This provides utility methods which couldn't be exclusively assigned to any other class.
 */
public class CertificateTagUtils {
    /**
     * This checks association between GiftCertificate and Tag retrieved from database.
     *
     * @param certificate not null with not null id and tags
     * @param tag not null with not null id and certificates
     * @return true if certificate is present in tag's set and vice versa, otherwise returns false
     */
    public static boolean isBidirectionalRelation(GiftCertificate certificate, Tag tag) {
        final Long cid = certificate.getId();
        final Long tid = tag.getId();
        boolean certificateHasTag = certificate.getTags().stream().map(Tag::getId).anyMatch(id -> id.equals(tid));
        boolean tagHasCertificate =
                tag.getCertificates().stream().map(GiftCertificate::getId).anyMatch(id -> id.equals(cid));
        return certificateHasTag && tagHasCertificate;
    }
}
