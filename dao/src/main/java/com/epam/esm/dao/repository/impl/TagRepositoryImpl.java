package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.model.converter.EntityDtoConverter;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.TagRepository;
import com.epam.esm.dao.repository.impl.utils.CertificateTagUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Stream;

/**
 * This provides performing CRD operations on Tag entity, pagination is supported for read operations excluding
 * searching most widely used tag in certificates bought by user, who spent most money in total
 */
@Repository
public class TagRepositoryImpl implements TagRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<TagDto> createTag(Tag tag) {
        try {
            entityManager.persist(tag);
        } catch (PersistenceException | IllegalArgumentException e) {
            return Optional.empty();
        }
        return Optional.of(EntityDtoConverter.convertEntityToDto(tag, true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<TagDto> connectTagAndCertificate(GiftCertificate certificate, Tag tag) {
        try {
            certificate = entityManager.find(GiftCertificate.class, certificate.getId());
            tag = entityManager.find(Tag.class, tag.getId());
            if (CertificateTagUtils.isBidirectionalRelation(certificate, tag)) {
                return Optional.empty();
            }
        } catch (NullPointerException e) {
            return Optional.empty();
        }
        tag.getCertificates().add(certificate);
        entityManager.flush();
        return Optional.of(EntityDtoConverter.convertEntityToDto(tag, true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<TagDto> findTagById(Tag tag) {
        try {
            return Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.find(Tag.class, tag.getId()), true));
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<TagDto>> findTagsById(int pageNumber, int pageSize, boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
        Root<Tag> root = cq.from(Tag.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        CriteriaQuery<Tag> all = cq.select(root);
        TypedQuery<Tag> query = entityManager.createQuery(all);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertTagEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<TagDto>> findTagsByNamesAndIds(Tag[] tags, int pageNumber, int pageSize,
            boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
        Root<Tag> root = cq.from(Tag.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        Stream<Predicate> predicateIds = Arrays.stream(tags).map(Tag::getId).map(id -> cb.equal(root.get("id"), id));
        Stream<Predicate> predicateNames =
                Arrays.stream(tags).map(Tag::getName).map(name -> cb.like(root.get("name"), name + "%"));
        Predicate[] predicateAll = Stream.concat(predicateIds, predicateNames).toArray(Predicate[]::new);
        cq.where(cb.or(predicateAll));
        CriteriaQuery<Tag> all = cq.select(root);
        TypedQuery<Tag> query = entityManager.createQuery(all);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertTagEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<TagDto>> findUnassignedTags(int pageNumber, int pageSize, boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> cq = cb.createQuery(Tag.class);
        Root<Tag> root = cq.from(Tag.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        Predicate predicate = cb.isEmpty(root.get("certificates"));
        cq.where(predicate);
        TypedQuery<Tag> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.of(EntityDtoConverter.convertTagEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<TagDto> deleteTagById(Tag tag) {
        try {
            tag = entityManager.find(Tag.class, tag.getId());
            entityManager.remove(tag);
        } catch (NullPointerException | IllegalArgumentException e) {
            return Optional.empty();
        }
        return Optional.of(EntityDtoConverter.convertEntityToDto(tag, true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<TagDto> findMostFrequentTagFromUserWithMostExpensiveOrdersTotal() {
        Query query = entityManager.createNamedQuery("users.getIdOfUserWithHighestCostOfOrders");
        long val;
        try {
            val = Long.parseLong(query.getSingleResult().toString());
        } catch (NoResultException e) {
            return Optional.empty();
        }
        Optional<UserDto> user =
                Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.find(User.class, val), true));
        Set<OrderDto> orders = user.get().getOrders();

        Map<TagDto, Long> tagOccurs = new HashMap<>();
        orders.stream()
              .map(OrderDto::getCertificateId)
              .map(id -> entityManager.find(GiftCertificate.class, id))
              .map(certificate -> EntityDtoConverter.convertEntityToDto(certificate, true))
              .flatMap(dto -> dto.getTags().stream())
              .forEach(dto -> {
                  tagOccurs.putIfAbsent(dto, 1L);
                  tagOccurs.computeIfPresent(dto, (key, value) -> ++value);
              });

        TagDto dto = tagOccurs.entrySet()
                              .stream()
                              .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
                              .get()
                              .getKey();
        return Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.find(Tag.class, dto.getId()), true));
    }
}
