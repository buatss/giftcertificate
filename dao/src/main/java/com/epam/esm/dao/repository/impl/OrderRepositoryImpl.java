package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.model.converter.EntityDtoConverter;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.OrderRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * This provides performing CR operations on Order entity, pagination is supported for read operations
 */
@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<OrderDto> createOrder(Long certificateId, Long userId) {
        Order order;
        try {
            GiftCertificate certificate = entityManager.find(GiftCertificate.class, certificateId);
            User user = entityManager.find(User.class, userId);
            order = new Order();
            order.setUser(user);
            order.setGiftCertificate(certificate);
            order.setPrice(certificate.getPrice());
            order = entityManager.merge(order);
            entityManager.persist(order);
        } catch (NullPointerException | IllegalArgumentException e) {
            return Optional.empty();
        }
        return Optional.of(EntityDtoConverter.convertEntityToDto(order, true));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<OrderDto> findOrderById(Order order) {
        TypedQuery<Order> query = entityManager.createNamedQuery("orders.findByOrderId", Order.class);
        query.setParameter("id", order.getId());
        try {
            return Optional.of(EntityDtoConverter.convertEntityToDto(query.getSingleResult(), true));
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<OrderDto>> findOrdersById(Integer pageNumber, Integer pageSize, boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);
        Root<Order> root = cq.from(Order.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        TypedQuery<Order> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertOrderEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<OrderDto>> findOrdersByUserId(User[] users, int pageNumber, int pageSize,
            boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);
        Root<Order> root = cq.from(Order.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        Predicate[] predicateAll = Arrays.stream(users)
                                         .map(User::getId)
                                         .map(id -> cb.equal(root.get("user").get("id"), id))
                                         .toArray(Predicate[]::new);
        cq.where(cb.or(predicateAll));
        TypedQuery<Order> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertOrderEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<OrderDto>> findOrdersByCertificateId(GiftCertificate[] certificates, int pageNumber,
            int pageSize, boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);
        Root<Order> root = cq.from(Order.class);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        Predicate[] predicateAll = Arrays.stream(certificates)
                                         .map(GiftCertificate::getId)
                                         .map(id -> cb.equal(root.get("giftCertificate").get("id"), id))
                                         .toArray(Predicate[]::new);
        cq.where(cb.or(predicateAll));
        TypedQuery<Order> query = entityManager.createQuery(cq);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertOrderEntitiesToDto(query.getResultList()));
    }
}
