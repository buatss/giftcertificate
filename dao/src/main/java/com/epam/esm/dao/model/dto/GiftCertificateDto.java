package com.epam.esm.dao.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * A DTO for the {@link com.epam.esm.dao.model.entity.GiftCertificate} entity
 */
public class GiftCertificateDto extends RepresentationModel<GiftCertificateDto>
        implements Serializable, Comparable<GiftCertificateDto> {
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Integer duration;
    private LocalDateTime createDate;
    private LocalDateTime lastUpdateDate;
    @JsonIgnoreProperties("certificates")
    private TreeSet<TagDto> tags = new TreeSet<>();
    @JsonIgnoreProperties({"user", "giftCertificate"})
    private TreeSet<OrderDto> orders = new TreeSet<>();

    public GiftCertificateDto() {
    }

    public GiftCertificateDto(Long id, String name, String description, BigDecimal price, Integer duration,
            LocalDateTime createDate, LocalDateTime lastUpdateDate, TreeSet<TagDto> tags, TreeSet<OrderDto> orders) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.duration = duration;
        this.createDate = createDate;
        this.lastUpdateDate = lastUpdateDate;
        this.tags = tags;
        this.orders = orders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Set<TagDto> getTags() {
        return tags;
    }

    public void setTags(TreeSet<TagDto> tags) {
        this.tags = tags;
    }

    public Set<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(TreeSet<OrderDto> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GiftCertificateDto entity = (GiftCertificateDto) o;
        return Objects.equals(this.id, entity.id) && Objects.equals(this.name, entity.name) &&
                Objects.equals(this.description, entity.description) && Objects.equals(this.price, entity.price) &&
                Objects.equals(this.duration, entity.duration) && Objects.equals(this.createDate, entity.createDate) &&
                Objects.equals(this.lastUpdateDate, entity.lastUpdateDate) && Objects.equals(this.tags, entity.tags) &&
                Objects.equals(this.orders, entity.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, duration, createDate, lastUpdateDate, tags, orders);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + "id = " + id + ", " + "name = " + name + ", " + "description = " +
                description + ", " + "price = " + price + ", " + "duration = " + duration + ", " + "createDate = " +
                createDate + ", " + "lastUpdateDate = " + lastUpdateDate + ", " + "tags = " + tags + ", " +
                "orders = " + orders + ")";
    }

    @Override
    public int compareTo(GiftCertificateDto o) {
        return this.getId().compareTo(o.getId());
    }
}
