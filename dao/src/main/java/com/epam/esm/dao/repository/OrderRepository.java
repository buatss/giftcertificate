package com.epam.esm.dao.repository;

import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * Implementing this interface allows to do CR operations on Order entity.
 */
public interface OrderRepository {

    /**
     * This persists Order record in database initially with associations then retrieves its dto.
     *
     * @param certificateId not null id of certificate persisted in db.
     * @param userId not null id of user persisted in db.
     * @return Optional OderDto in case of success or empty in case of wrong arguments.
     */
    Optional<OrderDto> createOrder(Long certificateId, Long userId);

    /**
     * This retrieves Order record by primary key(id) from database and returns dto.
     *
     * @param order with not null id of persisted order in db.
     * @return Optional OrderDto if found, otherwise empty.
     */
    Optional<OrderDto> findOrderById(Order order);

    /**
     * This retrieves collection of OrderDto sorted by id which are in range of page parameters(offset
     * and limit).
     * If no record found retrieved collection is empty.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional list of OrderDto.
     */
    Optional<List<OrderDto>> findOrdersById(Integer pageNumber, Integer pageSize, boolean sortAscending);

    /**
     * This retrieves collection of OrderDto found by User's id sorted by id which are in range of page
     * parameters(offset and limit).
     * If no record found retrieved collection is empty.
     *
     * @param users with not null id of persisted user in db.
     * @param pageNumber number of displayed page.
     * @param pageSize size of displayed page.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of OrderDto if found, otherwise empty.
     */
    Optional<List<OrderDto>> findOrdersByUserId(User[] users, int pageNumber, int pageSize, boolean sortAscending);

    /**
     * This retrieves collection of OrderDto found by GiftCertificate's id sorted by id which are in range of page
     * parameters(offset and limit).
     * If no record found retrieved collection is empty.
     *
     * @param certificate with not null id of persisted certificate in db.
     * @param pageNumber number of displayed page.
     * @param pageSize size of displayed page.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of OrderDto if found, otherwise empty.
     */
    Optional<List<OrderDto>> findOrdersByCertificateId(GiftCertificate[] certificate, int pageNumber, int pageSize,
            boolean sortAscending);

}
