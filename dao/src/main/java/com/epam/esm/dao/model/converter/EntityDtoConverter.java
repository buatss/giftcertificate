package com.epam.esm.dao.model.converter;

import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.model.entity.User;

import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * This provides conversion methods from Entity to DTO.
 */
public class EntityDtoConverter {
    /**
     * This converts GiftCertificate entity to GiftCertificateDto, which holds maximum one further reference to
     * associated records in contrast to entity which has circular references.
     *
     * @param entity not null with not null attributes(excluding tags, orders).
     * @param withNested if true converts also tags and orders to DTO(theirs subsets will be empty), if false tags and
     * orders will be set to new TreeSet.
     * @return GiftCertificateDto.
     */
    public static GiftCertificateDto convertEntityToDto(GiftCertificate entity, boolean withNested) {
        GiftCertificateDto dto = new GiftCertificateDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setDuration(entity.getDuration());
        dto.setPrice(entity.getPrice());
        dto.setLastUpdateDate(entity.getLastUpdateDate());
        dto.setCreateDate(entity.getCreateDate());
        setDtoSubsets(dto, entity, withNested);
        return dto;
    }

    /**
     * This converts Tag entity to TagDto, which holds maximum one further reference to associated records in contrast
     * to entity which has circular references.
     *
     * @param entity not null with not null attributes(excluding certificates).
     * @param withNested if true converts also certificates to DTO(theirs subsets will be empty), if false certificates
     * will be set to new TreeSet.
     * @return TagDto.
     */
    public static TagDto convertEntityToDto(Tag entity, boolean withNested) {
        TagDto dto = new TagDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        setDtoSubsets(dto, entity, withNested);
        return dto;
    }

    /**
     * This converts User entity to UserDto, which holds maximum one further reference to associated records in contrast
     * to entity which has circular references.
     *
     * @param entity not null with not null attributes(excluding orders).
     * @param withNested if true converts also orders to DTO(theirs subsets will be empty), if false orders will be set
     * to new TreeSet.
     * @return UserDto.
     */
    public static UserDto convertEntityToDto(User entity, boolean withNested) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setUserName(entity.getUserName());
        setDtoSubsets(dto, entity, withNested);
        return dto;
    }

    /**
     * This converts Order entity to OrderDto, which holds maximum one further reference to associated records in
     * contrast to entity which has circular references.
     *
     * @param entity not null with not null attributes(excluding giftCertificate and user).
     * @param withNested if true converts also giftCertificate and user to DTO(theirs subsets will be empty), if false
     * tags and orders are set to new TreeSet.
     * @return OrderDto.
     */
    public static OrderDto convertEntityToDto(Order entity, boolean withNested) {
        OrderDto dto = new OrderDto();
        dto.setId(entity.getId());
        dto.setUserId(entity.getUser().getId());
        dto.setCertificateId(entity.getGiftCertificate().getId());
        dto.setPrice(entity.getPrice());
        dto.setPurchase_time(entity.getPurchase_time());
        setDtoSubsets(dto, entity, withNested);
        return dto;
    }

    /**
     * This converts List of GiftCertificate entities to GiftCertificateDto, where each holds maximum one further
     * reference to associated records in contrast to entity which has circular references.
     *
     * @param resultList not null with not null attributes(excluding tags and orders).
     * @return List of GiftCertificateDto, if subset is not empty each element in this subset has its own empty subset.
     */
    public static List<GiftCertificateDto> convertCertificateEntitiesToDto(List<GiftCertificate> resultList) {
        return resultList.stream()
                         .map(certificate -> convertEntityToDto(certificate, true))
                         .collect(Collectors.toList());
    }

    /**
     * This converts List of Tag entities to TagDto, where each holds maximum one further reference to associated
     * records in contrast to entity which has circular references.
     *
     * @param resultList not null with not null attributes(excluding certificates).
     * @return List of GiftCertificateDto, if subset is not empty each element in this subset has its own empty subset.
     */
    public static List<TagDto> convertTagEntitiesToDto(List<Tag> resultList) {
        return resultList.stream().map(tag -> convertEntityToDto(tag, true)).collect(Collectors.toList());
    }

    /**
     * This converts List of Order entities to OrderDto, where each holds maximum one further reference to associated
     * records in contrast to entity which has circular references.
     *
     * @param resultList not null with not null attributes(excluding user and giftCertificate).
     * @return List of GiftCertificateDto, if subset is not empty each element in this subset has its own empty subset.
     */
    public static List<OrderDto> convertOrderEntitiesToDto(List<Order> resultList) {
        return resultList.stream().map(order -> convertEntityToDto(order, true)).collect(Collectors.toList());
    }

    /**
     * This converts List of User entities to UserDto, where each holds maximum one further reference to associated
     * records in contrast to entity which has circular references.
     *
     * @param result not null with not null attributes(excluding orders).
     * @return List of UserDto, if subset is not empty each element in nested subset will be empty.
     */
    public static List<UserDto> convertUserEntitiesToDto(List<User> result) {
        return result.stream().map(user -> convertEntityToDto(user, true)).collect(Collectors.toList());
    }

    private static void setTagsCertificates(Tag entity, TagDto dto) {
        dto.setCertificates(entity.getCertificates()
                                  .stream()
                                  .map(certificate -> convertEntityToDto(certificate, false))
                                  .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(
                                          GiftCertificateDto::getId)))));
    }

    private static void setUsersOrders(User entity, UserDto dto) {
        dto.setOrders(entity.getOrders()
                            .stream()
                            .map(order -> convertEntityToDto(order, false))
                            .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(OrderDto::getId)))));
    }

    private static void setCertificatesTags(GiftCertificate entity, GiftCertificateDto dto) {
        dto.setTags(entity.getTags()
                          .stream()
                          .map(tag -> convertEntityToDto(tag, false))
                          .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(TagDto::getId)))));
    }

    private static void setCertificatesOrders(GiftCertificate entity, GiftCertificateDto dto) {
        dto.setOrders(entity.getOrders()
                            .stream()
                            .map(order -> convertEntityToDto(order, false))
                            .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparingLong(OrderDto::getId)))));
    }

    private static void setDtoSubsets(OrderDto dto, Order entity, boolean withNested) {
        if (withNested) {
            try {
                dto.setUser(convertEntityToDto(entity.getUser(), false));
            } catch (NullPointerException e) {
                dto.setUser(new UserDto());
            }
            try {
                dto.setGiftCertificate(convertEntityToDto(entity.getGiftCertificate(), false));
            } catch (NullPointerException e) {
                dto.setGiftCertificate(new GiftCertificateDto());
            }
        } else {
            dto.setUser(new UserDto());
            dto.setGiftCertificate(new GiftCertificateDto());
        }
    }

    private static void setDtoSubsets(GiftCertificateDto dto, GiftCertificate entity, boolean withNested) {
        if (withNested) {
            try {
                setCertificatesTags(entity, dto);
            } catch (NullPointerException e) {
                dto.setTags(new TreeSet<>());
            }

            try {
                setCertificatesOrders(entity, dto);
            } catch (NullPointerException e) {
                dto.setOrders(new TreeSet<>());
            }
        } else {
            dto.setTags(new TreeSet<>());
            dto.setOrders(new TreeSet<>());
        }
    }

    private static void setDtoSubsets(UserDto dto, User entity, boolean withNested) {
        if (withNested) {
            setUsersOrders(entity, dto);
        } else {
            dto.setOrders(new TreeSet<>());
        }
    }

    private static void setDtoSubsets(TagDto dto, Tag entity, boolean withNested) {
        if (withNested) {
            setTagsCertificates(entity, dto);
        } else {
            dto.setCertificates(new TreeSet<>());
        }
    }

}
