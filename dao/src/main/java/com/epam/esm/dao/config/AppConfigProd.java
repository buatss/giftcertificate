package com.epam.esm.dao.config;

import com.mysql.cj.jdbc.Driver;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This configuration is destined to production use. If profile <b>prod</b> is chosen it provides pooled datasource and
 * necessary beans for JPA standard
 * <p>
 * Note: This configuration might be in conflict with dev configuration if used both at the same time.
 */
@Configuration
@Profile("prod")
@ComponentScan("com.epam.esm")
@PropertySource("classpath:database.properties")
public class AppConfigProd {

    private final String URL = "url";
    private final String USER = "dbuser";
    private final String PASSWORD = "dbpassword";
    private final String MAX_POOL = "maxpool";
    private final String MIN_POOL = "minpool";
    private final String MAX_STATEMENTS = "maxstatements";

    private final Environment environment;

    @Autowired
    public AppConfigProd(Environment environment) {
        this.environment = environment;
    }

    /**
     * This provides DataSource dedicated for production.
     *
     * @return DataSource used for production with internally pooled connections which are configured in
     * database.properties file.
     * @throws SQLException if driver is incorrect.
     */
    @Bean(name = "dataSource")
    DataSource dataSource() throws SQLException {
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(environment.getProperty(URL));
        ds.setUsername(environment.getProperty(USER));
        ds.setPassword(environment.getProperty(PASSWORD));
        Driver driver = new Driver();
        ds.setDriver(driver);
        ds.setMinIdle(Integer.parseInt(environment.getProperty(MIN_POOL)));
        ds.setMaxIdle(Integer.parseInt(environment.getProperty(MAX_POOL)));
        ds.setMaxOpenPreparedStatements(Integer.parseInt(environment.getProperty(MAX_STATEMENTS)));
        return ds;
    }

    /**
     * This provides instance of  LocalContainerEntityManagerFactoryBean configured with EmbeddedDatabase and
     * HibernateJpaVendorAdapter.
     *
     * @return {@link org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean}.
     * @throws SQLException in case of related to invalid datasource.
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.epam.esm");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    /**
     * This provides PlatformTransactionManager compatible with JPA standard.
     *
     * @return {@link org.springframework.transaction.PlatformTransactionManager}.
     * @throws SQLException in case of failure related to EntityManagerFactory.
     */
    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    /**
     * This provides PostProcessor translating native exceptions to spring's exceptions.
     *
     * @return {@link org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor}.
     */
    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("spring.jpa.defer-datasource-initialization", "true");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
        return properties;
    }

}
