package com.epam.esm.dao.config;

import org.springframework.context.annotation.*;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * This configuration is destined to development use. If profile <b>dev</b> is chosen it provides EmbeddedDatabase as
 * DataSource.
 * <p>
 * Note: This configuration might be in conflict with production configuration if used both at the same time.
 */
@Configuration
@Profile("dev")
@ComponentScan("com.epam.esm")
@PropertySource("classpath:database.properties")
public class AppConfigDev {

    /**
     * This provides DataSource dedicated for testing.
     *
     * @return DataSource which is singleton H2 EmbeddedDatabase.
     */
    @Bean(name = "dataSource")
    DataSource dataSource() {
        return testDB.getEmbeddedDatabase();
    }

    /**
     * This provides instance of  LocalContainerEntityManagerFactoryBean configured with EmbeddedDatabase and
     * HibernateJpaVendorAdapter.
     *
     * @return {@link org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean}.
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("com.epam.esm");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    /**
     * This provides PlatformTransactionManager compatible with JPA standard.
     *
     * @return {@link org.springframework.transaction.PlatformTransactionManager}.
     */
    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    /**
     * This provides PostProcessor translating native exceptions to spring's exceptions.
     *
     * @return {@link org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor}.
     */
    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create");
        properties.setProperty("spring.jpa.defer-datasource-initialization", "true");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        return properties;
    }

    private static class testDB {
        private static EmbeddedDatabase embeddedDatabase;

        private static void buildDB() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            builder.setType(EmbeddedDatabaseType.H2);
            embeddedDatabase = builder.build();
        }

        private static EmbeddedDatabase getEmbeddedDatabase() {
            if (embeddedDatabase == null) {
                buildDB();
            }
            return embeddedDatabase;
        }
    }
}
