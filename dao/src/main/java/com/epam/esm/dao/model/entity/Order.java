package com.epam.esm.dao.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * This is representation of Order entity in JPA standard.
 */
@Entity(name = "orders")
@NamedQuery(name = "orders.findByOrderId", query = "SELECT o FROM orders o WHERE o.id = :id")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.PERSIST})
    @JoinColumn
    @JsonIgnoreProperties("orders")
    private User user;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.PERSIST})
    @JoinColumn
    @JsonIgnoreProperties("tags")
    private GiftCertificate giftCertificate;

    @Column(nullable = false)
    @CreationTimestamp
    private LocalDateTime purchase_time;

    @Column(updatable = false, precision = 12, scale = 2, nullable = false)
    private BigDecimal price;

    public Order() {
    }

    public Order(Long id, User user, GiftCertificate giftCertificate, LocalDateTime purchase_time, BigDecimal price) {
        this.id = id;
        this.user = user;
        this.giftCertificate = giftCertificate;
        this.purchase_time = purchase_time;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GiftCertificate getGiftCertificate() {
        return giftCertificate;
    }

    public void setGiftCertificate(GiftCertificate giftCertificate) {
        this.giftCertificate = giftCertificate;
    }

    public LocalDateTime getPurchase_time() {
        return purchase_time;
    }

    public void setPurchase_time(LocalDateTime purchase_time) {
        this.purchase_time = purchase_time;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Order)) {
            return false;
        }

        Order order = (Order) o;

        if (getUser() != null ? !getUser().equals(order.getUser()) : order.getUser() != null) {
            return false;
        }
        if (getGiftCertificate() != null ? !getGiftCertificate().equals(order.getGiftCertificate()) :
                order.getGiftCertificate() != null) {
            return false;
        }
        if (getPurchase_time() != null ? !getPurchase_time().equals(order.getPurchase_time()) :
                order.getPurchase_time() != null) {
            return false;
        }
        return getPrice() != null ? getPrice().equals(order.getPrice()) : order.getPrice() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getPurchase_time() != null ? getPurchase_time().hashCode() : 0);
        result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", user=" + user + ", giftCertificate=" + giftCertificate + ", purchase_time=" +
                purchase_time + ", price=" + price + '}';
    }
}
