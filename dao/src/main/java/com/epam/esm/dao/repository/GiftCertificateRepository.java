package com.epam.esm.dao.repository;

import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;

import java.util.List;
import java.util.Optional;

/**
 * Implementing this interface allows to do CRUD operations on GiftCertificate entity.
 * <p>
 * NOTE: Some attributes are mandatory and some redundant, read @param description for details.
 */
public interface GiftCertificateRepository {
    /**
     * This persists GiftCertificate record in database initially without any association then retrieves its dto.
     *
     * @param certificate with null id, createDate, lastUpdateDate, tags and orders.
     * @return Optional of Dto object, if id is not null then returns optional empty.
     */
    Optional<GiftCertificateDto> createGiftCertificate(GiftCertificate certificate);

    /**
     * This associates persisted records in database GiftCertificate and Tag by theirs primary keys(id) then retrieves
     * certificate's dto.
     *
     * @param certificate with not null id of certificate persisted in db.
     * @param tag with not null id of tag persisted in db.
     * @return Optional GiftCertificateDto in case of success, otherwise optional empty(including case if pair passed as
     * arguments is already connected in db).
     */
    Optional<GiftCertificateDto> connectCertificateAndTag(GiftCertificate certificate, Tag tag);

    /**
     * This retrieves GiftCertificate record by primary key(id) from database and returns dto.
     *
     * @param certificate wit not null id of certificate persisted in db.
     * @return Optional GiftCertificateDto if found, otherwise empty.
     */
    Optional<GiftCertificateDto> findGiftCertificateById(GiftCertificate certificate);

    /**
     * This retrieves collection of GiftCertificateDto sorted by id which are in range of page parameters(offset
     * and limit).
     * If no record found retrieved collection is empty.
     *
     * @param offset positive integer.
     * @param limit positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of GiftCertificateDto.
     */
    Optional<List<GiftCertificateDto>> findGiftCertificatesById(int offset, int limit, boolean sortAscending);

    /**
     * This retrieves certificates searched by theirs id and name, where searching by name is by matching prefix.
     * If no record found retrieved collection is empty.
     *
     * @param certificates not null values, they should meet at least one requirement: not null nor empty name, not
     * null id, if array is empty returns Optional of empty list, if certificate is empty then is
     * ignored in searching.
     * @param pageNumber number of displayed page.
     * @param pageSize size of displayed page.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional list of certificates with specified name, Optional.empty() if not found any matching pattern.
     */
    Optional<List<GiftCertificateDto>> searchByIdsAndNames(GiftCertificate[] certificates, int pageNumber, int pageSize,
            boolean sortAscending);

    /**
     * This retrieves collection of certificates dtos searched by their tags' id and name, where searching by name is by
     * matching prefix.
     * If no record found retrieved collection is empty.
     *
     * @param tags not null values, they should meet at least one requirement: not null nor empty name, not
     * null id, if array is empty returns Optional of empty list, if tag is without any value then
     * is ignored.
     * @param pageNumber number of displayed page.
     * @param pageSize size of displayed page.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional list of certificates matching input parameters sorted, if there is no result then empty list is
     * returned.
     */
    Optional<List<GiftCertificateDto>> searchByManyTagsIdAndName(Tag[] tags, int pageNumber, int pageSize,
            boolean sortAscending);

    /**
     * This retrieves collection of unassociated certificates dtos from database.
     * If no record found retrieved collection is empty.
     *
     * @param pageNumber number of displayed page.
     * @param pageSize size of displayed page.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return optional list of unassigned certificates or Optional.empty() if any not found.
     */
    Optional<List<GiftCertificateDto>> findUnassignedCertificates(int pageNumber, int pageSize, boolean sortAscending);

    /**
     * This updates single record in database then retrieves its dto.
     *
     * @param certificate with not null id.
     * @return Optional dto of updated certificate, Optional.empty() in case of wrong argument or such certificate is
     * not found in database.
     */
    Optional<GiftCertificateDto> updateGiftCertificate(GiftCertificate certificate);

    /**
     * This deletes record from database then retrieves its dto.
     *
     * @param certificate with not null id.
     * @return Optional dto of certificate if successfully deleted, Optional.empty() if such certificate is not found in
     * database.
     */
    Optional<GiftCertificateDto> deleteGiftCertificateById(GiftCertificate certificate);
}
