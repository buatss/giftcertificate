package com.epam.esm.dao.repository;

import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * Implementing this interface allows to do CR operations on User entity.
 * <p>
 * Restriction: Create operation should be used only on initializing database, further usage is forbidden.
 */
public interface UserRepository {
    /**
     * This retrieves User record by primary key(id) from database and returns dto.
     *
     * @param id not null.
     * @return Optional UserDto if found, otherwise empty.
     */
    Optional<UserDto> findUserById(Long id);

    /**
     * This retrieves User record by name and returns as entity.
     *
     * @param name not null, nor empty name, case-sensitive.
     * @return Optional of User if found, otherwise empty.
     */
    Optional<User> findUserByName(String name);

    /**
     * This retrieves collection of GiftCertificateDto sorted by id which are in range of page parameters(offset
     * and limit).
     * If no record found retrieved collection is empty.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return Optional List of UserDto.
     */
    Optional<List<UserDto>> findUsersById(Integer pageNumber, Integer pageSize, boolean sortAscending);

    /**
     * This saves user to database.
     *
     * @param user with not null nor empty username & password
     * @return Optional of UserDto, in case of failure Optional.empty()
     */
    Optional<UserDto> saveUser(User user);
}
