package com.epam.esm.dao.repository.impl;

import com.epam.esm.dao.model.converter.EntityDtoConverter;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.UserRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * This provides performing Read operations on User entity, pagination is supported for read operations
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<UserDto> findUserById(Long id) {
        try {
            return Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.find(User.class, id), true));
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<User> findUserByName(String name) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        CriteriaQuery<User> all = cq.select(root);
        cq.where(cb.equal(root.get("userName"), name));
        TypedQuery<User> query = entityManager.createQuery(all);
        return Optional.ofNullable(query.getSingleResult());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<List<UserDto>> findUsersById(Integer pageNumber, Integer pageSize, boolean sortAscending) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        CriteriaQuery<User> all = cq.select(root);
        if (sortAscending) {
            cq.orderBy(cb.asc(root.get("id")));
        } else {
            cq.orderBy(cb.desc(root.get("id")));
        }
        TypedQuery<User> query = entityManager.createQuery(all);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return Optional.ofNullable(EntityDtoConverter.convertUserEntitiesToDto(query.getResultList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Optional<UserDto> saveUser(User user) {
        entityManager.persist(user);
        return Optional.of(EntityDtoConverter.convertEntityToDto(entityManager.find(User.class, user.getId()), true));
    }
}
