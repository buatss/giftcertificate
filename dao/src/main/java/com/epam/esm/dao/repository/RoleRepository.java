package com.epam.esm.dao.repository;

import com.epam.esm.dao.model.entity.Role;

import java.util.Optional;

/**
 * This provides CR operations on Role entity.
 */
public interface RoleRepository {
    /**
     * This creates role and persists it in database.
     *
     * @param role not null with not null name, case-insensitive
     * @return Role persisted in database
     */
    Optional<Role> createRole(Role role);

    /**
     * This retrieves role by exact name case-insensitive
     *
     * @param role not null with not null name, case-insensitive
     * @return Role persisted in database
     */
    Optional<Role> findRoleByName(Role role);
}
