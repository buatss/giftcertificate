package com.epam.esm.controller.web.controller;

import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.model.entity.User;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * This provides commonly used methods to retrieve arguments from mapped parameters, it ignores unknown params and
 * fetches only known such as: offset, limit, desc/descending, id% and name%.
 */
public class AbstractController {
    protected Integer fetchPageNumber(Map<String, String> params) {
        try {
            return Integer.parseInt(params.get("offset"));
        } catch (NumberFormatException e) {
            return 1;
        }
    }

    protected Integer fetchPageSize(Map<String, String> params) {
        try {
            return Integer.parseInt(params.get("limit"));
        } catch (NumberFormatException e) {
            return 5;
        }
    }

    protected Boolean fetchSortingType(Map<String, String> params) {
        try {
            String sort = params.get("sort");
            return !sort.equalsIgnoreCase("desc") && !sort.equalsIgnoreCase("descending");
        } catch (NullPointerException e) {
            return true;
        }
    }

    protected Long[] fetchIds(Map<String, String> params) {
        return params.keySet()
                     .stream()
                     .filter(param -> param.startsWith("id"))
                     .map(params::get)
                     .map(Long::valueOf)
                     .toArray(Long[]::new);
    }

    protected String[] fetchNames(Map<String, String> params) {
        return params.keySet()
                     .stream()
                     .filter(param -> param.startsWith("name"))
                     .map(params::get)
                     .map(Object::toString)
                     .toArray(String[]::new);
    }

    protected Map<String, String> convertEntrySetToLowercase(Map<String, String> params) {
        Map<String, String> paramsLowercase = new HashMap<>();
        params.keySet().forEach(key -> {
            paramsLowercase.put(key.toLowerCase(), params.get(key).toLowerCase());
        });
        return paramsLowercase;
    }

    protected GiftCertificate[] fetchCertificateFilters(Map<String, String> params) {
        Stream<GiftCertificate> nameFilters = Arrays.stream(fetchNames(params)).map(name -> {
            GiftCertificate certificate = new GiftCertificate();
            certificate.setName(name);
            return certificate;
        });
        Stream<GiftCertificate> idFilters = Arrays.stream(fetchIds(params)).map(id -> {
            GiftCertificate certificate = new GiftCertificate();
            certificate.setId(id);
            return certificate;
        });
        return Stream.concat(nameFilters, idFilters).toArray(GiftCertificate[]::new);
    }

    protected Tag[] fetchTagFilters(Map<String, String> params) {
        Stream<Tag> nameFilters = Arrays.stream(fetchNames(params)).map(name -> {
            Tag tag = new Tag();
            tag.setName(name);
            return tag;
        });
        Stream<Tag> idFilters = Arrays.stream(fetchIds(params)).map(id -> {
            Tag tag = new Tag();
            tag.setId(id);
            return tag;
        });
        return Stream.concat(nameFilters, idFilters).toArray(Tag[]::new);
    }

    protected User[] fetchUserFilters(Map<String, String> params) {
        return Arrays.stream(fetchIds(params)).map(id -> {
            User user = new User();
            user.setId(id);
            return user;
        }).toArray(User[]::new);
    }
}
