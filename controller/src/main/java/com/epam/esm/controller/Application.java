package com.epam.esm.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.epam.esm"})
public class Application {
    public static void main(String[] args) {
        System.setProperty("server.servlet.context-path", "/giftcertificate");
        System.setProperty("server.port", "8081");
        new SpringApplicationBuilder(Application.class).profiles("prod").run(args);
    }
}
