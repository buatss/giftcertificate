package com.epam.esm.controller.web.controller;

import com.epam.esm.controller.assembler.CertificateRepresentationModelAssembler;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.service.servicelogic.GiftCertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * This controller provides endpoints for GiftCertificate which allows to perform CRUD operations.
 * Pagination is supported for read operations.
 */
@RestController
@RequestMapping(value = "/certificates")
public class CertificateController extends AbstractController {
    private final GiftCertificateService service;
    private final CertificateRepresentationModelAssembler assembler;

    @Autowired
    public CertificateController(GiftCertificateService giftCertificateService,
            CertificateRepresentationModelAssembler assembler) {
        service = giftCertificateService;
        this.assembler = assembler;
    }

    /**
     * This retrieves single record by single id.
     *
     * @param id not null
     * @return ResponseEntity with model linked to itself and associated
     */
    @GetMapping(value = "/{id}")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<EntityModel<GiftCertificateDto>> certificateById(@PathVariable Long id) {
        GiftCertificate filter = new GiftCertificate();
        filter.setId(id);
        GiftCertificateDto certificate = service.findGiftCertificateById(filter);
        EntityModel<GiftCertificateDto> model = EntityModel.of(certificate);
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This retrieves collection of records in range of page params sorted by id. Page params are safe, if any is
     * incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: offset - should be positive integer(if incorrect by
     * default 1) limit - should be positive integer not grater than 10(if incorrect by default 5) sort -
     * ASC or DESC/DESCENDING values(if incorrect by default ascending) other params are ignored
     * @return ResponseEntity with models linked to itself and associated, records are sorted by id
     */
    @GetMapping
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<GiftCertificateDto>>> certificatesById(
            @RequestParam Map<String, String> params) {
        List<GiftCertificateDto> certificates = service.findGiftCertificatesById(fetchPageNumber(params),
                fetchPageSize(params),
                fetchSortingType(params)
        );
        CollectionModel<EntityModel<GiftCertificateDto>> models = convertCertificateListToCollectionModel(certificates);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This retrieves collection of records matching passed requirements as query params in range of page params sorted
     * by id. Page params are safe, if any is incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: name(mandatory - should be not null nor empty,
     * offset - should be positive integer(if incorrect by default 1) limit - should be positive integer
     * not grater than 10(if incorrect by default 5) sort - ASC or DESC/DESCENDING values(if incorrect by
     * default ascending) other params are ignored
     * @return CollectionModel of ResponseEntity with models linked to itself and associated by default shows first 10
     * records which are sorted by id
     * <p>
     * @apiNote unknown query params are ignored
     */
    @GetMapping(value = "search")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<GiftCertificateDto>>> searchByIdsAndNames(
            @RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<GiftCertificateDto> result = service.searchByIdsAndNames(fetchCertificateFilters(params),
                fetchPageNumber(params),
                fetchPageSize(params),
                fetchSortingType(params)
        );
        CollectionModel<EntityModel<GiftCertificateDto>> models = convertCertificateListToCollectionModel(result);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This retrieves collection of records matching passed requirements as query params in range of page params sorted
     * by id. Page params are safe, if any is incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: id% with positive integer value, name% with
     * non-empty String value, offset - should be positive integer(if incorrect by default 1) limit -
     * should be positive integer not grater than 10(if incorrect by default 5) sort - ASC or
     * DESC/DESCENDING values(if incorrect by default ascending) other params are ignored
     * @return CollectionModel of ResponseEntity with models linked to itself and associated by default shows first 10
     * records are sorted by id
     * <p>
     * @apiNote unknown query params are ignored
     */
    @GetMapping(value = "/tags")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<GiftCertificateDto>>> searchByTagsId(
            @RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<GiftCertificateDto> certificates = service.searchByManyTagsIdAndName(fetchTagFilters(params),
                fetchPageNumber(params),
                fetchPageSize(params),
                fetchSortingType(params)
        );
        CollectionModel<EntityModel<GiftCertificateDto>> models = convertCertificateListToCollectionModel(certificates);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This retrieves collection of records in range of page params sorted by id. Page params are safe, if any is
     * incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: offset - should be positive integer(if incorrect by
     * default 1) limit - should be positive integer not grater than 10(if incorrect by default 5) sort -
     * ASC or DESC/DESCENDING values(if incorrect by default ascending) other params are ignored
     * @return CollectionModel of ResponseEntity with models linked to itself and associated by default shows first 10
     * records which are sorted by id
     */
    @GetMapping(value = "/unassigned")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<GiftCertificateDto>>> getUnassignedCertificates(
            @RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<GiftCertificateDto> certificates = service.findUnassignedCertificates(fetchPageNumber(params),
                fetchPageSize(params),
                fetchSortingType(params)
        );
        CollectionModel<EntityModel<GiftCertificateDto>> models = convertCertificateListToCollectionModel(certificates);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This method is used to both create and update record in database. To create id argument should be null, to update
     * not null.
     *
     * @param certificate with attributes meeting following requirements: -certificate is not null -id is null -price
     * has precision maximum 12 and scale maximum 2 -durations is not null and is non-negative -name
     * is not null -description is not null
     * @return ResponseEntity with model linked to itself and associated
     */
    @PostMapping
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<GiftCertificateDto>> createOrUpdateCertificate(
            @RequestBody GiftCertificate certificate) {
        EntityModel<GiftCertificateDto> model;
        HttpStatus status;
        if (isNull(certificate.getId())) {
            status = HttpStatus.CREATED;
            model = EntityModel.of(service.createGiftCertificate(certificate));
        } else {
            model = EntityModel.of(service.updateGiftCertificate(certificate));
            status = HttpStatus.OK;
        }
        assembler.addLinks(model);
        return new ResponseEntity<>(model, status);
    }

    /**
     * This is used to associate certificate and tag, then retrieve associated certificate this supports only two query
     * params.
     *
     * @param certificateId not null
     * @param tagId not null
     * @return ResponseEntity with model linked to itself and associated
     */
    @PostMapping(params = {"certificateId", "tagId"})
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<GiftCertificateDto>> assignCertificateToTagById(@RequestParam Long certificateId,
            @RequestParam Long tagId) {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(certificateId);
        Tag tag = new Tag();
        tag.setId(tagId);
        EntityModel<GiftCertificateDto> model = EntityModel.of(service.connectCertificateAndTag(certificate, tag));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This is used to remove GiftCertificate with passed id, in case of success it's representation model is retrieved.
     *
     * @param id not null and refers to existing record in database
     * @return ResponseEntity with model linked to itself and associated
     */
    @DeleteMapping(value = "/{id}")
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<GiftCertificateDto>> deleteGiftCertificateById(@PathVariable Long id) {
        GiftCertificate filter = new GiftCertificate();
        filter.setId(id);
        EntityModel<GiftCertificateDto> model = EntityModel.of(service.deleteGiftCertificateById(filter));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.ACCEPTED);
    }

    private CollectionModel<EntityModel<GiftCertificateDto>> convertCertificateListToCollectionModel(
            List<GiftCertificateDto> certificates) {
        return CollectionModel.of(certificates.stream().map(EntityModel::of).collect(Collectors.toList()));
    }

}
