package com.epam.esm.controller.exception;

import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A handler determines project's behaviour when encountering an execution error.
 * This handler should be set once for whole project.
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    /**
     * Handles BadRequestException which is caused by wrong arguments delivered by user
     *
     * @param exception BadRequestException.
     * @return ResponseEntity.
     */
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<ExceptionMessage> handleBadRequestException(Exception exception) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ExceptionMessage message = new ExceptionMessage(exception, status);
        return new ResponseEntity<>(message, status);
    }

    /**
     * Handles ResourceNotFoundException which is caused by missing resource in database.
     *
     * @param exception ResourceNotFoundException.
     * @return ResponseEntity.
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    public ResponseEntity<ExceptionMessage> handleResourceNotFoundException(Exception exception) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ExceptionMessage message = new ExceptionMessage(exception, status);
        return new ResponseEntity<>(message, status);
    }

    /**
     * This handles InternalServerException which is caused by internal critical failure of application e.g. cannot
     * connect to database.
     *
     * @param exception InternalServerException.
     * @return ResponseEntity.
     */
    @ExceptionHandler({InternalServerException.class})
    @ResponseBody
    public ResponseEntity<ExceptionMessage> handleInternalServerError(InternalServerException exception) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ExceptionMessage message = new ExceptionMessage(exception, status);
        return new ResponseEntity<>(message, status);
    }

    /**
     * Handles Exception which cause is unknown and exception unhandled by other handles.
     *
     * @param exception not handled exception by other handlers.
     * @return ResponseEntity.
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ExceptionMessage> applicationCrash(Exception exception) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ExceptionMessage message = new ExceptionMessage(
                "Application crash caused by: " + exception.getClass() + "\nMessage=" + exception.getMessage(), status);
        logger.error(exception.getClass() + "\n" + exception.getMessage());
        return new ResponseEntity<>(message, status);
    }

}
