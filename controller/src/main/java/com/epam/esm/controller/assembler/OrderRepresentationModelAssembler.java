package com.epam.esm.controller.assembler;

import com.epam.esm.controller.web.controller.CertificateController;
import com.epam.esm.controller.web.controller.OrderController;
import com.epam.esm.controller.web.controller.UserController;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.UserDto;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * This provides methods to add links to RepresentationalModel of OrderDto related to itself and associated.
 */
@Component
public class OrderRepresentationModelAssembler implements SimpleRepresentationModelAssembler<OrderDto> {
    /**
     * This adds self-link to single resource and link to associated resources.
     *
     * @param resource not null EntityModel of OrderDto.
     */
    @Override
    public void addLinks(EntityModel<OrderDto> resource) {
        addAllLinks(Objects.requireNonNull(resource.getContent()));
    }

    /**
     * This adds self-link to each resource and link to associated resources.
     *
     * @param resources not null CollectionModel of EntityModels of OrderDto.
     */
    @Override
    public void addLinks(CollectionModel<EntityModel<OrderDto>> resources) {
        resources.getContent().forEach(this::addLinks);
    }

    private void addAllLinks(OrderDto order) {
        order.addIf(
                !order.hasLink("self"),
                () -> linkTo(methodOn(OrderController.class).getOrderById(order.getId())).withSelfRel()
        );

        UserDto user = order.getUser();
        GiftCertificateDto certificate = order.getGiftCertificate();

        certificate.addIf(
                !certificate.hasLink("self"),
                () -> linkTo(methodOn(CertificateController.class).certificateById(certificate.getId())).withSelfRel()
        );

        user.addIf(
                !user.hasLink("self"),
                () -> linkTo(methodOn(UserController.class).getUserById(user.getId())).withSelfRel()
        );

        order.setUser(user);
        order.setGiftCertificate(certificate);
    }
}
