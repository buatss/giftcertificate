package com.epam.esm.controller.web.controller;

import com.epam.esm.controller.assembler.UserRepresentationModelAssembler;
import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.service.servicelogic.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This controller provides endpoint for User which allows to perform read operation.
 * Pagination is supported for read operations.
 */
@RestController
@RequestMapping(value = "/users")
public class UserController extends AbstractController {

    private final UserService service;

    private final UserRepresentationModelAssembler assembler;

    @Autowired
    public UserController(UserService service, UserRepresentationModelAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    /**
     * This retrieves single User by its id.
     *
     * @param id not null
     * @return ResponseEntity with model linked to itself and associated
     */
    @GetMapping(value = "/{id}")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<EntityModel<UserDto>> getUserById(@PathVariable Long id) {
        EntityModel<UserDto> model = EntityModel.of(service.findUserById(id));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This retrieves collection of records in range of page params sorted by id. Page params are safe, if any is
     * incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: offset - should be positive integer(if incorrect by
     * default 1) limit - should be positive integer not grater than 10(if incorrect by default 5) sort -
     * ASC or DESC/DESCENDING values(if incorrect by default ascending) other params are ignored
     * @return Response entity of models linked to itself and associated
     */
    @GetMapping
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<UserDto>>> getUsersById(@RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<UserDto> users =
                service.findUsersById(fetchPageNumber(params), fetchPageSize(params), fetchSortingType(params));
        CollectionModel<EntityModel<UserDto>> models = convertTagListToCollectionModel(users);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    private CollectionModel<EntityModel<UserDto>> convertTagListToCollectionModel(List<UserDto> users) {
        return CollectionModel.of(users.stream().map(EntityModel::of).collect(Collectors.toList()));
    }
}
