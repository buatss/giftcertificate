package com.epam.esm.controller.web.filter;

import com.epam.esm.dao.model.entity.User;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * This provides utility methods for JWT token and supports processing it for this application.
 */
@Component("jwtTokenUtil")
public class JwtTokenUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenUtil.class);

    private static final long EXPIRE_DURATION = 24 * 60 * 60 * 1000;

    private final String SECRET_KEY = "SECRET_KEY";

    /**
     * This generates JWT token with user's data required for further authorization.
     *
     * @param user not null, with not null nor empty id and username
     * @return JWT token as String
     */
    public String generateAccessToken(User user) {
        return Jwts.builder()
                   .setSubject(String.format("%s,%s", user.getId(), user.getUsername()))
                   .setIssuer("GiftCertificateApplication")
                   .claim("roles", convertAuthoritiesToStringAsRoles(user.getAuthorities()))
                   .setIssuedAt(new Date())
                   .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION))
                   .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                   .compact();
    }

    /**
     * This checks that Token is valid and ready for further processing.
     *
     * @param token subject of validation.
     * @return true if token is: not expired, not null nor empty, has correct syntax and has valid signature, otherwise
     * returns false.
     */
    public boolean validateAccessToken(String token) {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException ex) {
            LOGGER.error("JWT expired", ex.getMessage());
        } catch (IllegalArgumentException ex) {
            LOGGER.error("Token is null, empty or only whitespace", ex.getMessage());
        } catch (MalformedJwtException ex) {
            LOGGER.error("JWT is invalid", ex);
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("JWT is not supported", ex);
        } catch (SignatureException ex) {
            LOGGER.error("Signature validation failed");
        }

        return false;
    }

    public String getSubject(String token) {
        return parseClaims(token).getSubject();
    }

    /**
     * This parses token's claims encrypted by secret key.
     *
     * @param token which should pass validation: {@link #validateAccessToken(String token)}
     * @return token's claims
     */
    public Claims parseClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private String convertAuthoritiesToStringAsRoles(Collection<? extends GrantedAuthority> authorities) {
        String roles = authorities.stream()
                                  .map(GrantedAuthority::getAuthority)
                                  .map("ROLE_"::concat)
                                  .map(str -> str.concat(" "))
                                  .collect(Collectors.joining())
                                  .trim();
        return roles;
    }
}
