package com.epam.esm.controller.web.controller;

import com.epam.esm.controller.web.filter.JwtTokenUtil;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.service.servicelogic.impl.UserDetailsServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/login")
public class LoginController {
    private final UserDetailsServiceManager service;
    private final JwtTokenUtil jwtTokenUtil;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public LoginController(UserDetailsServiceManager service, JwtTokenUtil jwtTokenUtil,
            PasswordEncoder passwordEncoder) {
        this.service = service;
        this.jwtTokenUtil = jwtTokenUtil;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * This authenticates user by checking, that passed credentials are already stored in database, if so returns JWT
     * token for further authentication.
     *
     * @param user not null with not null nor empty username & password
     * @return JWT token
     */
    @PostMapping
    public ResponseEntity<String> login(@RequestBody User user) {
        if (service.userExists(user.getUsername())) {
            UserDetails userPersisted = service.loadUserByUsername(user.getUsername());
            if (checkPassword(user, userPersisted)) {
                String accessToken = jwtTokenUtil.generateAccessToken((User) userPersisted);
                return new ResponseEntity<>(accessToken, HttpStatus.OK);
            }
        }
        throw new BadCredentialsException("Invalid user's credentials");
    }

    private boolean checkPassword(User userInput, UserDetails userPersisted) {
        return passwordEncoder.matches(userInput.getPassword(), userPersisted.getPassword());
    }
}
