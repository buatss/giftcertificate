package com.epam.esm.controller.web.controller;

import com.epam.esm.controller.assembler.TagRepresentationModelAssembler;
import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.service.servicelogic.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This controller provides endpoints for Tag which allows to perform CRD operations.
 * Pagination is supported for read operations.
 */
@RestController
@RequestMapping(value = "/tags")
public class TagController extends AbstractController {

    private final TagService service;
    private final TagRepresentationModelAssembler assembler;

    @Autowired
    public TagController(TagService service, TagRepresentationModelAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    /**
     * This retrieves representational model of single tag by its id.
     *
     * @param id not null and refers to persisted record in database.
     * @return ResponseEntity.
     */
    @GetMapping(value = "/{id}")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<EntityModel<TagDto>> getTagById(@PathVariable Long id) {
        Tag filter = new Tag();
        filter.setId(id);
        TagDto tag = service.getTagById(filter);
        EntityModel<TagDto> model = EntityModel.of(tag);
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This retrieves collection of records in range of page params sorted by id. Page params are safe, if any is
     * incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: offset - should be positive integer(if incorrect by
     * default 1) limit - should be positive integer not grater than 10(if incorrect by default 5) sort -
     * ASC or DESC/DESCENDING values(if incorrect by default ascending) other params are ignored
     * @return Response entity of models linked to itself and associated.
     */
    @GetMapping
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<TagDto>>> getTagsByIds(@RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<TagDto> tags =
                service.getTagsById(fetchPageNumber(params), fetchPageSize(params), fetchSortingType(params));
        CollectionModel<EntityModel<TagDto>> models = convertTagListToCollectionModel(tags);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This is used to retrieve collection model of tag which is most frequent among bought certificates by user, who
     * spent most money from all users.
     *
     * @return tag.
     */
    @GetMapping(value = "/returnMostFrequent")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<EntityModel<TagDto>> getMostFrequentTagFromUserWithMostExpensiveOrdersTotal() {
        TagDto tag = service.getMostFrequentTagFromUserWithMostExpensiveOrdersTotal();
        EntityModel<TagDto> model = EntityModel.of(tag);
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This retrieves collection of records matching passed requirements as query params in range of page params sorted
     * by id. Page params are safe, if any is incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: id% with positive integer value, name% with
     * non-empty String value, offset - should be positive integer(if incorrect by default 1) limit -
     * should be positive integer not grater than 10(if incorrect by default 5) sort - ASC or
     * DESC/DESCENDING values(if incorrect by default ascending) other params are ignored.
     * @return Collection of ResponseEntity with models linked to itself and associated by default shows first 10
     * records are sorted by id.
     * <p>
     * @apiNote unknown query params are ignored.
     */
    @GetMapping(value = "/search")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<TagDto>>> getTagsByNamesAndIds(
            @RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<TagDto> tags = service.findByNamesAndIds(fetchTagFilters(params),
                fetchPageNumber(params),
                fetchPageSize(params),
                fetchSortingType(params)
        );
        CollectionModel<EntityModel<TagDto>> models = convertTagListToCollectionModel(tags);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This retrieves collection of records in range of page params sorted by id. Page params are safe, if any is
     * incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: offset - should be positive integer(if incorrect by
     * default 1) limit - should be positive integer not grater than 10(if incorrect by default 5) sort -
     * ASC or DESC/DESCENDING values(if incorrect by default ascending) other params are ignored.
     * @return Response entity of models linked to itself and associated.
     */
    @GetMapping(value = "unassigned")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<TagDto>>> getUnassignedTags(@RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        List<TagDto> tags =
                service.getUnassignedTags(fetchPageNumber(params), fetchPageSize(params), fetchSortingType(params));
        CollectionModel<EntityModel<TagDto>> models = convertTagListToCollectionModel(tags);
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This is used to persist tag's record in database then retrieve it representational model.
     *
     * @param tag not null, with null id and not null nor empty name.
     * @return ResponseEntity.
     */
    @PostMapping
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<TagDto>> createTag(@RequestBody Tag tag) {
        EntityModel<TagDto> model = EntityModel.of(service.createTag(tag));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    /**
     * This is used to establish association between Tag and GiftCertificate then return Tag's representational model
     * with all associations.
     *
     * @param certificateId not null and refers to persisted record in database.
     * @param tagId not null and refers to persisted record in database.
     * @return ResponseEntity.
     */
    @PostMapping(params = {"certificateId", "tagId"})
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<TagDto>> assignTagToCertificateById(@RequestParam Long certificateId,
            @RequestParam Long tagId) {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setId(certificateId);
        Tag tag = new Tag();
        tag.setId(tagId);
        EntityModel<TagDto> model = EntityModel.of(service.connectTagAndCertificate(certificate, tag));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This is used to remove persisted Tag from database then retrieve its representational model.
     *
     * @param id not null and refers to persisted record in database
     * @return ResponseEntity
     */
    @DeleteMapping(value = "/{id}")
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<TagDto>> deleteTagById(@PathVariable Long id) {
        Tag filter = new Tag();
        filter.setId(id);
        EntityModel<TagDto> model = EntityModel.of(service.deleteTagById(filter));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.ACCEPTED);
    }

    private CollectionModel<EntityModel<TagDto>> convertTagListToCollectionModel(List<TagDto> tags) {
        return CollectionModel.of(tags.stream().map(EntityModel::of).collect(Collectors.toList()));
    }

}
