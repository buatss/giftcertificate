package com.epam.esm.controller.assembler;

import com.epam.esm.controller.web.controller.CertificateController;
import com.epam.esm.controller.web.controller.OrderController;
import com.epam.esm.controller.web.controller.TagController;
import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.TagDto;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * This provides methods to add links to RepresentationalModel of GiftCertificateDto related to itself and associated.
 */
@Component
public class CertificateRepresentationModelAssembler implements SimpleRepresentationModelAssembler<GiftCertificateDto> {
    /**
     * This adds self-link to single resource and link to associated resources.
     *
     * @param resource not null EntityModel of GiftCertificateDto.
     */
    @Override
    public void addLinks(EntityModel<GiftCertificateDto> resource) {
        addAllLinks(Objects.requireNonNull(resource.getContent()));
    }

    /**
     * This adds self-link to each resource and link to associated resources.
     *
     * @param resources not null CollectionModel of EntityModels of GiftCertificateDto.
     */
    @Override
    public void addLinks(CollectionModel<EntityModel<GiftCertificateDto>> resources) {
        resources.getContent().forEach(this::addLinks);
    }

    private void addAllLinks(GiftCertificateDto certificate) {
        certificate.addIf(
                !certificate.hasLink("self"),
                () -> linkTo(methodOn(CertificateController.class).certificateById(certificate.getId())).withSelfRel()
        );

        Collection<TagDto> tags = certificate.getTags();
        if (!tags.isEmpty()) {
            tags.forEach(tag -> tag.addIf(
                    !tag.hasLink("self"),
                    () -> linkTo(methodOn(TagController.class).getTagById(tag.getId())).withSelfRel()
            ));
        }

        Collection<OrderDto> orders = certificate.getOrders();
        if (!orders.isEmpty()) {
            orders.forEach(order -> order.addIf(
                    !order.hasLink("self"),
                    () -> linkTo(methodOn(OrderController.class).getOrderById(order.getId())).withSelfRel()
            ));
        }
    }

}
