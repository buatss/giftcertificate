package com.epam.esm.controller.assembler;

import com.epam.esm.controller.web.controller.CertificateController;
import com.epam.esm.controller.web.controller.OrderController;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.dto.UserDto;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * This provides methods to add links to RepresentationalModel of UserDto related to itself and associated.
 */
@Component
public class UserRepresentationModelAssembler implements SimpleRepresentationModelAssembler<UserDto> {
    /**
     * This adds self-link to single resource and link to associated resources.
     *
     * @param resource not null EntityModel of UserDto.
     */
    @Override
    public void addLinks(EntityModel<UserDto> resource) {
        addAllLinks(Objects.requireNonNull(resource.getContent()));
    }

    /**
     * This adds self-link to each resource and link to associated resources.
     *
     * @param resources not null CollectionModel of EntityModels of UserDto.
     */
    @Override
    public void addLinks(CollectionModel<EntityModel<UserDto>> resources) {
        resources.getContent().forEach(this::addLinks);
    }

    private void addAllLinks(UserDto userDto) {
        userDto.addIf(
                !userDto.hasLink("self"),
                () -> linkTo(methodOn(CertificateController.class).certificateById(userDto.getId())).withSelfRel()
        );

        Collection<OrderDto> orders = userDto.getOrders();
        if (!orders.isEmpty()) {
            orders.forEach(order -> order.addIf(
                    !order.hasLink("self"),
                    () -> linkTo(methodOn(OrderController.class).getOrderById(order.getId())).withSelfRel()
            ));
        }
    }

}
