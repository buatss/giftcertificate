package com.epam.esm.controller.web.controller;

import com.epam.esm.dao.model.entity.User;
import com.epam.esm.service.servicelogic.impl.UserDetailsServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/register")
public class RegistrationController {

    private final UserDetailsServiceManager service;

    @Autowired
    public RegistrationController(UserDetailsServiceManager service) {
        this.service = service;
    }

    /**
     * This simply creates new user in database with initial "user" role.
     *
     * @param user with not null nor empty username & password
     * @return ResponseEntity, if success with no content.
     */
    @PostMapping
    public ResponseEntity<HttpStatus> register(@RequestBody User user) {
        if (!service.userExists(user.getUsername())) {
            service.createUser(user);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            throw new BadCredentialsException("Invalid username or password.");
        }
    }
}
