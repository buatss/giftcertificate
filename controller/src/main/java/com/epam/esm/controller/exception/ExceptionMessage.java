package com.epam.esm.controller.exception;

import org.springframework.http.HttpStatus;

/**
 * Provider of exception message with error code in case of application failure.
 */
public class ExceptionMessage {
    private final String errorMessage;
    private final String errorCode;

    public ExceptionMessage(String errorMessage, HttpStatus errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = String.valueOf(errorCode.value());
    }

    public ExceptionMessage(Exception exception, HttpStatus errorCode) {
        this.errorMessage = exception.getMessage();
        this.errorCode = String.valueOf(errorCode.value());
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return "ControllerException{" + "errorMessage='" + errorMessage + '\'' + ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
