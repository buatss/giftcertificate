package com.epam.esm.controller.web.controller;

import com.epam.esm.controller.assembler.OrderRepresentationModelAssembler;
import com.epam.esm.controller.web.filter.JwtTokenUtil;
import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.service.servicelogic.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This controller provides endpoints for Order which allows to perform CR operations.
 * Pagination is supported for read operations.
 */
@RestController
@RequestMapping(value = "/orders")
public class OrderController extends AbstractController {
    private final OrderService service;
    private final OrderRepresentationModelAssembler assembler;
    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public OrderController(OrderService service, OrderRepresentationModelAssembler assembler,
            JwtTokenUtil jwtTokenUtil) {
        this.service = service;
        this.assembler = assembler;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    /**
     * This is used to retrieve single Order by it's id.
     *
     * @param id not null and refers to persisted record in database.
     * @return ResponseEntity with model linked to itself and associated.
     */
    @GetMapping(value = "/{id}")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<EntityModel<OrderDto>> getOrderById(@PathVariable Long id) {
        Order filter = new Order();
        filter.setId(id);
        EntityModel<OrderDto> model = EntityModel.of(service.findOrderById(filter));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * This retrieves collection of records in range of page params sorted by id. Page params are safe, if any is
     * incorrect then is set to default: offset 1, limit 5, sort ascending.
     *
     * @param params Map of query params(case-insensitive) such as: offset - should be positive integer(if incorrect by
     * default 1) limit - should be positive integer not grater than 10(if incorrect by default 5) sort -
     * ASC or DESC/DESCENDING values(if incorrect by default ascending) other params are ignored
     * @return Response entity of models linked to itself and associated
     */
    @GetMapping
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<OrderDto>>> getOrdersById(@RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        CollectionModel<EntityModel<OrderDto>> models =
                CollectionModel.of(convertOrderListToCollectionModel(service.findOrdersById(fetchPageNumber(params),
                        fetchPageSize(params),
                        fetchSortingType(params)
                )));
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This retrieves collection of orders associated to passed certificates as query params in range of page params
     * sorted by id. Page params are safe, if any is incorrect then is set to default: offset 1, limit 5, sort
     * ascending.
     *
     * @param params Map of query params(case-insensitive) such as: id - mandatory exactly one with such prefix, offset
     * - should be positive integer(if incorrect by default 1) limit - should be positive integer not
     * grater than 10(if incorrect by default 5) sort - ASC or DESC/DESCENDING values(if incorrect by
     * default ascending) other params are ignored
     * @return CollectionModel of ResponseEntity with models linked to itself and associated by default shows first 10
     * records
     */
    @GetMapping(value = "/certificates")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<OrderDto>>> getOrdersByCertificateIdPaginated(
            @RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        GiftCertificate[] filters = fetchCertificateFilters(params);
        CollectionModel<EntityModel<OrderDto>> models =
                CollectionModel.of(convertOrderListToCollectionModel(service.findOrdersByCertificateId(filters,
                        fetchPageNumber(params),
                        fetchPageSize(params),
                        fetchSortingType(params)
                )));
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This retrieves collection of orders associated to passed users as query params in range of page params
     * sorted by id. Page params are safe, if any is incorrect then is set to default: offset 1, limit 5, sort
     * ascending.
     *
     * @param params Map of query params(case-insensitive) such as: id - mandatory exactly one with such prefix, offset
     * - should be positive integer(if incorrect by default 1) limit - should be positive integer not
     * grater than 10(if incorrect by default 5) sort - ASC or DESC/DESCENDING values(if incorrect by
     * default ascending) other params are ignored
     * @return CollectionModel of ResponseEntity with models linked to itself and associated by default shows first 10
     * records
     */
    @GetMapping(value = "users")
    @RolesAllowed(value = {"user", "admin"})
    public ResponseEntity<Collection<EntityModel<OrderDto>>> getOrdersByUserIdPaginated(
            @RequestParam Map<String, String> params) {
        params = convertEntrySetToLowercase(params);
        User[] filters = fetchUserFilters(params);
        CollectionModel<EntityModel<OrderDto>> models =
                CollectionModel.of(convertOrderListToCollectionModel(service.findOrdersByUserId(filters,
                        fetchPageNumber(params),
                        fetchPageSize(params),
                        fetchSortingType(params)
                )));
        assembler.addLinks(models);
        return new ResponseEntity<>(models.getContent(), HttpStatus.OK);
    }

    /**
     * This is used to add Order record in database having association with GiftCertificate and User, then retrieve
     * it's collection model with all associated.
     *
     * @param cid not null and refers to persisted record in database
     * @param uid not null and refers to persisted record in database
     * @return ResponseEntity with model linked to itself and associated
     */
    @PostMapping(params = {"cid", "uid"})
    @RolesAllowed(value = {"admin"})
    public ResponseEntity<EntityModel<OrderDto>> createOrder(@RequestParam Long cid, @RequestParam Long uid) {
        EntityModel<OrderDto> model = EntityModel.of(service.createOrder(cid, uid));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    /**
     * This is used to make order by user.
     *
     * @param id not null, refers GiftCertificate record destined to buy
     * @param token it is JWT token found in header "Authorization", it must contain user's id
     * @return ResponseEntity with model linked to itself and associated
     */
    @PostMapping(params = {"id"})
    @RolesAllowed(value = {"user"})
    public ResponseEntity<EntityModel<OrderDto>> createOrder(@RequestParam Long id,
            @RequestHeader("Authorization") String token) {
        token = token.substring(7);
        String subject = jwtTokenUtil.getSubject(token);
        Long userId = Long.valueOf(subject.substring(0, subject.indexOf(',')));
        EntityModel<OrderDto> model = EntityModel.of(service.createOrder(id, userId));
        assembler.addLinks(model);
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    private CollectionModel<EntityModel<OrderDto>> convertOrderListToCollectionModel(List<OrderDto> orders) {
        return CollectionModel.of(orders.stream().map(EntityModel::of).collect(Collectors.toList()));
    }
}
