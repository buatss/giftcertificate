package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.GiftCertificateRepository;
import com.epam.esm.dao.repository.impl.GiftCertificateRepositoryImpl;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.servicelogic.GiftCertificateService;
import com.epam.esm.service.validator.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

/**
 * This provides performing CRUD operations on GiftCertificate Entity through {@link GiftCertificateRepositoryImpl} with
 * input validation and handling retrieved result from persistence layer.
 * In most cases data is retrieved or exception is thrown. Safe pagination is supported for read operations.
 */
@Service
public class GiftCertificateServiceImpl extends AbstractService<GiftCertificateDto> implements GiftCertificateService {
    private final GiftCertificateRepository giftCertificateRepository;
    private final InputValidator inputValidator;

    @Autowired
    public GiftCertificateServiceImpl(GiftCertificateRepository giftCertificateRepository,
            InputValidator inputValidator) {
        this.giftCertificateRepository = giftCertificateRepository;
        this.inputValidator = inputValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GiftCertificateDto createGiftCertificate(GiftCertificate certificate) {
        if (!isNull(certificate) && inputValidator.validateBeforeCreate(certificate)) {
            return handleOptionalResult(giftCertificateRepository.createGiftCertificate(certificate));
        } else {
            throw new BadRequestException("Invalid arguments");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GiftCertificateDto connectCertificateAndTag(GiftCertificate certificate, Tag tag) {
        if (!isNull(certificate) && !isNull(tag) && inputValidator.isValidLong(certificate.getId()) &&
                inputValidator.isValidLong(tag.getId())) {
            return handleOptionalResult(giftCertificateRepository.connectCertificateAndTag(certificate, tag));
        } else {
            throw new BadRequestException("Invalid ID.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GiftCertificateDto findGiftCertificateById(GiftCertificate certificate) {
        if (!isNull(certificate) && inputValidator.isValidLong(certificate.getId())) {
            return handleOptionalResult(giftCertificateRepository.findGiftCertificateById(certificate));
        } else {
            throw new BadRequestException("Wrong ID.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GiftCertificateDto> findGiftCertificatesById(Integer pageNumber, Integer pageSize,
            Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        return handleOptionalResultList(giftCertificateRepository.findGiftCertificatesById(pageNumber,
                pageSize,
                sortAscending
        ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GiftCertificateDto> searchByIdsAndNames(GiftCertificate[] certificates, Integer pageNumber,
            Integer pageSize, Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        if (inputValidator.validateCertificatesBeforeSearch(certificates)) {
            return handleOptionalResultList(giftCertificateRepository.searchByIdsAndNames(certificates,
                    pageNumber,
                    pageSize,
                    sortAscending
            ));
        } else {
            throw new BadRequestException("Wrong name.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GiftCertificateDto> searchByManyTagsIdAndName(Tag[] tags, Integer pageNumber, Integer pageSize,
            Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        if (inputValidator.validateTagsBeforeSearch(tags)) {
            return handleOptionalResultList(giftCertificateRepository.searchByManyTagsIdAndName(tags,
                    pageNumber,
                    pageSize,
                    sortAscending
            ));
        } else {
            throw new BadRequestException("Incorrect arguments");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GiftCertificateDto> findUnassignedCertificates(Integer pageNumber, Integer pageSize,
            Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        return handleOptionalResultList(giftCertificateRepository.findUnassignedCertificates(pageNumber,
                pageSize,
                sortAscending
        ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GiftCertificateDto updateGiftCertificate(GiftCertificate certificate) {
        if (!isNull(certificate) && inputValidator.isValidLong(certificate.getId()) &&
                (inputValidator.isCertificatePatchable(certificate) ||
                        inputValidator.validateBeforeCreate(certificate))) {
            return handleOptionalResult(giftCertificateRepository.updateGiftCertificate(certificate));
        } else {
            throw new BadRequestException("Wrong arguments.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GiftCertificateDto deleteGiftCertificateById(GiftCertificate certificate) {
        if (!isNull(certificate) && inputValidator.isValidLong(certificate.getId())) {
            return handleOptionalResult(giftCertificateRepository.deleteGiftCertificateById(certificate));
        } else {
            throw new BadRequestException("Wrong ID.");
        }
    }
}
