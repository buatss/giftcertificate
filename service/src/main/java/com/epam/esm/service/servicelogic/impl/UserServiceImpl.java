package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.repository.UserRepository;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.servicelogic.UserService;
import com.epam.esm.service.validator.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This provides performing only read operations on User Entity. Safe pagination is supported for read operations.
 */
@Service
public class UserServiceImpl extends AbstractService<UserDto> implements UserService {

    private final UserRepository repository;

    private final InputValidator inputValidator;

    @Autowired
    public UserServiceImpl(UserRepository repository, InputValidator inputValidator) {
        this.repository = repository;
        this.inputValidator = inputValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDto findUserById(Long id) {
        if (inputValidator.isValidLong(id)) {
            return handleOptionalResult(repository.findUserById(id));
        } else {
            throw new BadRequestException("Wrong id or user");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserDto> findUsersById(Integer pageNumber, Integer pageSize, Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        return handleOptionalResultList(repository.findUsersById(pageNumber, pageSize, sortAscending));
    }
}
