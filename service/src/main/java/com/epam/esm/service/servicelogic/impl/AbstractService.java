package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.service.exception.ResourceNotFoundException;
import com.epam.esm.service.validator.InputValidator;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

/**
 * This provides reusable methods of handling optional results retrieved prom repository layer.
 *
 * @param <T> parameter which is wrapped into Optional class
 */
public class AbstractService<T> {
    private final InputValidator validator = new InputValidator();

    protected T handleOptionalResult(Optional<T> result) {
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new ResourceNotFoundException("Such resource is not found");
        }
    }

    protected List<T> handleOptionalResultList(Optional<List<T>> result) {
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new ResourceNotFoundException("Such resources are not found.");
        }
    }

    protected Integer handleOffset(Integer pageNumber) {
        if (validator.validatePageNumber(pageNumber)) {
            return pageNumber;
        } else {
            return 1;
        }
    }

    protected Integer handleLimit(Integer pageSize) {
        if (validator.validatePageNumber(pageSize)) {
            return pageSize;
        } else {
            return 5;
        }
    }

    protected boolean handleSorting(Boolean sortAscending) {
        if (isNull(sortAscending)) {
            return true;
        } else {
            return sortAscending;
        }
    }
}
