package com.epam.esm.service.exception;

/**
 * This exception is thrown in case of wrong argument.
 */
public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }
}
