package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.OrderRepository;
import com.epam.esm.dao.repository.impl.OrderRepositoryImpl;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.servicelogic.OrderService;
import com.epam.esm.service.validator.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

/**
 * This provides performing CR operations on Order Entity through
 * {@link OrderRepositoryImpl} with input validation and handling retrieved result from persistence layer.
 * In most cases data is retrieved or exception is thrown. Safe pagination is supported for read operations.
 */
@Service
public class OrderServiceImpl extends AbstractService<OrderDto> implements OrderService {
    private final OrderRepository repository;
    private final InputValidator inputValidator;

    @Autowired
    public OrderServiceImpl(OrderRepository repository, InputValidator inputValidator) {
        this.repository = repository;
        this.inputValidator = inputValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderDto createOrder(Long certificateId, Long userId) {
        if (inputValidator.isValidLong(certificateId) && inputValidator.isValidLong(userId)) {
            return handleOptionalResult(repository.createOrder(certificateId, userId));
        } else {
            throw new BadRequestException("Wrong order arguments.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderDto findOrderById(Order order) {
        if (!isNull(order) && inputValidator.isValidLong(order.getId())) {
            return handleOptionalResult(repository.findOrderById(order));
        } else {
            throw new BadRequestException("Wrong input arguments");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<OrderDto> findOrdersById(Integer pageNumber, Integer pageSize, Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        return handleOptionalResultList(repository.findOrdersById(pageNumber, pageSize, sortAscending));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<OrderDto> findOrdersByUserId(User[] users, Integer pageNumber, Integer pageSize,
            Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        if (!isNull(users) && inputValidator.validateUsersBeforeSearch(users)) {
            return handleOptionalResultList(repository.findOrdersByUserId(users, pageNumber, pageSize, sortAscending));
        } else {
            throw new BadRequestException("Wrong input arguments.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<OrderDto> findOrdersByCertificateId(GiftCertificate[] certificates, Integer pageNumber,
            Integer pageSize, Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        if (!isNull(certificates) && inputValidator.validateCertificatesBeforeSearch(certificates)) {
            return handleOptionalResultList(repository.findOrdersByCertificateId(certificates,
                    pageNumber,
                    pageSize,
                    sortAscending
            ));
        } else {
            throw new BadRequestException("Wrong input arguments.");
        }
    }
}
