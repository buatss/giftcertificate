package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.TagRepository;
import com.epam.esm.dao.repository.impl.TagRepositoryImpl;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.exception.ResourceNotFoundException;
import com.epam.esm.service.servicelogic.TagService;
import com.epam.esm.service.validator.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

/**
 * This provides to perform CRD operations and tag through {@link TagRepositoryImpl} with input validation and handling
 * retrieved result from persistence layer. Safe pagination is supported for read operations excluding getting most
 * widely used tag from certificates bought by user, who spent the most money.
 */
@Service
public class TagServiceImpl extends AbstractService<TagDto> implements TagService {
    private final TagRepository tagRepository;

    private final InputValidator inputValidator;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, InputValidator inputValidator) {
        this.tagRepository = tagRepository;
        this.inputValidator = inputValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TagDto createTag(Tag tag) {
        if (!isNull(tag) && inputValidator.validateBeforeCreate(tag)) {
            return handleOptionalResult(tagRepository.createTag(tag));
        } else {
            throw new BadRequestException("Wrong arguments.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TagDto connectTagAndCertificate(GiftCertificate certificate, Tag tag) {
        if (!isNull(certificate) && !isNull(tag) && inputValidator.isValidLong(certificate.getId()) &&
                inputValidator.isValidLong(tag.getId())) {
            return handleOptionalResult(tagRepository.connectTagAndCertificate(certificate, tag));
        } else {
            throw new BadRequestException("Invalid ID.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TagDto getTagById(Tag tag) {
        if (!isNull(tag) && inputValidator.isValidLong(tag.getId())) {
            return handleOptionalResult(tagRepository.findTagById(tag));
        } else {
            throw new BadRequestException("Wrong ID.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TagDto> getTagsById(Integer pageNumber, Integer pageSize, Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        return handleOptionalResultList(tagRepository.findTagsById(pageNumber, pageSize, sortAscending));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TagDto> findByNamesAndIds(Tag[] tags, Integer pageNumber, Integer pageSize, Boolean sortAscending) {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        if (!isNull(tags) && inputValidator.validateTagsBeforeSearch(tags)) {
            return handleOptionalResultList(tagRepository.findTagsByNamesAndIds(tags,
                    pageNumber,
                    pageSize,
                    sortAscending
            ));
        } else {
            throw new BadRequestException("Wrong arguments");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TagDto> getUnassignedTags(Integer pageNumber, Integer pageSize, Boolean sortAscending)
            throws ResourceNotFoundException {
        pageNumber = handleOffset(pageNumber);
        pageSize = handleLimit(pageSize);
        sortAscending = handleSorting(sortAscending);
        return handleOptionalResultList(tagRepository.findUnassignedTags(pageNumber, pageSize, sortAscending));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TagDto getMostFrequentTagFromUserWithMostExpensiveOrdersTotal() {
        return handleOptionalResult(tagRepository.findMostFrequentTagFromUserWithMostExpensiveOrdersTotal());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TagDto deleteTagById(Tag tag) {
        if (!isNull(tag) && inputValidator.isValidLong(tag.getId())) {
            return handleOptionalResult(tagRepository.deleteTagById(tag));
        } else {
            throw new BadRequestException("Wrong ID.");
        }
    }
}
