package com.epam.esm.service.validator;

import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.model.entity.User;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.isNull;

/**
 * This provides validation methods for arguments. In most cases returns boolean denoting correctness of arguments.
 */
@Component
public class InputValidator {

    private final int MAXIMUM_PAGE_SIZE = 10;

    /**
     * That checks that page number is not null nor non-positive
     *
     * @param pageNumber integer
     * @return 1 if page number was non-positive or null, otherwise returns input
     */
    public boolean validatePageNumber(Integer pageNumber) {
        return !isNull(pageNumber) && pageNumber >= 1;
    }

    /**
     * This checks that page size is not null, positive and is not too big.
     *
     * @param pageSize integer
     * @return 5 if page number was non-positive or null or greater than 10, otherwise returns input
     */
    public boolean validatePageSize(Integer pageSize) {
        return !isNull(pageSize) && pageSize >= 1 && pageSize <= MAXIMUM_PAGE_SIZE;
    }

    /**
     * This checks that tag meets all requirements to be persisted in database.
     *
     * @param tag not null with not null nor empty name attribute
     * @return true if name is not null nor empty, otherwise false
     */
    public boolean validateBeforeCreate(Tag tag) {
        return !isNull(tag) && isNull(tag.getId()) && isValidString(tag.getName());
    }

    /**
     * This checks that certificate meets all requirements to be persisted in database.
     *
     * @param certificate not null, with all present attributes not managed by application
     * @return true if attributes meet following requirements: -id is null -certificate is not null -price has precision
     * maximum 12 and scale maximum 2 -durations is not null and is non-negative -name is not null -description is not
     * null If any of requirements is not met then returns false.
     */
    public boolean validateBeforeCreate(GiftCertificate certificate) {
        return !isNull(certificate) && isNull(certificate.getId()) && isValidPrice(certificate.getPrice()) &&
                isValidDuration(certificate.getDuration()) && isValidString(certificate.getName()) &&
                isValidString(certificate.getDescription());
    }

    /**
     * This check that String is not null nor empty.
     *
     * @param string subject of validation
     * @return true if string is not null nor empty
     */
    public boolean isValidString(final String string) {
        return !isNull(string) && !string.isEmpty();
    }

    /**
     * This checks that Long value is not null.
     *
     * @param longVal - subject of validation
     * @return true if longVal is not null
     */
    public boolean isValidLong(Long longVal) {
        return !isNull(longVal);
    }

    /**
     * This checks that each Long value is not null.
     *
     * @param longVal - subjects of validation
     * @return true if longVal every is not null
     */
    public boolean isValidLong(Long... longVal) {
        Optional<Boolean> predicate =
                Arrays.stream(longVal).map(Objects::isNull).filter(e -> e.equals(true)).findFirst();
        return predicate.isEmpty();
    }

    /**
     * This checks that BigDecimal properly represents price attribute.
     *
     * @param price subject of validation.
     * @return true if price is non-negative with precision not greater than 12 and scale not greater than 2.
     */
    public boolean isValidPrice(BigDecimal price) {
        if (isNull(price)) {
            return false;
        }
        String priceString = price.toString();
        int precision = priceString.indexOf('.');
        int scale = priceString.length() - precision - 1;
        return price.abs().equals(price) && scale <= 2 && precision <= 12;
    }

    /**
     * This checks that GiftCertificate entity has valid attributes to update before is passed to repository layer.
     *
     * @param certificate not null, with attributes required to patch.
     * @return true if id is not null AND at least one (and also not every) of following attributes is present: name,
     * description, price, duration, false if id is null OR all attributes are present(this requires different http
     * method) OR if any of not null attributes is invalid.
     */
    public boolean isCertificatePatchable(GiftCertificate certificate) {
        return !isNull(certificate) && isValidLong(certificate.getId()) &&
                hasCertificatePatchableNumberOfAttributes(certificate) && isStringNullOrValid(certificate.getName()) &&
                isStringNullOrValid(certificate.getDescription()) && isDurationNullOrValid(certificate.getDuration()) &&
                isPriceNullOrValid(certificate.getPrice());
    }

    private boolean isStringNullOrValid(String string) {
        if (isNull(string)) {
            return true;
        } else {
            return !string.isEmpty();
        }
    }

    private boolean isDurationNullOrValid(Integer duration) {
        return isNull(duration) || duration >= 0;
    }

    private boolean isPriceNullOrValid(BigDecimal price) {
        if (isNull(price)) {
            return true;
        }
        String priceString = price.toString();
        int precision = priceString.indexOf('.');
        int scale = priceString.length() - precision - 1;
        return price.abs().equals(price) && scale <= 2 && precision <= 12;
    }

    private boolean hasCertificatePatchableNumberOfAttributes(GiftCertificate certificate) {
        boolean isAnyPresent = !isNull(certificate.getName()) || !isNull(certificate.getDescription()) ||
                !isNull(certificate.getDuration()) || !isNull(certificate.getPrice());
        boolean areAllPresent = isNull(certificate.getName()) && isNull(certificate.getDescription()) &&
                isNull(certificate.getDuration()) && isNull(certificate.getPrice());
        return isAnyPresent && !areAllPresent;
    }

    private boolean isValidDuration(Integer duration) {
        return !isNull(duration) && duration >= 0;
    }

    /**
     * This checks that GiftCertificate entities have valid attributes before they are passed to repository layer.
     *
     * @param certificates not null, with present id OR not null nor empty name.
     * @return true if array is not null nor empty and each certificate has at least one correct attribute.
     */
    public boolean validateCertificatesBeforeSearch(GiftCertificate[] certificates) {
        if (!isNull(certificates) && certificates.length > 0) {
            return Arrays.stream(certificates)
                         .map(c -> isValidLong(c.getId()) || isValidString(c.getName()))
                         .noneMatch(e -> e.equals(Boolean.FALSE));
        } else {
            return false;
        }
    }

    /**
     * This checks that Tags entities have valid attributes before they are passed to repository layer.
     *
     * @param tags not null, with present id OR not null nor empty name.
     * @return true if array is not null nor empty and each tag has at least one correct attribute, otherwise false.
     */
    public boolean validateTagsBeforeSearch(Tag[] tags) {
        if (!isNull(tags) && tags.length > 0) {
            return Arrays.stream(tags)
                         .map(c -> isValidLong(c.getId()) || isValidString(c.getName()))
                         .noneMatch(e -> e.equals(Boolean.FALSE));
        } else {
            return false;
        }
    }

    /**
     * This checks that Users entities have valid attributes before they are passed to repository layer.
     *
     * @param users not null, with present id.
     * @return true if array is not null nor empty and each has present id.
     */
    public boolean validateUsersBeforeSearch(User[] users) {
        if (!isNull(users) && users.length > 0) {
            return Arrays.stream(users).map(c -> isValidLong(c.getId())).noneMatch(e -> e.equals(Boolean.FALSE));
        } else {
            return false;
        }
    }
}
