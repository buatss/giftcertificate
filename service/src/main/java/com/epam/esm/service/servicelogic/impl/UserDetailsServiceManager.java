package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.model.entity.Role;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.RoleRepository;
import com.epam.esm.dao.repository.UserRepository;
import com.epam.esm.service.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * This provides implementation of UserDetailsManager adapted to this application in accordance to custom schema.
 */
@Service
public class UserDetailsServiceManager extends AbstractService<User> implements UserDetailsManager {

    private final UserRepository userRepo;
    private final RoleRepository roleRepo;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserDetailsServiceManager(UserRepository userRepo, RoleRepository roleRepo,
            PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * This loads user persisted in database through searching by name.
     *
     * @param username the username identifying the user whose data is required.
     * @return UserDetails.
     * @throws UsernameNotFoundException if such record not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = handleOptionalResult(userRepo.findUserByName(username));
        if (user != null) {
            return user;
        }
        throw new UsernameNotFoundException("User '" + username + "' not found");
    }

    /**
     * This creates and persists to database user with encrypted password.
     *
     * @param user with not null, nor empty name & password
     */
    @Override
    public void createUser(UserDetails user) {
        User userEntity = new User();
        userEntity.setUserName(user.getUsername());
        userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
        userEntity.setRoles(getUserRoleOrCreateIfNotExists());
        Optional<UserDto> userDto = userRepo.saveUser(userEntity);
        if (userDto.isEmpty()) {
            throw new BadRequestException("User not created");
        }
    }

    @Override
    public void updateUser(UserDetails user) {
        throw new UnsupportedOperationException("This is not supported for purpose of this application");
    }

    @Override
    public void deleteUser(String username) {
        throw new UnsupportedOperationException("This is not supported for purpose of this application");
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        throw new UnsupportedOperationException("This is not supported for purpose of this application");
    }

    /**
     * This returns information that is user persisted in database or not.
     *
     * @param username with not null name.
     * @return true if user is persisted in database, false if not.
     */
    @Override
    public boolean userExists(String username) {
        try {
            handleOptionalResult(userRepo.findUserByName(username));
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
        return true;
    }

    private Set<Role> getUserRoleOrCreateIfNotExists() {
        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        role.setName("user");
        Optional<Role> result = roleRepo.findRoleByName(role);
        if (result.isEmpty()) {
            result = roleRepo.createRole(role);
            if (result.isEmpty()) {
                throw new BadRequestException("Such role not found.");
            }
        }
        roles.add(result.get());
        return roles;
    }
}
