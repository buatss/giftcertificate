package com.epam.esm.service.servicelogic;

import com.epam.esm.dao.model.dto.UserDto;

import java.util.List;

/**
 * Implementing this interface enables performing only read operations on User Entity
 */
public interface UserService {
    /**
     * This makes sure that passed argument is valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated arguments is not corrected.
     *
     * @param id not null with not null id
     * @return UserDto of persisted user in database
     */
    UserDto findUserById(Long id);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param pageNumber positive integer
     * @param pageSize positive integer not greater than 10
     * @param sortAscending if true sorts ascending by id, false descending
     * @return List of UserDto
     */
    List<UserDto> findUsersById(Integer pageNumber, Integer pageSize, Boolean sortAscending);
}
