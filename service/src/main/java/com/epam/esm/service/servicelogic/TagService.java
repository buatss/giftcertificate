package com.epam.esm.service.servicelogic;

import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;

import java.util.List;

/**
 * Implementing this interface allows CRD operations and tag with input validation and handling retrieved result from
 * persistence layer.
 */
public interface TagService {
    /**
     * This makes sure that Tag meets requirements to be persisted in database, then calls proper repository method
     * to execute creation and wraps out received result.
     * Validated arguments are not corrected.
     *
     * @param tag not null with null id and not null nor empty name.
     * @return TagDto.
     */
    TagDto createTag(Tag tag);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to establish association
     * and wraps out received result.
     * Validated arguments are not corrected.
     *
     * @param certificate not null with not null id of certificate persisted in db.
     * @param tag not null with not null id of tag persisted in db.
     * @return TagDto.
     */
    TagDto connectTagAndCertificate(GiftCertificate certificate, Tag tag);

    /**
     * This makes sure that passed argument is valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated argument is not corrected.
     *
     * @param tag not null with not null id.
     * @return TagDto.
     */
    TagDto getTagById(Tag tag);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return List of TagDto.
     */
    List<TagDto> getTagsById(Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to search for records
     * and unwraps received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param tags not null with not null nor empty name or id of persisted tag in database.
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return List of TagDto.
     */
    List<TagDto> findByNamesAndIds(Tag[] tags, Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to search for records
     * and unwraps received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return List of TagDto unassociated tags with certificates.
     */
    List<TagDto> getUnassignedTags(Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * This does nothing more than unwrap retrieved result, there are no arguments to pass.
     *
     * @return TagDto of most popular tag assigned to certificates from user's orders, who spent most money among all
     * users.
     */
    TagDto getMostFrequentTagFromUserWithMostExpensiveOrdersTotal();

    /**
     * This makes sure that passed argument is valid, then calls proper repository method to remove record from database
     * and unwraps received result.
     *
     * @param tag not null with not null id.
     * @return TagDto of deleted tag from database.
     */
    TagDto deleteTagById(Tag tag);
}
