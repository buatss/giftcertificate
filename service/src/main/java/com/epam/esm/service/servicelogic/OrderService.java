package com.epam.esm.service.servicelogic;

import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;

import java.util.List;

/**
 * Implementing this interface provides CR operations on Order Entity with input validation and handling retrieved
 * result from persistence layer. In most cases data is retrieved or exception is thrown.
 */
public interface OrderService {
    /**
     * This makes sure that Order meets requirements to be persisted in database, then calls proper repository method
     * to execute creation and wraps out received result.
     * Validated arguments are not corrected.
     *
     * @param certificateId not null of certificate persisted in db
     * @param userId not null of user persisted in db
     * @return OrderDto
     */
    OrderDto createOrder(Long certificateId, Long userId);

    /**
     * This makes sure that passed argument is valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated argument is not corrected.
     *
     * @param order not null, not null id of order persisted in db
     * @return OrderDto
     */
    OrderDto findOrderById(Order order);

    /**
     * @param pageNumber positive integer
     * @param pageSize positive integer not greater than 10
     * @param sortAscending if true sorts ascending by id, false descending
     * @return List of OrderDto
     */
    List<OrderDto> findOrdersById(Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * @param users not null, not null id of user persisted in db
     * @param pageNumber positive integer
     * @param pageSize positive integer not greater than 10
     * @param sortAscending if true sorts ascending by id, false descending
     * @return List of OrderDto
     */
    List<OrderDto> findOrdersByUserId(User[] users, Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * @param certificates not null, not null id of certificates persisted in db
     * @param pageNumber positive integer
     * @param pageSize positive integer not greater than 10
     * @param sortAscending if true sorts ascending by id, false descending
     * @return List of OrderDto
     */
    List<OrderDto> findOrdersByCertificateId(GiftCertificate[] certificates, Integer pageNumber, Integer pageSize,
            Boolean sortAscending);

}
