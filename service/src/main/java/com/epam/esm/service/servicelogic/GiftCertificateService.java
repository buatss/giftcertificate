package com.epam.esm.service.servicelogic;

import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;

import java.util.List;

/**
 * Implementing this interface provides CRUD operations on GiftCertificate Entity with input validation and handling
 * retrieved result from persistence layer. In most cases data is retrieved or exception is thrown.
 */
public interface GiftCertificateService {

    /**
     * This makes sure that GiftCertificate meets requirements to be persisted in database, then calls proper repository
     * method to execute creation and wraps out received result.
     * Validated arguments are not corrected.
     *
     * @param certificate not null with attributes meeting following conditions: -id is null -certificate is not null
     * -price has precision maximum 12 and scale maximum 2 -durations is not null and is non-negative
     * -name is not null -description is not null.
     * @return GiftCertificateDto.
     */
    GiftCertificateDto createGiftCertificate(GiftCertificate certificate);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to establish association
     * and wraps out received result.
     * Validated arguments are not corrected.
     *
     * @param certificate not null with not null id.
     * @param tag not null with not null id.
     * @return GiftCertificateDto.
     */
    GiftCertificateDto connectCertificateAndTag(GiftCertificate certificate, Tag tag);

    /**
     * This makes sure that passed argument is valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated arguments is not corrected.
     *
     * @param certificate not null with not nul id.
     * @return GiftCertificateDto.
     */
    GiftCertificateDto findGiftCertificateById(GiftCertificate certificate);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to find record and unwraps
     * received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending if null ascending.
     * @return List of GiftCertificateDto.
     */
    List<GiftCertificateDto> findGiftCertificatesById(Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to search for records
     * and unwraps received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param certificates not null values, they should meet at least one requirement: not null nor empty name, not
     * null id, if array is empty returns Optional of empty list, if certificate is empty then is
     * ignored in searching.
     * @param pageNumber not null positive integer if incorrect by default 1.
     * @param pageSize not null positive integer if incorrect by default 5.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return list of certificates with specified name, empty list if not found any matching pattern.
     */
    List<GiftCertificateDto> searchByIdsAndNames(GiftCertificate[] certificates, Integer pageNumber, Integer pageSize,
            Boolean sortAscending);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to search for records
     * and unwraps received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param tags not null values, they should meet at least one requirement: not null nor empty name, not
     * null id, if array is empty returns Optional of empty list, if tag is without any value then
     * is ignored.
     * @param pageNumber not null positive integer if incorrect by default 1.
     * @param pageSize not null positive integer if incorrect by default 5.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return list of certificates matching input parameters sorted, if there is no result then empty list is returned.
     */
    List<GiftCertificateDto> searchByManyTagsIdAndName(Tag[] tags, Integer pageNumber, Integer pageSize,
            Boolean sortAscending);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to search for records
     * and unwraps received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param pageNumber positive integer.
     * @param pageSize positive integer not greater than 10.
     * @param sortAscending if true sorts ascending by id, false descending.
     * @return List of GiftCertificateDto representing page with input parameters.
     */
    List<GiftCertificateDto> findUnassignedCertificates(Integer pageNumber, Integer pageSize, Boolean sortAscending);

    /**
     * This makes sure that arguments are valid, then calls proper repository method to search for records
     * and unwraps received result.
     * Validated arguments: pageNumber, pageSize, sortAscending are corrected if invalid.
     *
     * @param certificate not null, with not null id and at least one other not null attribute, requirements for not
     * null attributes: -price is positive and has precision maximum 12 and scale maximum 2 -duration
     * is positive integer -name is not empty -description is not empty.
     * @return true if successfully updated(even if all attributes were same before), false if update fails.
     */
    GiftCertificateDto updateGiftCertificate(GiftCertificate certificate);

    /**
     * This makes sure that passed argument is valid, then calls proper repository method to remove record from database
     * and unwraps received result.
     *
     * @param certificate not null with not null id.
     * @return GiftCertificateDto.
     */
    GiftCertificateDto deleteGiftCertificateById(GiftCertificate certificate);
}
