package com.epam.esm.service.exception;

/**
 * This exception is thrown in case when expected resource is not found.
 */
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
