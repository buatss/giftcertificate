package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.UserDto;
import com.epam.esm.dao.repository.UserRepository;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.exception.ResourceNotFoundException;
import com.epam.esm.service.validator.InputValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    private static final Boolean SORTED_ASCENDING_PRESENT = Boolean.TRUE;
    private final int PAGE_NUMBER = 1;
    private final int PAGE_SIZE = 5;
    @Mock
    InputValidator validator;
    @Mock
    UserRepository repository;
    @InjectMocks
    UserServiceImpl service;

    @Test
    void findUserById_validInputFound_returnsDto() {
        Long userId = 1L;
        UserDto userDto = new UserDto();
        Optional<UserDto> result = Optional.of(userDto);
        when(validator.isValidLong(userId)).thenReturn(true);
        when(repository.findUserById(userId)).thenReturn(result);
        assertEquals(result.get(), service.findUserById(userId));
    }

    @Test
    void findUserById_validInputNotFound_throwsResourceNotFoundException() {
        Long userId = 1L;
        when(validator.isValidLong(userId)).thenReturn(true);
        when(repository.findUserById(userId)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.findUserById(userId));
    }

    @Test
    void findUserById_invalidInput_throwsBadRequestException() {
        Long userId = 1L;
        when(validator.isValidLong(userId)).thenReturn(false);
        assertThrows(BadRequestException.class, () -> service.findUserById(userId));
    }

    @Test
    void findUsersById_validInputFound_returnsListOfDto() {
        Optional<List<UserDto>> result = Optional.of(List.of(new UserDto()));

        when(repository.findUsersById(1, 5, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.findUsersById(1, 5, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findUsersById_validInputNotFound_returnsEmptyList() {
        Optional<List<UserDto>> result = Optional.of(Collections.emptyList());

        when(repository.findUsersById(1, 5, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.findUsersById(1, 5, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findUsersById_invalidPageParams_returnsListWithDefaultParams() {
        List<UserDto> result = List.of(new UserDto(), new UserDto());

        when(repository.findUsersById(PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(Optional.of(result));

        assertEquals(result, service.findUsersById(null, null, null));
    }
}
