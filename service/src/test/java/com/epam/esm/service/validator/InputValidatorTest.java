package com.epam.esm.service.validator;

import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.model.entity.User;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class InputValidatorTest {
    private static final int MAXIMUM_PAGE_SIZE = 10;
    private InputValidator validator;

    @BeforeEach
    void setUp() {
        validator = new InputValidator();
    }

    @Test
    void validatePageNumber_valid_returnsTrue() {
        Integer pageNumber = randomPositiveInt(Integer.MAX_VALUE);
        assertTrue(validator.validatePageNumber(pageNumber));
    }

    @Test
    void validatePageNumber_invalid_returnsFalse() {
        SoftAssertions softly = new SoftAssertions();
        Integer negativePageNumber = randomNonPositiveInt();
        Integer nullPageNumber = null;

        softly.assertThat(validator.validatePageNumber(negativePageNumber)).isFalse();
        softly.assertThat(validator.validatePageNumber(nullPageNumber)).isFalse();
        softly.assertAll();
    }

    @Test
    void validatePageSize_valid_returnsTrue() {
        Integer pageSize = randomPositiveInt(MAXIMUM_PAGE_SIZE);

        assertTrue(validator.validatePageSize(pageSize));
    }

    @Test
    void validatePageSize_invalid_returnsFalse() {
        SoftAssertions softly = new SoftAssertions();
        Integer tooBigPageSize = randomPositiveInt(Integer.MAX_VALUE);
        Integer negativePageSize = randomNonPositiveInt();
        Integer nullPageSize = null;

        softly.assertThat(validator.validatePageSize(tooBigPageSize)).isFalse();
        softly.assertThat(validator.validatePageSize(nullPageSize)).isFalse();
        softly.assertThat(validator.validatePageSize(negativePageSize)).isFalse();
        softly.assertAll();

    }

    @ParameterizedTest
    @MethodSource("com.epam.esm.service.utils.DataProvider#provideValidCertificatesToCreate")
    void validateCertificateBeforeCreate_valid_returnsTrue(GiftCertificate input) {
        assertTrue(validator.validateBeforeCreate(input));
    }

    @ParameterizedTest
    @MethodSource("com.epam.esm.service.utils.DataProvider#provideInvalidCertificatesToCreate")
    void validateCertificateBeforeCreate_invalid_returnsFalse(GiftCertificate input) {
        assertFalse(validator.validateBeforeCreate(input));
    }

    @ParameterizedTest
    @MethodSource("com.epam.esm.service.utils.DataProvider#provideValidTagsToCreate")
    void validateTagBeforeCreate_invalid_returnsTrue(Tag input) {
        assertTrue(validator.validateBeforeCreate(input));
    }

    @ParameterizedTest
    @MethodSource("com.epam.esm.service.utils.DataProvider#provideInvalidTagsToCreate")
    void validateTagBeforeCreate_invalid_returnsFalse(Tag input) {
        assertFalse(validator.validateBeforeCreate(input));
    }

    @Test
    void isValidString() {
        SoftAssertions softly = new SoftAssertions();
        final String str = "test";

        softly.assertThat(validator.isValidString(str)).isTrue();
        softly.assertThat(validator.isValidString(null)).isFalse();
        softly.assertThat(validator.isValidString("")).isFalse();

        softly.assertAll();
    }

    @Test
    void isValidLong() {
        assertFalse(validator.isValidLong(null, null));
    }

    @Test
    void isValidPrice() {
        assertFalse(validator.isValidPrice(new BigDecimal("10.031")));
    }

    @ParameterizedTest
    @MethodSource("com.epam.esm.service.utils.DataProvider#providePatchableCertificates")
    void isCertificatePatchable_patchableCertificate_returnsTrue(GiftCertificate input) {
        assertTrue(validator.isCertificatePatchable(input));
    }

    @ParameterizedTest
    @MethodSource("com.epam.esm.service.utils.DataProvider#provideUnpatchableCertificates")
    void isCertificatePatchable_unpatchableCertificate_returnsFalse(GiftCertificate input) {
        assertFalse(validator.isCertificatePatchable(input));
    }

    private int randomNonPositiveInt() {
        int max = 0;
        int min = Integer.MIN_VALUE + 1;
        return (int) ((Math.random() * (max - min)) + min);
    }

    private int randomPositiveInt(int max) {
        int min = 1;
        return (int) ((Math.random() * (max - min)) + min);
    }

    @Test
    void validateCertificatesBeforeSearch_validInput_returnsTrue() {
        GiftCertificate certificate1 = new GiftCertificate();
        GiftCertificate certificate2 = new GiftCertificate();
        GiftCertificate certificate3 = new GiftCertificate();
        certificate1.setId(1L);
        certificate2.setName("name");
        certificate3.setId(2L);
        certificate3.setName("name2");
        GiftCertificate[] certificates = {certificate1, certificate2, certificate3};

        assertTrue(validator.validateCertificatesBeforeSearch(certificates));
    }

    @Test
    void validateCertificatesBeforeSearch_invalidInput_returnsFalse() {
        SoftAssertions softly = new SoftAssertions();

        GiftCertificate certificate1 = new GiftCertificate();
        GiftCertificate certificate2 = new GiftCertificate();
        certificate2.setName("");
        GiftCertificate[] certificates = {certificate1, certificate2};
        GiftCertificate[] certificatesEmpty = {};

        softly.assertThat(validator.validateCertificatesBeforeSearch(certificates)).isFalse();
        softly.assertThat(validator.validateCertificatesBeforeSearch(certificatesEmpty)).isFalse();
        softly.assertAll();
    }

    @Test
    void validateTagsBeforeSearch_validInput_returnsTrue() {
        Tag tag1 = new Tag();
        Tag tag2 = new Tag();
        Tag tag3 = new Tag();
        tag1.setId(1L);
        tag2.setName("name");
        tag3.setId(2L);
        tag3.setName("name2");
        Tag[] tags = {tag1, tag2, tag3};

        assertTrue(validator.validateTagsBeforeSearch(tags));
    }

    @Test
    void validateTagsBeforeSearch_invalidInput_returnsFalse() {
        SoftAssertions softly = new SoftAssertions();

        Tag tag1 = new Tag();
        Tag tag2 = new Tag();
        tag2.setName("");
        Tag[] tags = {tag1, tag2};
        Tag[] tagsEmpty = {};

        softly.assertThat(validator.validateTagsBeforeSearch(tags)).isFalse();
        softly.assertThat(validator.validateTagsBeforeSearch(tagsEmpty)).isFalse();
        softly.assertAll();

    }

    @Test
    void validateUsersBeforeSearch_valid_returnsTrue() {
        User user = new User();
        user.setId(1L);
        User[] users = {user, user};

        assertTrue(validator.validateUsersBeforeSearch(users));
    }

    @Test
    void validateUsersBeforeSearch_invalid_returnsFalse() {
        SoftAssertions softly = new SoftAssertions();

        User[] usersEmpty = {};
        User[] usersInvalid = {new User(), new User()};

        softly.assertThat(validator.validateUsersBeforeSearch(usersEmpty)).isFalse();
        softly.assertThat(validator.validateUsersBeforeSearch(usersInvalid)).isFalse();
        softly.assertAll();
    }
}
