package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.GiftCertificateDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.GiftCertificateRepository;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.exception.ResourceNotFoundException;
import com.epam.esm.service.validator.InputValidator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GiftCertificateServiceImplTest {

    private final GiftCertificate certificate = new GiftCertificate();
    private final Tag tag = new Tag();
    private final GiftCertificateDto certificateDto = new GiftCertificateDto();
    private final int PAGE_NUMBER = 1;
    private final int PAGE_SIZE = 5;
    private final Boolean SORTED_ASCENDING_PRESENT = Boolean.TRUE;
    private final Tag[] tags = {};
    private final GiftCertificate[] certificates = {};
    @Mock
    private InputValidator validator;
    @Mock
    private GiftCertificateRepository repository;
    @InjectMocks
    private GiftCertificateServiceImpl service;

    @Test
    void createGiftCertificate_validInputCreated_returnsDto() {
        Optional<GiftCertificateDto> result = Optional.of(certificateDto);
        when(validator.validateBeforeCreate(certificate)).thenReturn(true);
        when(repository.createGiftCertificate(certificate)).thenReturn(result);
        assertEquals(result.get(), service.createGiftCertificate(certificate));
    }

    @Test
    void createGiftCertificate_validInputNotCreated_resourceNotFoundException() {
        when(validator.validateBeforeCreate(certificate)).thenReturn(true);
        when(repository.createGiftCertificate(certificate)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.createGiftCertificate(certificate));
    }

    @Test
    void createGiftCertificate_invalidInput_throwsBadRequestException() {
        when(validator.validateBeforeCreate(certificate)).thenReturn(false);
        assertThrows(BadRequestException.class, () -> service.createGiftCertificate(certificate));
    }

    @Test
    void connectCertificateAndTag_validPairConnected_returnsPairWithDto() {
        Optional<GiftCertificateDto> result = Optional.of(certificateDto);

        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.connectCertificateAndTag(certificate, tag)).thenReturn(result);

        assertEquals(result.get(), service.connectCertificateAndTag(certificate, tag));
    }

    @Test
    void connectCertificateAndTag_invalidInput_throwsBadRequestException() {
        SoftAssertions softly = new SoftAssertions();

        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        when(validator.isValidLong(tag.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.connectCertificateAndTag(certificate, tag))
              .isInstanceOf(BadRequestException.class);

        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(tag.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.connectCertificateAndTag(certificate, tag))
              .isInstanceOf(BadRequestException.class);

        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.connectCertificateAndTag(certificate, tag))
              .isInstanceOf(BadRequestException.class);

        softly.assertAll();
    }

    @Test
    void connectCertificateAndTag_validInputNotConnected_throwsResourceNotFoundException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.connectCertificateAndTag(certificate, tag)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.connectCertificateAndTag(certificate, tag));
    }

    @Test
    void findGiftCertificateById_validInputFound_returnsDto() {
        Optional<GiftCertificateDto> result = Optional.of(certificateDto);
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(repository.findGiftCertificateById(certificate)).thenReturn(result);
        assertEquals(result.get(), service.findGiftCertificateById(certificate));
    }

    @Test
    void findGiftCertificateById_validInputNotFound_throwsResourceNotFoundException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(repository.findGiftCertificateById(certificate)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.findGiftCertificateById(certificate));
    }

    @Test
    void findGiftCertificatesById_validInputFound_returnsListOfDto() {
        Optional<List<GiftCertificateDto>> result = Optional.of(List.of(new GiftCertificateDto()));

        when(repository.findGiftCertificatesById(1, 5, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.findGiftCertificatesById(1, 5, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findGiftCertificatesById_validInputNotFound_returnsEmptyList() {
        Optional<List<GiftCertificateDto>> result = Optional.of(Collections.emptyList());

        when(repository.findGiftCertificatesById(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(service.findGiftCertificatesById(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT), result.get());
    }

    @Test
    void findGiftCertificatesById_invalidPageParams_returnsListOfDtoWithDefaultParams() {
        List<GiftCertificateDto> result = List.of(certificateDto, certificateDto);

        when(repository.findGiftCertificatesById(PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(Optional.of(result));

        assertEquals(service.findGiftCertificatesById(null, null, null), result);
    }

    @Test
    void findGiftCertificateById_invalidInput_throwsBadRequestException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        assertThrows(BadRequestException.class, () -> service.findGiftCertificateById(certificate));
    }

    @Test
    void searchByManyTagsIdAndName_validInputFound_returnsListOfDto() {
        List<GiftCertificateDto> expectedList = Collections.emptyList();
        when(validator.validateTagsBeforeSearch(tags)).thenReturn(true);
        when(repository.searchByManyTagsIdAndName(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(expectedList));
        assertEquals(expectedList,
                service.searchByManyTagsIdAndName(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void searchByManyTagsIdAndName_validInputNotFound_returnsEmptyList() {
        List<GiftCertificateDto> result = Collections.emptyList();
        when(validator.validateTagsBeforeSearch(tags)).thenReturn(true);
        when(repository.searchByManyTagsIdAndName(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(result));
        assertEquals(result, service.searchByManyTagsIdAndName(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void searchByManyTagsIdAndName_invalidInput_throwsBadRequestException() {
        when(validator.validateTagsBeforeSearch(tags)).thenReturn(false);
        assertThrows(BadRequestException.class,
                () -> service.searchByManyTagsIdAndName(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void searchByManyTagsIdAndName_invalidPageParams_returnsWithDefaultParams() {
        List<GiftCertificateDto> result = List.of(certificateDto, certificateDto);
        when(validator.validateTagsBeforeSearch(tags)).thenReturn(true);
        when(repository.searchByManyTagsIdAndName(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(result));
        assertEquals(result, service.searchByManyTagsIdAndName(tags, null, null, null));
    }

    @Test
    void searchByIdsAndNames_validInputFound_returnsListOfDto() {
        List<GiftCertificateDto> expectedList = new ArrayList<>();
        expectedList.add(certificateDto);
        when(validator.validateCertificatesBeforeSearch(certificates)).thenReturn(true);
        when(repository.searchByIdsAndNames(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(expectedList));
        assertEquals(expectedList,
                service.searchByIdsAndNames(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void searchByIdsAndNames_validInputNotFound_returnsEmptyList() {
        Optional<List<GiftCertificateDto>> result = Optional.of(Collections.emptyList());
        when(validator.validateCertificatesBeforeSearch(certificates)).thenReturn(true);
        when(repository.searchByIdsAndNames(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                result);
        assertEquals(service.searchByIdsAndNames(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT),
                result.get()
        );
    }

    @Test
    void searchByIdsAndNames_invalidInput_throwsBadRequestException() {
        assertThrows(BadRequestException.class,
                () -> service.searchByIdsAndNames(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void searchByIdsAndNames_invalidPageParams_returnsListOfDtoWithDefaultParams() {
        List<GiftCertificateDto> result = List.of(certificateDto, certificateDto);

        when(validator.validateCertificatesBeforeSearch(certificates)).thenReturn(true);
        when(repository.searchByIdsAndNames(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(result));
        assertEquals(service.searchByIdsAndNames(certificates, null, null, null), result);
    }

    @Test
    void findUnassignedCertificates_validPageParams_returnsListOfDto() {
        List<GiftCertificateDto> result = List.of(certificateDto, certificateDto);

        when(repository.findUnassignedCertificates(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(result));

        assertEquals(result, service.findUnassignedCertificates(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findUnassignedCertificates_validInputNotFound_returnsEmptyList() {
        List<GiftCertificateDto> result = Collections.emptyList();

        when(repository.findUnassignedCertificates(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(result));

        assertEquals(service.findUnassignedCertificates(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT), result);
    }

    @Test
    void findUnassignedCertificates_invalidPageParams_returnsWithDefaultPageParams() {
        List<GiftCertificateDto> result = List.of(certificateDto, certificateDto);

        when(repository.findUnassignedCertificates(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(result));

        assertEquals(service.findUnassignedCertificates(null, null, null), result);
    }

    @Test
    void updateGiftCertificate_validInput_returnsDto() {
        Optional<GiftCertificateDto> result = Optional.of(certificateDto);

        when(validator.validateBeforeCreate(certificate)).thenReturn(true);
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(repository.updateGiftCertificate(certificate)).thenReturn(result);

        assertEquals(result.get(), service.updateGiftCertificate(certificate));
    }

    @Test
    void updateGiftCertificate_validInputNotUpdated_throwsResourceNotFoundException() {
        when(validator.validateBeforeCreate(certificate)).thenReturn(true);
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(repository.updateGiftCertificate(certificate)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.updateGiftCertificate(certificate));
    }

    @Test
    void updateGiftCertificate_invalidInput_throwsBadRequestException() {
        SoftAssertions softly = new SoftAssertions();

        when(validator.validateBeforeCreate(certificate)).thenReturn(false);
        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.updateGiftCertificate(certificate))
              .isInstanceOf(BadRequestException.class);

        when(validator.validateBeforeCreate(certificate)).thenReturn(true);
        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.updateGiftCertificate(certificate))
              .isInstanceOf(BadRequestException.class);

        when(validator.validateBeforeCreate(certificate)).thenReturn(false);
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        softly.assertThatThrownBy(() -> service.updateGiftCertificate(certificate))
              .isInstanceOf(BadRequestException.class);

        softly.assertAll();
    }

    @Test
    void deleteGiftCertificateById_validInputDeleted_returnsDto() {
        Optional<GiftCertificateDto> result = Optional.of(certificateDto);

        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(repository.deleteGiftCertificateById(certificate)).thenReturn(result);

        assertEquals(result.get(), service.deleteGiftCertificateById(certificate));
    }

    @Test
    void deleteGiftCertificateById_validInputNotDeleted_throwsResourceNotFoundException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(repository.deleteGiftCertificateById(certificate)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.deleteGiftCertificateById(certificate));
    }

    @Test
    void deleteGiftCertificateById_invalidInput_throwsBadRequestException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(false);

        assertThrows(BadRequestException.class, () -> service.deleteGiftCertificateById(certificate));
    }
}
