package com.epam.esm.service.utils;

import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import org.junit.jupiter.params.provider.Arguments;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.stream.Stream;

public class DataProvider {
    public static Stream<Arguments> provideValidCertificatesToCreate() {
        return Stream.of(Arguments.of(new GiftCertificate(null,
                        "name",
                        "description",
                        new BigDecimal("10.2"),
                        15,
                        null,
                        null,
                        null,
                        null
                )),
                Arguments.of(new GiftCertificate(null,
                        "default",
                        "desc",
                        new BigDecimal("16.0"),
                        8,
                        null,
                        null,
                        Collections.emptySet(),
                        Collections.emptySet()
                )),
                Arguments.of(new GiftCertificate(null,
                        "default",
                        "desc",
                        new BigDecimal("14"),
                        211,
                        null,
                        null,
                        Collections.emptySet(),
                        Collections.emptySet()
                )),
                Arguments.of(new GiftCertificate(null,
                        "name",
                        "description",
                        new BigDecimal("14.01"),
                        2,
                        null,
                        null,
                        null,
                        null
                ))
        );
    }

    public static Stream<Arguments> provideInvalidCertificatesToCreate() {
        Stream<Arguments> patchableCertificates = providePatchableCertificates();
        Stream<Arguments> invalidCertificates = Stream.of(Arguments.of(new GiftCertificate(2L,
                        "name",
                        "description",
                        new BigDecimal("10.2"),
                        15,
                        null,
                        null,
                        null,
                        null
                )),
                Arguments.of(new GiftCertificate(0L,
                        "default",
                        "desc",
                        new BigDecimal("14.01"),
                        2,
                        null,
                        null,
                        Collections.emptySet(),
                        Collections.emptySet()
                )),
                Arguments.of(new GiftCertificate(null,
                        "",
                        "desc",
                        new BigDecimal("14.01"),
                        2,
                        null,
                        null,
                        Collections.emptySet(),
                        Collections.emptySet()
                )),
                Arguments.of(new GiftCertificate(null,
                        "default",
                        "desc",
                        new BigDecimal("14.011"),
                        2,
                        null,
                        null,
                        Collections.emptySet(),
                        Collections.emptySet()
                ))
        );
        return Stream.concat(invalidCertificates, patchableCertificates);
    }

    public static Stream<Arguments> provideValidTagsToCreate() {
        return Stream.of(Arguments.of(new Tag(null, "name", Collections.emptySet())),
                Arguments.of(new Tag(null, "n", null)),
                Arguments.of(new Tag(null, "n", Collections.emptySet())),
                Arguments.of(new Tag(null, "tag", null)),
                Arguments.of(new Tag(null, "example name", null))
        );
    }

    public static Stream<Arguments> provideInvalidTagsToCreate() {
        return Stream.of(Arguments.of(new Tag()),
                Arguments.of(new Tag(null, null, null)),
                Arguments.of(new Tag(1L, null, null)),
                Arguments.of(new Tag(null, "", null)),
                Arguments.of(new Tag(2L, "", null))
        );
    }

    public static Stream<Arguments> providePatchableCertificates() {
        return Stream.of(Arguments.of(new GiftCertificate(2L, "name", null, null, null, null, null, null, null)),
                Arguments.of(new GiftCertificate(4L, null, "description", null, null, null, null, null, null)),
                Arguments.of(new GiftCertificate(5L, null, null, null, 4, null, null, null, null)),
                Arguments.of(new GiftCertificate(6L, "name", null, BigDecimal.TEN, 12, null, null, null, null)),
                Arguments.of(new GiftCertificate(7L, null, null, BigDecimal.ONE, 4, null, null, null, null))
        );
    }

    public static Stream<Arguments> provideUnpatchableCertificates() {
        Stream<Arguments> validCertificatesToCreate = provideValidCertificatesToCreate();
        Stream<Arguments> unpatchableCertificates =
                Stream.of(Arguments.of(new GiftCertificate(null, "name", null, null, null, null, null, null, null)),
                        Arguments.of(new GiftCertificate(4L, "", "description", null, null, null, null, null, null)),
                        Arguments.of(new GiftCertificate(null, null, null, null, 4, null, null, null, null)),
                        Arguments.of(new GiftCertificate(5L,
                                "name",
                                null,
                                new BigDecimal("0.001"),
                                12,
                                null,
                                null,
                                null,
                                null
                        )),
                        Arguments.of(new GiftCertificate(null, null, null, BigDecimal.ONE, 4, null, null, null, null))
                );
        return Stream.concat(validCertificatesToCreate, unpatchableCertificates);
    }
}
