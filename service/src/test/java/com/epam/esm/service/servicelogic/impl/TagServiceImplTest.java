package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.TagDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Tag;
import com.epam.esm.dao.repository.TagRepository;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.exception.ResourceNotFoundException;
import com.epam.esm.service.validator.InputValidator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TagServiceImplTest {
    private static final Boolean SORTED_ASCENDING_PRESENT = Boolean.TRUE;
    private final int PAGE_NUMBER = 1;
    private final int PAGE_SIZE = 5;
    private final GiftCertificate certificate = new GiftCertificate();
    private final Tag tag = new Tag();
    private final Tag[] tags = {tag};
    private final TagDto tagDto = new TagDto();

    @Mock
    InputValidator validator;
    @Mock
    TagRepository repository;
    @InjectMocks
    TagServiceImpl service;

    @Test
    void createTag_validInputCreated_returnsDto() {
        Optional<TagDto> result = Optional.of(tagDto);

        when(validator.validateBeforeCreate(tag)).thenReturn(true);
        when(repository.createTag(tag)).thenReturn(result);

        assertEquals(result.get(), service.createTag(tag));
    }

    @Test
    void createTag_validInputNotCreated_throwsResourceNotFoundException() {
        when(validator.validateBeforeCreate(tag)).thenReturn(true);
        when(repository.createTag(tag)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.createTag(tag));
    }

    @Test
    void createTag_invalidInput_throwsBadRequestException() {
        when(validator.validateBeforeCreate(tag)).thenReturn(false);

        assertThrows(BadRequestException.class, () -> service.createTag(tag));
    }

    @Test
    void connectTagAndCertificate_validPairConnected_returnsPairWithDto() {
        Optional<TagDto> result = Optional.of(tagDto);

        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.connectTagAndCertificate(certificate, tag)).thenReturn(result);

        assertEquals(result.get(), service.connectTagAndCertificate(certificate, tag));
    }

    @Test
    void connectTagAndCertificate_unvalidInput_throwsBadRequestException() {
        SoftAssertions softly = new SoftAssertions();

        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        when(validator.isValidLong(tag.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.connectTagAndCertificate(certificate, tag))
              .isInstanceOf(BadRequestException.class);

        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(tag.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.connectTagAndCertificate(certificate, tag))
              .isInstanceOf(BadRequestException.class);

        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.connectTagAndCertificate(certificate, tag))
              .isInstanceOf(BadRequestException.class);

        softly.assertAll();
    }

    @Test
    void connectTagAndCertificate_validInputNotConnected_throwsResourceNotFoundException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.connectTagAndCertificate(certificate, tag)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.connectTagAndCertificate(certificate, tag));
    }

    @Test
    void getTagById_validInputFound_returnsDto() {
        Optional<TagDto> result = Optional.of(tagDto);

        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.findTagById(tag)).thenReturn(result);

        assertEquals(result.get(), (service.getTagById(tag)));
    }

    @Test
    void getTagById_validInputNotFound_throwsResourceNotFoundException() {
        Optional<TagDto> result = Optional.of(tagDto);

        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.findTagById(tag)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.getTagById(tag));
    }

    @Test
    void getTagById_invalidInput_throwsBadRequestException() {
        when(validator.isValidLong(tag.getId())).thenReturn(false);

        assertThrows(BadRequestException.class, () -> service.getTagById(tag));
    }

    @Test
    void getTagByIds_validInputFound_returnsListOfDto() {
        Optional<List<TagDto>> result = Optional.of(List.of(new TagDto()));

        when(repository.findTagsById(1, 5, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.getTagsById(1, 5, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void getTagByIds_validInputNotFound_returnsEmptyList() {
        Optional<List<TagDto>> result = Optional.of(Collections.emptyList());

        when(repository.findTagsById(1, 5, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.getTagsById(1, 5, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void getTagByIds_invalidPageParams_returnsListWithDefaultParams() {
        List<TagDto> result = List.of(tagDto, tagDto);

        when(repository.findTagsById(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(Optional.of(result));

        assertEquals(service.getTagsById(null, null, null), result);
    }

    @Test
    void getTagByName_validInputFound_returnsDto() {
        Optional<List<TagDto>> result = Optional.of(List.of(tagDto));

        when(validator.validateTagsBeforeSearch(tags)).thenReturn(true);
        when(repository.findTagsByNamesAndIds(tags,
                PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(result);

        assertEquals(result.get(), service.findByNamesAndIds(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void getTagByName_validInputNotFound_returnsEmptyList() {
        Optional<List<TagDto>> result = Optional.of(Collections.emptyList());

        when(validator.validateTagsBeforeSearch(tags)).thenReturn(true);
        when(repository.findTagsByNamesAndIds(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(
                Optional.of(Collections.emptyList()));

        assertEquals(result.get(), service.findByNamesAndIds(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void getTagByName_invalidInput_throwsBadRequestException() {
        when(validator.validateTagsBeforeSearch(tags)).thenReturn(false);

        assertThrows(BadRequestException.class,
                () -> service.findByNamesAndIds(tags, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void getUnassignedTags_validInputFound_returnsListOfDto() {
        Optional<List<TagDto>> result = Optional.of(List.of(tagDto));

        when(repository.findUnassignedTags(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.getUnassignedTags(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void getUnassignedTags_validInputNotFound_returnsEmptyList() {
        List<TagDto> result = Collections.emptyList();

        when(repository.findUnassignedTags(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(Optional.of(
                result));

        assertEquals(result, service.getUnassignedTags(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void getUnassignedTags_invalidParams_returnsListWithDefaultParams() {
        List<TagDto> result = List.of(tagDto, tagDto);

        when(repository.findUnassignedTags(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(Optional.of(
                result));

        assertEquals(result, service.getUnassignedTags(null, null, null));

    }

    @Test
    void getMostFrequentTagFromUserWithMostExpensiveOrdersTotal_found_returnsDto() {
        Optional<TagDto> result = Optional.of(tagDto);

        when(repository.findMostFrequentTagFromUserWithMostExpensiveOrdersTotal()).thenReturn(result);

        assertEquals(result.get(), service.getMostFrequentTagFromUserWithMostExpensiveOrdersTotal());
    }

    @Test
    void getMostFrequentTagFromUserWithMostExpensiveOrdersTotal_notFound_throwsResourceNotFoundException() {
        when(repository.findMostFrequentTagFromUserWithMostExpensiveOrdersTotal()).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> service.getMostFrequentTagFromUserWithMostExpensiveOrdersTotal()
        );
    }

    @Test
    void deleteTagById_validInputDeleted_returnsDto() {
        Optional<TagDto> result = Optional.of(tagDto);

        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.deleteTagById(tag)).thenReturn(result);

        assertEquals(result.get(), service.deleteTagById(tag));
    }

    @Test
    void deleteTagById_validInputNotDeleted_throwsResourceNotFoundException() {
        when(validator.isValidLong(tag.getId())).thenReturn(true);
        when(repository.deleteTagById(tag)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.deleteTagById(tag));
    }

    @Test
    void deleteTagById_invalidInput_throwsBadRequestException() {
        when(validator.isValidLong(tag.getId())).thenReturn(false);

        assertThrows(BadRequestException.class, () -> service.deleteTagById(tag));
    }
}
