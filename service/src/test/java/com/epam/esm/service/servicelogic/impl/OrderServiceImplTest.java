package com.epam.esm.service.servicelogic.impl;

import com.epam.esm.dao.model.dto.OrderDto;
import com.epam.esm.dao.model.entity.GiftCertificate;
import com.epam.esm.dao.model.entity.Order;
import com.epam.esm.dao.model.entity.User;
import com.epam.esm.dao.repository.OrderRepository;
import com.epam.esm.service.exception.BadRequestException;
import com.epam.esm.service.exception.ResourceNotFoundException;
import com.epam.esm.service.validator.InputValidator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {
    private static final Boolean SORTED_ASCENDING_PRESENT = Boolean.TRUE;
    private static final Boolean SORTED_ASCENDING_ABSENT = null;

    private final GiftCertificate certificate = new GiftCertificate();
    private final GiftCertificate[] certificates = {new GiftCertificate()};
    private final User user = new User();
    private final User[] users = {user};
    private final OrderDto orderDto = new OrderDto();
    private final int PAGE_NUMBER = 1;
    private final int PAGE_SIZE = 5;
    @Mock
    InputValidator validator;
    @Mock
    OrderRepository repository;
    @InjectMocks
    OrderServiceImpl service;

    @Test
    void createOrder_validInputCreated_returnsDto() {
        final Optional<OrderDto> result = Optional.of(orderDto);

        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(user.getId())).thenReturn(true);
        when(repository.createOrder(certificate.getId(), user.getId())).thenReturn(result);

        assertEquals(result.get(), service.createOrder(certificate.getId(), user.getId()));
    }

    @Test
    void createOrder_validInputNotCreated_throwsResourceNotFoundException() {
        when(validator.isValidLong(certificate.getId())).thenReturn(true);
        when(validator.isValidLong(user.getId())).thenReturn(true);
        when(repository.createOrder(certificate.getId(), user.getId())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.createOrder(certificate.getId(), user.getId()));
    }

    @Test
    void createOrder_invalidInput_throwsBadRequestException() {
        SoftAssertions softly = new SoftAssertions();

        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        when(validator.isValidLong(user.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.createOrder(certificate.getId(), user.getId()))
              .isInstanceOf(BadRequestException.class);

        when(validator.isValidLong(user.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.createOrder(certificate.getId(), user.getId()))
              .isInstanceOf(BadRequestException.class);

        when(validator.isValidLong(certificate.getId())).thenReturn(false);
        softly.assertThatThrownBy(() -> service.createOrder(certificate.getId(), user.getId()))
              .isInstanceOf(BadRequestException.class);

        softly.assertAll();
    }

    @Test
    void findOrderById_validInputFound_returnsDto() {
        final Optional<OrderDto> result = Optional.of(orderDto);
        Order order = new Order();

        when(validator.isValidLong(order.getId())).thenReturn(true);
        when(repository.findOrderById(order)).thenReturn(result);

        assertEquals(result.get(), service.findOrderById(order));
    }

    @Test
    void findOrderById_validInputNotFound_throwsResourceNotFoundException() {
        Order order = new Order();

        when(validator.isValidLong(order.getId())).thenReturn(true);
        when(repository.findOrderById(order)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.findOrderById(order));
    }

    @Test
    void findOrderById_invalidInput_throwsBadRequestException() {
        Order order = new Order();

        when(validator.isValidLong(order.getId())).thenReturn(false);

        assertThrows(BadRequestException.class, () -> service.findOrderById(order));
    }

    @Test
    void findOrdersById_validInputFound_returnsListOfDto() {
        Optional<List<OrderDto>> result = Optional.of(new ArrayList<>());

        when(repository.findOrdersById(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.findOrdersById(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findOrdersById_validInputNotFound_returnsEmptyList() {
        List<OrderDto> result = Collections.emptyList();

        when(repository.findOrdersById(PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(Optional.of(result));

        assertEquals(result, service.findOrdersById(PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findOrdersById_invalidPageParams_returnsWithDefaultParams() {
        List<OrderDto> result = List.of(orderDto, orderDto);

        when(repository.findOrdersById(PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(Optional.of(result));

        assertEquals(result, service.findOrdersById(null, null, null));

    }

    @Test
    void findOrdersByUserId_validInputFound_returnsDto() {
        final Optional<List<OrderDto>> results = Optional.of(List.of(orderDto));

        when(validator.validateUsersBeforeSearch(users)).thenReturn(true);
        when(repository.findOrdersByUserId(users,
                PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(results);

        assertEquals(results.get(),
                service.findOrdersByUserId(users, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void findOrdersByUserId_validInputNotFound_returnsEmptyList() {
        Optional<List<OrderDto>> result = Optional.of(Collections.emptyList());

        when(validator.validateUsersBeforeSearch(users)).thenReturn(true);
        when(repository.findOrdersByUserId(users, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)).thenReturn(result);

        assertEquals(result.get(), service.findOrdersByUserId(users, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT));
    }

    @Test
    void findOrdersByUserId_invalidInput_throwsBadRequestException() {
        when(validator.validateUsersBeforeSearch(users)).thenReturn(false);
        assertThrows(BadRequestException.class,
                () -> service.findOrdersByUserId(users, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );

    }

    @Test
    void findOrdersByCertificateId_validInputFound_returnsDto() {
        final Optional<List<OrderDto>> results = Optional.of(List.of(orderDto));

        when(validator.validateCertificatesBeforeSearch(certificates)).thenReturn(true);
        when(repository.findOrdersByCertificateId(certificates,
                PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(results);

        assertEquals(results.get(),
                service.findOrdersByCertificateId(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void findOrdersByCertificateId_validInputNotFound_returnsEmptyList() {
        Optional<List<OrderDto>> result = Optional.of(Collections.emptyList());

        when(validator.validateCertificatesBeforeSearch(certificates)).thenReturn(true);
        when(repository.findOrdersByCertificateId(certificates,
                PAGE_NUMBER,
                PAGE_SIZE,
                SORTED_ASCENDING_PRESENT
        )).thenReturn(result);

        assertEquals(result.get(),
                service.findOrdersByCertificateId(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }

    @Test
    void findOrdersByCertificateId_invalidInput_throwsBadRequestException() {
        when(validator.validateCertificatesBeforeSearch(certificates)).thenReturn(false);

        assertThrows(BadRequestException.class,
                () -> service.findOrdersByCertificateId(certificates, PAGE_NUMBER, PAGE_SIZE, SORTED_ASCENDING_PRESENT)
        );
    }
}
