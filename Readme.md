# READ ME
---

## RUN PROJECT STEPS:

- download and install *JDK 11*
- download and install *Apache Maven 3.8.4*
- _(optional)_ download and install Postman 9.31.0
- set up your local MySql database with according properties set in `dao/src/main/resources/database.properties`
    - the project is configured to use **MySQL** database, to use another it is necessary to use the appropriate driver
      in
      `dao/src/main/java/com/epam/esm/dao/config/AppConfigProd.java`
    - it is possible to run also the application on _dev_ profile without involving an external database, to do so
      change the profile
      for `controller/src/main/java/com/epam/esm/controller/Application.java`
- to run your application execute following commands in terminal from repository root:
    - _(optional)_ `mvn clean`
    - _(optional)_ `mvn install`
    - `mvn spring-boot:run -pl controller`

---

## Security

This project uses JWT to authenticate and authorize requests. By default, JWT token expires after 24 hours from
creation.

Roles and permissions:

- anyone can access `/login` and `/register` endpoints
- `user` can access all endpoints with `GET` method and use `POST` method on `/orders` to make order on his behalf, this
  role is set initially when registered.
- `admin` can perform all operations, this role can be added only via database call.

---

## Demo

This example shows how to register, login and perform some operations in this application using JWT.
When application is running by default should be available under `localhost:8081/giftcertificate`

- To register no authentication is required, to create account you have to use http `POST` method on
  adress `localhost:8081/giftcertificate/register` and pass your account's credentials - `userName` and `password` in
  request body as below.

![image description](registation.jpg)

When  `201 Created` is returned account is successfully persisted in database.

- To log in it is required to use same credentials but under `localhost:8081/giftcertificate/login`

![image description](login.jpg)

- Token returned in response body from previous request should be used for further authentication & authorization
  e.g. `GET` GiftCertificate resource with id=1

![image description](search.jpg)

- Token contains also user's claims, so user can buy certificate on its own behalf

![image description](buyCertificate.jpg)
